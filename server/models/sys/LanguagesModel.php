<?php

/**
 * Description of LanguagesModel
 *
 * @author petrov
 */

namespace MODELS\SYS;

class LanguagesModel extends \CORE\Model {
    
    public function __construct() {
        parent::__construct('link');
    }
    
    public function getLanguage($short) {
        $sql = "SELECT * FROM sys_languages WHERE `short` = :short";
        
        $data = array(
            ':short' => trim($short)
        );
        
        return $this->prepare($sql)->execute($data)->fetchRowObject();
    }
    
    public function getLanguageById($id) {
        $sql = "SELECT * FROM sys_languages WHERE `id` = :id";
        
        $data = array(
            ':id' => (int) $id
        );
        
        return $this->prepare($sql)->execute($data)->fetchRowObject();
    }
    
    public function getLanguages($active = false) {
        $sql = "SELECT * FROM sys_languages WHERE 1 ";
        
        if ($active) {
            $sql .= " AND `active` ";
        }
        
        $sql .= " ORDER BY `order` ASC";
        
        return $this->prepare($sql)->execute()->fetchAllObject();
    }
    
    public function getDefaultLanguage() {
        $sql = "SELECT * FROM sys_languages WHERE `default` LIMIT 0, 1";
        
        return $this->prepare($sql)->execute()->fetchRowObject();
    }
    
    public function insertLanguage($item) {
        $sql = "INSERT INTO sys_languages (`title`, `short`, `default`, `order`, `active`) VALUES (:title, :short, :default, :order, :active)";
        
        $data = array(
            ':title'    => trim($item->title),
            ':short'    => trim($item->short),
            ':default'  => isset($item->default) ? (int) $item->default : 0,
            ':order'    => (int) $item->order,
            ':active'   => (int) $item->active,
        );
        
        return $this->prepare($sql)->execute($data)->getLastInsertID('id');
    }
    
    public function updateLanguage($item) {
        $sql = "UPDATE sys_languages SET `title` = :title, `short` = :short, `default` = :default, `order` = :order, `active` = :active WHERE `id` = :id";
        
        $data = array(
            ':id'       => (int) $item->id,
            ':title'    => trim($item->title),
            ':short'    => trim($item->short),
            ':default'  => isset($item->default) ? (int) $item->default : 0,
            ':order'    => (int) $item->order,
            ':active'   => (int) $item->active,
        );
        
        return $this->prepare($sql)->execute($data);
    }
    
    public function changeLangStatus($item) {
        $sql = "UPDATE sys_languages SET `active` = :active WHERE `id` = :id";
        
        $data = array(
            'active'    => (int) $item->active,
            'id'        => (int) $item->id
        );
        
        return $this->prepare($sql)->execute($data);
    }
    
    public function deleteLanguage($id) {
        return $this->prepare("DELETE FROM sys_languages WHERE id = :id")->execute(array(
            ':id' => (int) $id
        ));
    }
}
