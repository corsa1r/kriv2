<?php

namespace MODELS\SYS;

class AdminGroupsModel extends \CORE\Model {

    public function __construct() {
        parent::__construct('link');
    }

    public function getGroupById($id) {
        $result = $this->prepare('SELECT sys_admin_groups . * , COUNT( sys_admins.group_id ) AS totalUsers FROM sys_admin_groups LEFT JOIN sys_admins ON sys_admins.group_id = sys_admin_groups.id WHERE sys_admin_groups.id = :id GROUP BY sys_admin_groups.id')->execute(
                        array(
                            ':id' => $id
                        )
                )->fetchRowAssoc();
        if ($result) {
            $result['accessIds'] = json_decode($result['accessIds']);
        }
        return $result;
    }

    public function getAllGroups() {
        $result = $this->prepare('SELECT sys_admin_groups . * , COUNT( sys_admins.group_id ) AS totalUsers FROM sys_admin_groups LEFT JOIN sys_admins ON sys_admins.group_id = sys_admin_groups.id GROUP BY sys_admin_groups.id ORDER BY id ASC')->execute()->fetchAllAssoc();
        if ($result) {
            foreach ($result as &$res) {
                $res['accessIds'] = json_decode($res['accessIds']);
            }
        }

        return $result;
    }

    public function addGroup($params) {
        
        if (!is_array($params)) {
            $params = (array) $params;
        }
        
        if (isset($params['name'], $params['accessIds']) && strlen($params['name']) > 0 && is_array($params['accessIds'])) {
            $res = $this->prepare('INSERT INTO sys_admin_groups(name, accessIds) VALUES(:name, :accessIds)')->execute(array(
                ':name' => $params['name'],
                ':accessIds' => json_encode($params['accessIds'])
            ));

            return $res;
        }

        return false;
    }

    public function editGroup($id, $params) {

        if (!is_array($params)) {
            $params = (array) $params;
        }

        $sql = 'UPDATE sys_admin_groups SET ';

        $data = array(':id' => $id);


        $response = array(
            'state' => false,
            'errorMessage' => ''
        );

        $addToSql = array();

        if(isset($params['name']) && strlen($params['name']) > 0) {
            $addToSql[] = 'name = :name';
            $data[':name'] = $params['name'];
        }

        if(isset($params['accessIds']) && is_array($params['accessIds'])) {
            $addToSql[] = 'accessIds = :accessIds';
            $data[':accessIds'] = json_encode(array_map('intval', array_values(array_unique($params['accessIds']))));
        }



        $sql .= implode(', ', $addToSql) . ' WHERE id = :id';

        $result = $this->prepare($sql)->execute($data);

        $response['state'] = (bool) $result;

        return $response;
    }
    
    public function deleteGroup($id) {
        return $this->prepare('DELETE FROM sys_admin_groups WHERE id = :id')->execute(array(':id' => (int)$id));
    }
}
