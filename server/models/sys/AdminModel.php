<?php

namespace MODELS\SYS;

class AdminModel extends \CORE\Model {

    public function __construct() {
        parent::__construct('link');
    }

    private static function getHashedPassword($password) {
        return md5($password);
    }

    public function getAdminCredentials($username, $password) {
        $result = $this->prepare('SELECT id FROM sys_admins WHERE `username` = :username AND `password` = :password AND `is_active` = :is_active')->execute(
                        array(
                            ':username' => $username,
                            ':password' => self::getHashedPassword($password),
                            ':is_active' => 1
                        )
                )->fetchRowAssoc();

        if ($result) {
            return $this->getAdminById($result['id']);
        }

        return $result;
    }

    public function getAdminById($id, $fullInfo = false) {
        $result = $this->prepare('SELECT sys_admins.*, sys_admin_groups.name as group_name, sys_admin_groups.accessIds FROM sys_admins LEFT JOIN sys_admin_groups ON sys_admins.group_id = sys_admin_groups.id WHERE sys_admins.id = :id')->execute(
                        array(
                            ':id' => $id
                        )
                )->fetchRowAssoc();
        if ($result) {
            $result['accessIds'] = json_decode($result['accessIds']);

            if (!$fullInfo) {
                unset($result['password']);
                unset($result['is_active']);
                unset($result['date_added']);
            }
        }
        return $result;
    }

    public function getAllAdmins($fullInfo = false) {
        $result = $this->prepare('SELECT sys_admins.*, sys_admin_groups.name as group_name, sys_admin_groups.accessIds FROM sys_admins LEFT JOIN sys_admin_groups ON sys_admins.group_id = sys_admin_groups.id')->execute()->fetchAllAssoc();
        if ($result) {
            foreach ($result as &$res) {
                $res['accessIds'] = json_decode($res['accessIds']);

                if ($fullInfo !== false) {
                    unset($res['password']);
                    unset($res['is_active']);
                    unset($res['date_added']);
                }
            }
        }

        return $result;
    }

    public function addAdmin($params) {

        if (!is_array($params)) {
            $params = (array) $params;
        }

        $response = array(
            'state' => false,
            'errorMessage' => ''
        );
        
        $res = false;
        
        if(isset($params['group_id'], $params['username'], $params['password'])){
            if(!$this->isUsernameExist($params['username'])){
                $res = $this->prepare('INSERT INTO sys_admins(group_id, username, password, first_name, last_name, is_active, date_added) VALUES(:group_id, :username, :password, :first_name, :last_name, :is_active, :date_added)')->execute(array(
                    ':group_id' => $params['group_id'],
                    ':username' => $params['username'],
                    ':password' => self::getHashedPassword($params['password']),
                    ':first_name' => isset($params['first_name']) ? $params['first_name'] : '',
                    ':last_name' => isset($params['last_name']) ? $params['last_name'] : '',
                    ':is_active' => isset($params['is_active']) ? $params['is_active'] : 1,
                    ':date_added' => time()
                ));
            }else{
                $response['errorMessage'] = 'Username already exist!';
            }
        }
        else{
            $response['errorMessage'] = 'Group, Username and Password is required!';
        }

        $response['state'] = (bool) $res;

        return $response;
    }

    public function isUsernameExist($username, $ignoreId = 0) {
        $result = $this->prepare('SELECT COUNT(*) AS total FROM sys_admins WHERE username = :username AND id != :id')->execute(array(':username' => $username, ':id' => $ignoreId))->fetchRowAssoc();

        if ($result && $result['total'] == 0) {
            return false;
        }

        return true;
    }

    public function editAdmin($id, $params) {

        if (!is_array($params)) {
            $params = (array) $params;
        }

        $sql = 'UPDATE sys_admins SET ';

        $data = array(':id' => $id);


        $response = array(
            'state' => true,
            'errorMessage' => ''
        );


        foreach ($params as $name => $param) {
            switch ($name) {
                case 'is_active':
                    if (in_array($param, array(0, 1))) {
                        $data[':' . $name] = $param;
                        $addToSql[] = $name . ' = :' . $name;
                    }
                    break;
                case 'group_id':
                case 'first_name':
                case 'last_name':
                    $data[':' . $name] = $param;
                    $addToSql[] = $name . ' = :' . $name;
                    break;
                case 'password':
                    if (strlen($param) > 0) {
                        $data[':' . $name] = self::getHashedPassword($param);
                        $addToSql[] = $name . ' = :' . $name;
                    }
                    break;
                case 'username':
                    if (strlen($param) > 0 && !$this->isUsernameExist($param, $id)) {
                        $data[':' . $name] = $param;
                        $addToSql[] = $name . ' = :' . $name;
                    } else {
                        $response['state'] = false;
                        $response['errorMessage'] = 'User exist!';
                    }
                    break;
            }
        }

        $sql .= implode(', ', $addToSql) . ' WHERE id = :id';

        $result = $this->prepare($sql)->execute($data);

        return $response;
    }
    
    public function changeAdminsGroup($oldGroupId, $newGroupId) {
        return $this->prepare('UPDATE sys_admins SET group_id = :newGroupId WHERE group_id = :oldGroupId')->execute(array(':newGroupId' => $newGroupId, ':oldGroupId' => $oldGroupId));
    }

    public function deleteAdmin($id) {
        return $this->prepare('DELETE FROM sys_admins WHERE id = :id')->execute(array(':id' => (int) $id));
    }

}
