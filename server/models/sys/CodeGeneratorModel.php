<?php

namespace MODELS\SYS;

class CodeGeneratorModel extends \CORE\Model {

    public function __construct() {
        parent::__construct('link');
    }

    public function getDbTables() {
        $response = $this->prepare('SHOW TABLES')->execute()->fetchAllNum();
        $results = array();
        foreach($response as $table){
            $results[] = $table[0];
        }
        return $results;
    }

}
