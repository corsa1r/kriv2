<?php

namespace MODELS\SYS;

class ConfigModel extends \CORE\Model {

    private static $__instance = null;

    protected function __construct() {
        parent::__construct('link');
    }

    public static function getInstance() {
        if(is_null(self::$__instance)){
            self::$__instance = new self;
        }

        return self::$__instance;
    }

    public function get($key, $toJson = false, $extendInfo = false) {
        $result = $this->prepare('SELECT `value`, `last_update` FROM sys_configs WHERE `key` = :key')->execute(array(':key' => $key))->fetchRowAssoc();
        $this->dieOnError();
        $response = $result;
        if($extendInfo){
            return $toJson ? json_decode(json_encode($result)) : $result;
        }

        return $toJson ? json_decode($result['value']) : $result['value'];
    }

    public function getFromArray($keys, $extendInfo = false) {
        throw new \Exception('DO NOT USE THIS FUNCTION!');
        
        $result = $this->prepare('SELECT `key`, `value`, `last_update` FROM sys_configs WHERE `key` IN(:keys)')->execute(array(':keys' => implode(', ', $keys)))->fetchAllAssoc();
        $this->dieOnError();

        $response = array();
        foreach ($keys as $key){
            if($extendInfo){
//                $response[$key] = isset();
            } else{
                
            }
        }

        return $response;
    }
    
    public function getGroup($groupName, $extendInfo = false) {
        $result = $this->prepare('SELECT `key`, `value`, `last_update` FROM sys_configs WHERE `key` LIKE :groupName')->execute(array(':groupName' => $groupName . '.%'))->fetchAllAssoc();
        $this->dieOnError();
        
        $response = array();
        if(!$extendInfo){
            foreach ($result as $item){
                $response[substr($item['key'], strpos($item['key'], '.') + 1)] = $item['value'];
            }
        }else{
            $response = $result;
        }
        
        return $response;
    }

    public function getAll($extendInfo = false) {
        $result = $this->prepare('SELECT `key`, `value`, `last_update` FROM sys_configs')->execute()->fetchAllAssoc();
        $this->dieOnError();
        if(!$extendInfo){
            $response = array();

            foreach ($result as $item){
                $response[$item['key']] = $item['value'];
            }
            return $response;
        }

        return $result;
    }

    public function set($key, $value) {
        
        if(strpos($key, 'libs_path.') === 0) {
            $autoloaderCacheSession = new \MODULES\Session('Autoloader.cache');
            $autoloaderCacheSession->clean();// TODO fix this this !
        }
        
        $result = $this->prepare('REPLACE INTO sys_configs SET `key` = :key, `value` = :value, `last_update` = :last_update')->execute(
                array(
                    ':key' => $key,
                    ':value' => $value,
                    ':last_update' => time(),
                )
        );
        $this->dieOnError();
        if(!$result){
            return false;
        }

        return $result;
    }

    public function delete($key) {
        $result = $this->prepare('DELETE FROM sys_configs WHERE `key` = :key')->execute(
                array(
                    ':key' => $key,
                )
        );
        $this->dieOnError();
        if(!$result){
            return false;
        }

        return $result;
    }

}
