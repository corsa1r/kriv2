<?php

/*
 * 
 * @version 09.10.2014, 17:29:30
 */

namespace MODELS\SYS;

class TranslationsModel extends \CORE\Model {

    public function __construct() {
        parent::__construct('link');
    }

    public function getTranslationByLanguage($langCode) {
        $result = $this->prepare('SELECT * FROM `sys_translates`'
                        . ' JOIN `sys_languages` ON `sys_languages`.`id` = `sys_translates`.`lang_id`'
                        . ' JOIN `sys_translate_keys` ON `sys_translate_keys`.`id` = `sys_translates`.`key_id`'
                        . ' WHERE `sys_languages`.`short` = :langCode'
                        . '')->execute(Array(
                    ':langCode' => $langCode
                ))->fetchAllObject();

        $this->dieOnError();

        return $result;
    }

    public function updateRow($post) {
        if (!isset($post)) {
            return false;
        }

        $result = $this->prepare('UPDATE `sys_translates` SET `translation` = :translation WHERE `lang_id` = :lang_id AND `key_id` = :key_id')->execute(Array(
            'translation' => $post->translation,
            'lang_id' => (int) $post->lang_id,
            'key_id' => (int) $post->key_id
        ));

        $this->dieOnError();

        return (bool) $result;
    }

    public function addKey($post) {

        if (!isset($post) || empty($post->key)) {
            return false;
        }

        $key_id = $this->prepare("INSERT INTO `sys_translate_keys` (`key`) VALUES (:key)")->execute(Array(
                    ':key' => $post->key
                ))->getLastInsertID();

        $this->dieOnError();
        $this->addEmptyTranslation($key_id);
        
        return true;
    }

    private function addEmptyTranslation($key_id) {
        $languagesModel = new \MODELS\SYS\LanguagesModel();
        $languages = $languagesModel->getLanguages(true);
        $languagesIds = Array();

        foreach ($languages as &$lang) {
            $languagesIds[] = (int) $lang->id;
        }

        $stmt = $this->prepare("INSERT INTO `sys_translates` (`lang_id`, `key_id`, `translation`) VALUES (?, ?, ?)");

        foreach ($languagesIds as $languageId) {
            $stmt->execute(Array(
                $languageId,
                (int) $key_id,
                ''
            ));
        }

        $this->dieOnError();
    }

}
