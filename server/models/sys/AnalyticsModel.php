<?php

namespace MODELS\SYS;

class AnalyticsModel extends \CORE\Model {

    public function __construct() {
        parent::__construct('link');
    }

    public function addAnalytics($ip, $userAgent) {

        $response = false;
        if(isset($ip, $userAgent)){
            $browser = new \MODULES\Browser($userAgent);
            $response = $this->prepare('INSERT INTO sys_analytics(ip, browser, version, platform, is_mobile, user_agent, date_added) VALUES(:ip, :browser, :version, :platform, :is_mobile, :user_agent, :date_added)')->execute(array(
                ':ip' => $ip,
                ':browser' => $browser->getBrowser(), 
                ':version' => $browser->getVersion(), 
                ':platform' => $browser->getPlatform(), 
                ':is_mobile' => $browser->isMobile(),
                ':user_agent' => $userAgent,
                ':date_added' => time()
            ));
        }
        return $response;
    }
    
    public function getBrowsers($from = 0, $to = null) {
        if(is_null($to) || empty($to)){
            $to = time();
        }
        $results = $this->prepare('SELECT browser AS name, COUNT(*) AS total FROM sys_analytics WHERE date_added > :from AND date_added < :to GROUP BY browser')->execute(array(':from' => $from, ':to' => $to))->fetchAllAssoc();
        return $results;
    }
    
    public function getMobileCount($from = 0, $to = null) {
        if(is_null($to) || empty($to)){
            $to = time();
        }
        $results = $this->prepare('SELECT COUNT(*) AS total FROM sys_analytics WHERE date_added > :from AND date_added < :to AND is_mobile = 1')->execute(array(':from' => $from, ':to' => $to))->fetchRowAssoc();
        return $results['total'];
    }
    
    public function getDesktopCount($from = 0, $to = null) {
        if(is_null($to) || empty($to)){
            $to = time();
        }
        $results = $this->prepare('SELECT COUNT(*) AS total FROM sys_analytics WHERE date_added > :from AND date_added < :to AND is_mobile = 0')->execute(array(':from' => $from, ':to' => $to))->fetchRowAssoc();
        return $results['total'];
    }
    
    public function getPlatforms($from = 0, $to = null) {
        if(is_null($to) || empty($to)){
            $to = time();
        }
        $results = $this->prepare('SELECT platform AS name, COUNT(*) AS total FROM sys_analytics WHERE date_added > :from AND date_added < :to GROUP BY platform')->execute(array(':from' => $from, ':to' => $to))->fetchAllAssoc();
        return $results;
    }

    public function getAnalytics($from = 0, $to = null) {
        $result = array(
            'browsers' => $this->getBrowsers($from, $to),
            'platforms' => $this->getPlatforms($from, $to),
            'mobileCount' => $this->getMobileCount($from, $to),
            'desktopCount' => $this->getDesktopCount($from, $to),
        );
        
        return $result;
    }

}