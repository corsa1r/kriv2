<?php

namespace CORE;

class UrlBuilder {
    static function build($directory, $baseUrl = null){
        if(is_null($baseUrl)){
            $baseUrl = \MODELS\SYS\ConfigModel::getInstance()->get('baseUrl');
        }
        $transferDirs = \MODELS\SYS\ConfigModel::getInstance()->getGroup('fileTransferDirs');
        if(isset($transferDirs[$directory])){
            $directory = $transferDirs[$directory];
        }
        
        return $baseUrl . $directory;
    }
}