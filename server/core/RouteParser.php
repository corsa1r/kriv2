<?php

namespace CORE;

class RouteParser {

    public $controller = null;
    public $method = null;
    public $arguments = Array();
    public $get = Array();
    public $getEmpty = true;
    public $directory = '';
    
    public function __construct($paths) {
        $full_url = $paths['QUERY_STRING'];
        
        $exploded_url = explode("?", $full_url);
        
        $path = explode('@', $exploded_url[0]);
        
        
        $directory = '';
        $pathRoute = $exploded_url[0];
        if(count($path) > 1){
            $directory = $path[0];
            $pathRoute = $path[1];
        }
 
        $routes = explode("/", $pathRoute);
        
        if(count($path) === 1){
            array_shift($routes);
        }
        
        $get = explode("?", $paths['REQUEST_URI']);
        
        if (isset($get[1])) {
            $this->parseGET($get[1]);
        }

        if (count($routes) > 1) {
            $this->controller = $routes[0];
            $this->method = $routes[1];
            $this->directory = $directory;

            array_shift($routes);
            array_shift($routes);

            $this->arguments = $routes;
        }
    }

    private function parseGET($string) {
        $params = explode("&", $string);

        foreach ($params as $param) {
            $ex_param = explode("=", $param);
            $this->get[$ex_param[0]] = $ex_param[1];
            $this->getEmpty = false;
        }
    }

}
