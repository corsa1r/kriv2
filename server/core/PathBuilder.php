<?php

namespace CORE;

class PathBuilder {
    static function build($directory, $baseDir = null){
        if(is_null($baseDir)){
            $baseDir = BASE_PATH . DS . '..' . DS;
        }
        $transferDirs = \MODELS\SYS\ConfigModel::getInstance()->getGroup('fileTransferDirs');
        if(isset($transferDirs[$directory])){
            $directory = $transferDirs[$directory];
        }
        
        return $baseDir . $directory;
    }
}