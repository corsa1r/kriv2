<?php

namespace CORE;

class OutputResponse {

	public $timestamp = null;

	public static $RESPONSE_TYPE_OK		= "OK";
	public static $RESPONSE_TYPE_ERROR	= "ERROR";

	public $state;
	public $status;

	public function __construct() {
		$this->state  = false;
		$this->status = self::$RESPONSE_TYPE_ERROR;
	}

	public $data = null;

	public function setData($data) {
		if($data || is_array($data) || is_object($data) || is_bool($data)) {
			$this->data = $data;
		}
	}

	public $errorMessage = '';

	public function setErrorMessage ($message = null) {
		if($message !== null) {
			$this->errorMessage = $message;
		}
	}

	public function send($pretty = false) {
		if($this->data !== false && !$this->errorMessage) {
			$this->state = true;
			$this->status = self::$RESPONSE_TYPE_OK;
		}

		$this->timestamp = time() * 1000;

                header('Content-Type: application/json');
		echo json_encode($this, $pretty ? JSON_PRETTY_PRINT : 0);
		exit;
	}
}