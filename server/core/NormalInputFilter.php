<?php

namespace CORE;

class NormalInputFilter {

    public function __construct() {}

    public function filter($data, $without_html_escape = false) {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $data[$key] = $this->filter($value, $without_html_escape);
            }
        } else if(is_object($data)) {
            foreach ($data as $key => $value) {
                $data->$key = $this->filter($value, $without_html_escape);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = stripslashes($data);
            }

            if (!$without_html_escape) {
                $data = htmlspecialchars($data);
            }

            $data = trim($data);
        }

        return $data;
    }
}