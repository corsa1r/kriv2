<?php

namespace CORE;

class BaseController {

    protected $_dependency = array();
    protected $_depsInstances = array();

    public function __construct() {

        foreach ($this->_dependency as $dep) {

            $depExplode = explode(':', $dep);
            $classPath = $depExplode[0];
            $pathExplode = explode('\\', $classPath);
            $className = end($pathExplode);
            $params = isset($depExplode[1]) ? array_map('trim', explode(',', $depExplode[1])) : array();

            $nsname = "\\MODULES\\" . $classPath;
            if (!class_exists($nsname)) {
                $response = new \CORE\OutputResponse();
                $response->setErrorMessage('Class ' . $nsname . ' not found. Check the NAMESPACE');
                $response->send();
            }
            $module = new $nsname($params);

            $this->_depsInstances['_' . lcfirst($className)] = $module;
        }
    }

    public function __get($varName) {

        if (!array_key_exists($varName, $this->_depsInstances)) {
            //this attribute is not defined!
            throw new \Exception($varName . ' is not in controller dependency list!');
        } else {
            return $this->_depsInstances[$varName];
        }
    }
    
    public function invalidSession() {
        return (object) array(
            'invalidSession' => true
        );
    }
}
