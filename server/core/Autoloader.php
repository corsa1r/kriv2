<?php

namespace CORE;

/**
 * Description of Autoloader
 *
 * @author corsair
 */
class Autoloader {

    private static $cacheSession = null;

    /**
     * The autoload function being registered.
     */
    public static function registerAutoloader() {
        spl_autoload_register(array('\CORE\Autoloader', 'load'));

        self::$cacheSession = new \MODULES\Session('Autoloader.cache');

//        
//            echo "<pre>";
//            var_dump(self::$cacheSession->getAllData());
//            echo "</pre>";
    }

    /**
     * This is callback function from spl_autoload_register
     * @param String $nsname
     */
    public static function load($nsname) {
        $path = self::_normalizeFolders($nsname);

        if(file_exists($path)){
            include_once $path;
        } else{
            $response = new \CORE\OutputResponse();
            $response->setErrorMessage('Cannot instantiate class ' . $nsname . '. Not found.');
            $response->send();
        }
    }

    private static function _normalizeFolders(&$nsname) {
        $str = "";
        $arr = explode('\\', $nsname);

        for ($i = 0; $i < count($arr) - 1; $i++){
            $str .= strtolower($arr[$i]) . DS;
        }

        $realpath = realpath(BASE_PATH . DS . $str . $arr[count($arr) - 1] . '.php');

        if(!$realpath){
            if(!self::$cacheSession->exists('libs_path')){
                self::$cacheSession->set('libs_path', \MODELS\SYS\ConfigModel::getInstance()->getGroup('libs_path'));
            }

            $libsPaths = self::$cacheSession->get('libs_path');

            $moduleClass = explode('_', $nsname);
            $realpath = BASE_PATH . DS . $libsPaths[strtolower($moduleClass[0])] . implode(DS, $moduleClass) . '.php';
        }

        return $realpath;
    }

    private function __construct() {
        
    }

}
