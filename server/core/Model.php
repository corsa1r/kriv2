<?php
namespace CORE;
/*
 * Kriv Framework
 */

/**
 * Description of Model
 *
 * @author Vladimir <vladimir.enchev@interactive-share.com>
 * @version 2014-2-10
 */
class Model {

    private static $_connections = array();
    
    private $db = null;//PDO Object refference

    /**
     *
     * @var CORE\Model
     */
    private $stmt = null;
    private $params = array();
    private $sql;

    protected function __construct($connection = null) {
        if($connection !== null) {
            $this->db = $this->getDatabaseConnection($connection);
        }
    }
    
    private static function getDatabaseConnection($connection = 'link') {
        if (!isset($connection)) {
            throw new \Exception('Connection name is required !', 500);
        }
        
        if(isset(self::$_connections[$connection])) {
            return self::$_connections[$connection];
        }
        
        if(!array_key_exists($connection, \CORE\ConfigManager::get()['database'])) {
            throw new \Exception('Connection name is not defined in database.php config file !', 500);
        }
        
        $dbc = new \PDO(
            \CORE\ConfigManager::get()['database'][$connection]['connection_uri'],
            \CORE\ConfigManager::get()['database'][$connection]['username'],
            \CORE\ConfigManager::get()['database'][$connection]['password'],
            \CORE\ConfigManager::get()['database'][$connection]['pdo_options']
        );
        
        self::$_connections[$connection] = $dbc;
        return $dbc;
    }
    
    public function prepare($sql, $params = array(), $pdoOptions = array()) {
        $this->stmt = $this->db->prepare($sql, $pdoOptions);
        $this->params = $params;
        $this->sql = $sql;        
        return $this;
    }
    
    public function bindParam() {
        call_user_func_array(array($this->stmt, __FUNCTION__), func_get_args());
        return $this;
    }
    
    public function bindValue() {
        call_user_func_array(array($this->stmt, __FUNCTION__), func_get_args());
        return $this;
    }

    public function execute($params = array()) {
        if($params) {
            $this->params = $params;
        }
        
        $this->stmt->execute($this->params);
        return $this;
    }
    
    public function fetchRowAssoc() {
        return $this->stmt->fetch(\PDO::FETCH_ASSOC);
    }
    
    public function fetchAllAssoc() {
        return $this->stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    public function fetchRowJSON() {
        return json_encode($this->fetchRowAssoc());
    }
    
    public function fetchAllJSON() {
        return json_encode($this->fetchAllAssoc());
    }
    
    
    public function fetchRowObject() {
        return $this->stmt->fetch(\PDO::FETCH_OBJ);
    }
    
    public function fetchAllObject() {
        return $this->stmt->fetchAll(\PDO::FETCH_OBJ);
    }
    
    
    public function fetchRowNum() {
        return $this->stmt->fetch(\PDO::FETCH_NUM);
    }
    
    public function fetchAllNum() {
        return $this->stmt->fetchAll(\PDO::FETCH_NUM);
    }
    
    
    public function getAffectedRows() {
        return $this->stmt->rowCount();
    }
    
    
    public function getSTMT() {
        return $this->stmt;
    }
    
    public function getDB() {
        return $this->db;
    }
    
    public function getLastInsertID($name = null) {
        return $this->db->lastInsertId($name);
    }
    
    public function hasError() {
        return $this->stmt->errorInfo()[2];
    }
    
    public function dieOnError() {
        if($err = $this->hasError()) {
            die($err);
        }
    }
}