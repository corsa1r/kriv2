<?php

namespace CORE;

class CodeGenerator {

    protected $_insertMarker = '/*-CodeGenerator_marker-*/';
    private $__createdFiles = array();
    private $__updatedFiles = array();

    public function addRoutes($data) {
        foreach ($data->scopes as $scope => $scopeData){
            $scope = strtolower($scope);

            $routesFile = BASE_PATH . '/../' . $scope . '/configs/routes.js';
            $templatePath = BASE_PATH . '/../admin/templates/sys/code_generator/routes/' . $scope . '.js';

            $file = file_get_contents($routesFile);

            $template = $this->translateTemplate(file_get_contents($templatePath), array(
                '{component_name_ucfirst}' => ucfirst($data->componentName),
                '{component_name_lcfirst}' => lcfirst($data->componentName),
                '{component_name_underscore_lowercase}' => strtolower(preg_replace('/\B([A-Z])/', '_$1', lcfirst($data->componentName))),
                '{when}' => $scopeData->when,
                '{link_name}' => isset($scopeData->linkName) ? $scopeData->linkName : '',
                '{access_id}' => isset($scopeData->accessId) ? $scopeData->accessId : '',
            ));

            $this->updateFile($routesFile, $this->insertCode($file, $template));
        }
        return true;
    }

    public function addControllers($data) {
        foreach ($data->scopes as $scope => $scopeData){
            $scope = strtolower($scope);

            $filePath = BASE_PATH . '/../' . $scope . '/controllers/';
            $templatesPath = BASE_PATH . '/../admin/templates/sys/code_generator/controllers/';

            $file = file_get_contents($templatesPath . $scope . '.js');

            if(isset($data->actions->$scope)){
                foreach ($data->actions->$scope as $action){
                    $actionFile = $this->translateTemplate(file_get_contents($templatesPath . $scope . '-actions/' . $action . '.js'));

                    $file = $this->insertCode($file, $actionFile);
                }
            }

            $file = $this->translateTemplate($file, array(
                '{component_name_ucfirst}' => ucfirst($data->componentName),
                '{date_added}' => date('d.m.Y, H:i:s'),
            ));

            $this->createFile($filePath . ucfirst($data->componentName) . 'Controller.js', $file);
        }
        return true;
    }

    public function addTemplates($data) {
        foreach ($data->scopes as $scope => $scopeData){
            $scope = strtolower($scope);
            $componentNameUnderscoreLowercase = strtolower(preg_replace('/\B([A-Z])/', '_$1', lcfirst($data->componentName)));

            $filePath = BASE_PATH . '/../' . $scope . '/templates/';
            $templatesPath = BASE_PATH . '/../admin/templates/sys/code_generator/templates/';

            $file = file_get_contents($templatesPath . $scope . '.html');

            $file = $this->translateTemplate($file, array(
                '{component_name_underscore_lowercase}' => $componentNameUnderscoreLowercase,
                '{date_added}' => date('d.m.Y, H:i:s'),
            ));

            $this->createFile($filePath . $componentNameUnderscoreLowercase . '.html', $file);
        }
        return true;
    }

    public function addServerControllers($data) {
        foreach ($data->scopes as $scope => $scopeData){
            $scope = strtolower($scope);

            $filePath = BASE_PATH . '/controllers/' . $scope . '/';

            $templatesPath = BASE_PATH . '/../admin/templates/sys/code_generator/server/controllers/';

            $file = file_get_contents($templatesPath . $scope . '.php');

            if(isset($data->actions->model)){
                foreach ($data->actions->model as $action){
                    $actionFile = $this->translateTemplate(file_get_contents($templatesPath . $scope . '-actions/' . $action . '.php'));

                    $file = $this->insertCode($file, $actionFile);
                }
            }

            $file = $this->translateTemplate($file, array(
                '{component_name_ucfirst}' => ucfirst($data->componentName),
                '{component_name_lcfirst}' => lcfirst($data->componentName),
                '{date_added}' => date('d.m.Y, H:i:s'),
            ));

            $this->createFile($filePath . ucfirst($data->componentName) . 'Controller.php', $file);
        }
        return true;
    }

    public function addServerModels($data) {
        if(isset($data->actions->model)){

            $filePath = BASE_PATH . '/models/';

            $templatesPath = BASE_PATH . '/../admin/templates/sys/code_generator/server/models/';

            $file = file_get_contents($templatesPath . 'model.php');

            foreach ($data->actions->model as $action){
                $actionFile = $this->translateTemplate(file_get_contents($templatesPath . 'actions/' . $action . '.php'));

                $file = $this->insertCode($file, $actionFile);
            }

            $file = $this->translateTemplate($file, array(
                '{component_name_ucfirst}' => ucfirst($data->componentName),
                '{component_name_lcfirst}' => lcfirst($data->componentName),
                '{table_name}' => $data->dbTable,
                '{date_added}' => date('d.m.Y, H:i:s'),
            ));
            $this->createFile($filePath . ucfirst($data->componentName) . 'Model.php', $file);
        }
        return true;
    }

    public function createFile($path, $file) {
        $this->__createdFiles[] = str_replace(BASE_PATH, '/server', $path);
//        return;
        return file_put_contents($path, $file);
    }

    public function updateFile($path, $file) {
        $this->__updatedFiles[] = str_replace(BASE_PATH, '/server', $path);
//        return;
        return file_put_contents($path, $file);
    }

    public function insertCode($file, $template) {
        if(strpos($file, $this->_insertMarker) === false){
            throw new Exception('CodeGenerator mark is missing!');
        }
        return substr_replace($file, $template, strpos($file, $this->_insertMarker), 0);
    }

    public function translateTemplate($template, $params = array()) {
        return strtr($template, $params);
    }

    public function generate($data) {
        $this->addRoutes($data);
        $this->addControllers($data);
        $this->addTemplates($data);
        $this->addServerControllers($data);
        $this->addServerModels($data);
        
        return array('updated' => $this->__updatedFiles, 'created' => $this->__createdFiles);
    }

}
