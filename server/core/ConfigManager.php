<?php

namespace CORE;

class ConfigManager {

    private static $_config = null;

    static function get() {
        if (is_null(self::$_config)) {
            self::$_config = include './config.inc.php';
        }
        return self::$_config;
    }
    
    private function __construct() {
        
    }

}