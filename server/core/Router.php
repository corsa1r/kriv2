<?php

namespace CORE;

class Router {

    public function __construct($path = null) {
        
        if ($path instanceof \CORE\RouteParser) {
            
            if (!!$path->controller && !!$path->method) {
                
                $nsname = "\\CONTROLLERS" . str_replace('/', '\\', strtoupper($path->directory)) . '\\' . $path->controller;
                if (!class_exists($nsname)) {
                    $response = new \CORE\OutputResponse();
                    $response->setErrorMessage('Class ' . $nsname . ' not found. Check the NAMESPACE');
                    $response->send();
                }

                $controller = new $nsname();
                $controller->_GET = $path->get;

                if (method_exists($controller, $path->method)) {
                    $response = new \CORE\OutputResponse();
                    $response->setData(call_user_func_array(Array($controller, $path->method), $path->arguments));
                    $response->send(in_array('pretty', $path->arguments));
                } else {
                    $response = new \CORE\OutputResponse();
                    $response->setErrorMessage('Cannot call method ' . $path->method . ' of undefined in ' . $path->controller . ' class');
                    $response->send();
                }
            }
        }
    }

}
