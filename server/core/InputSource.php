<?php

namespace CORE;

class InputSource {

	public $data = null;

	public function __construct($source) {
		$this->data = $source;
	}

	public function isEmpty() {
		return !(bool)$this->data;
	}

	public function filter($filterClass) {
		$this->data = $filterClass->filter($this->data);
	}
}