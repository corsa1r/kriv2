<?php

namespace CONTROLLERS\SYS;

class EditorController extends \CORE\BaseController {

    protected $_dependency = Array(
        'Session:logged-admin',
        'InputHandler'
    );
    protected $_baseDir = null;

    public function __construct() {
        parent::__construct();

        $this->_baseDir = BASE_PATH . DS . '..' . DS;
    }

    public function isWritable($filename) {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        return is_writable($filename);
    }
    
    public function isReadable($filename) {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        return is_readable($filename);
    }
    
    public function isFile($filename) {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        return is_file($filename);
    }

    public function getFile() {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        $file = isset($this->_GET['file']) ? $this->_baseDir . urldecode($this->_GET['file']) : false;
        $result = array('state' => false);

        if($file){
            if($this->isFile($file) && $this->isReadable($file)){
                $content = file_get_contents($file);
                if($content !== false){
                    $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
                    $result['isWritable'] = $this->isWritable($file);
                    $result['extension'] = $fileExtension;
                    $result['content'] = $content;
                    $result['state'] = true;
                } else{
                    $result ['errorMessage'] = 'File can\'t be read.';
                }
            } else{
                $result['errorMessage'] = 'File is not readable or doesn\'t exist.';
            }
        } else{
            $result['errorMessage'] = 'You must specify file path.';
        }

        return $result;
    }
    
    public function saveFile() {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        $file = isset($this->_inputHandler->post->data->file) ? $this->_baseDir . $this->_inputHandler->post->data->file : false;
        $content = isset($this->_inputHandler->post->data->content) ? $this->_inputHandler->post->data->content : '';
        
        $result = array('state' => false);

        if($file){
            if($this->isFile($file) && $this->isWritable($file)){
                $saved = file_put_contents($file, $content);
                if($saved !== false){
                    $result['state'] = true;
                    $result['savedBytes'] = $saved;
                } else{
                    $result ['errorMessage'] = 'File can\'t be saved.';
                }
            } else{
                $result['errorMessage'] = 'File is not writable or doesn\'t exist.';
            }
        } else{
            $result['errorMessage'] = 'You must specify file path.';
        }

        return $result;
    }
}
