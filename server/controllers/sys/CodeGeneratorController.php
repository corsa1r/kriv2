<?php

namespace CONTROLLERS\SYS;

class CodeGeneratorController extends \CORE\BaseController {

    protected $_dependency = Array(
        'Session:logged-admin',
        'InputHandler'
    );

    public function __construct() {
        parent::__construct();
    }

    public function generate() {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }

        $data = $this->_inputHandler->post->data;
//        $data = new \stdClass();
//        $data->scopes = array('admin' => (object)array(
//                'when' => 'test-admin',
//                'linkName' => 'Test Link',
//                'accessId' => '2',
//        ));
//
//        $data->componentName = 'TestTemplateWithManyWords';
//        $data->dbTable = 'fortunes';
//
//        $data->actions = (object)array(
//            'admin' => (object)array(
//                'add',
//                'getById',
//                'getAll',
//                'deleteById',
//            ),
//            'model' => (object)array(
//                'add',
//                'getById',
//                'getAll',
//                'deleteById'
//            ),
//        );

        $codeGenerator = new \CORE\CodeGenerator();

        return $codeGenerator->generate($data);
    }

    public function getDbTables() {
        $codeGeneratorModel = new \MODELS\SYS\CodeGeneratorModel();
        
        return $codeGeneratorModel->getDbTables();
    }
}
