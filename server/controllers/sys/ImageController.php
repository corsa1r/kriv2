<?php

/**
 * @author Vladimir Enchev <vladimir.enchev@interactive-share.com>
 * @time 2014-10-29
 */

namespace CONTROLLERS\SYS;

class ImageController {

    private $images = null;

    public function __construct() {
        $this->images = new \MODULES\Images();
    }

    public function getThumbPath($params) {
        $paramsFormated = explode("#", base64_decode($params));

        if (count($paramsFormated) < 3) {
            return false;
        }

        $path = $paramsFormated[0];
        $width = $paramsFormated[1];
        $height = $paramsFormated[2];

        if ($width === 'true') {
            $width = true;
        } else if ($width === 'false') {
            $width = false;
        } else {
            $width = (int) $paramsFormated[1];
        }

        if ($height === 'true') {
            $height = true;
        } else if ($height === 'false') {
            $height = false;
        } else {
            $height = (int) $paramsFormated[2];
        }

        $position = in_array($paramsFormated[3], Array('tl', 'tc', 'tr', 'ml', 'mc', 'mr', 'bl', 'bc', 'br')) ? $paramsFormated[3] : 'mc';
        $grayscale = $paramsFormated[4] === 'true' ? true : false;

        $pathResult = $this->images->getThumb($path, $width, $height, $position, $grayscale);

        $configurations = new \CONTROLLERS\SYS\ConfigurationController();

        header('Location: ' . rtrim($configurations->get('baseUrl'), '/') . $pathResult);
        exit;
    }

}
