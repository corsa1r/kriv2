<?php

namespace CONTROLLERS\SYS;

class TestLoginController extends \CORE\BaseController {

    protected $_dependency = Array(
        'Session:logged-admin',
        'InputHandler'
    );
    private $userPost = null;

    public function __construct() {
        parent::__construct();
    }

    public function login() {
        if (!$this->_session->get('user')) {
            if ($this->_inputHandler->post->data->username == "asd" && $this->_inputHandler->post->data->password == "dsa") {

                $access_token = sha1(rand(-1024, 2048) . time());

                $response = Array(
                    'access_token' => $access_token
                );

                $this->_session->set('user', $response);

                return (object) $response;
            }
        }

        return $this->_session->get('user');
    }

}
