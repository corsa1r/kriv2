<?php

namespace CONTROLLERS\SYS;

class AdminController extends \CORE\BaseController {

    protected $_dependency = Array(
        'Session:logged-admin',
        'InputHandler'
    );

    public function __construct() {
        parent::__construct();
    }

    public function login() {
        if(!$this->_session->get('admin')){
            if(isset($this->_inputHandler->post->data->username, $this->_inputHandler->post->data->password)){

                $adminModel = new \MODELS\SYS\AdminModel();

                $admin = $adminModel->getAdminCredentials($this->_inputHandler->post->data->username, $this->_inputHandler->post->data->password);
                if($admin){
                    $this->_session->set('admin', $admin);
                }
            }
        }
        
        return $this->_session->get('admin');
    }

    public function add() {
        if($this->_session->get('admin')){
            $adminModel = new \MODELS\SYS\AdminModel();
            $admin = $adminModel->addAdmin($this->_inputHandler->post->data);
            return $admin;
        }
        
        return $this->invalidSession();
    }

    public function edit($id) {
        if($this->_session->get('admin')){
            $adminModel = new \MODELS\SYS\AdminModel();
            $admin = $adminModel->editAdmin($id, $this->_inputHandler->post->data);

            return $admin;
        }

        return $this->invalidSession();
    }

    public function delete($id) {
        if($this->_session->get('admin')){
            $adminModel = new \MODELS\SYS\AdminModel();
            $delete = $adminModel->deleteAdmin($id);

            return $delete;
        }

        return $this->invalidSession();
    }

    public function usernameExist() {
        if($this->_session->get('admin')){
            if(isset($this->_inputHandler->post->data->username)){

                $adminModel = new \MODELS\SYS\AdminModel();

                $usernameExist = $adminModel->isUsernameExist($this->_inputHandler->post->data->username);

                return array('usernameExist' => $usernameExist);
            }
        }

        return $this->invalidSession();
    }

    public function getAll($isFullInfo = false) {
        if($this->_session->get('admin')){
            $adminModel = new \MODELS\SYS\AdminModel();
            $admins = $adminModel->getAllAdmins($isFullInfo);
            return $admins;
        }
        
        return $this->invalidSession();
    }

    public function getAdminById($id, $isFullInfo = false) {
        if($this->_session->get('admin')){
            $adminModel = new \MODELS\SYS\AdminModel();
            $admin = $adminModel->getAdminById($id, $isFullInfo);
            return $admin;
        }
        
        return $this->invalidSession();
    }

    public function logout() {
        return $this->_session->clean();
    }

}
