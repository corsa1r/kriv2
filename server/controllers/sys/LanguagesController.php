<?php

/**
 * Description of LanguageController
 *
 * @author petrov
 */

namespace CONTROLLERS\SYS;

class LanguagesController extends \CORE\BaseController {

    protected $_dependency = Array(
        'InputHandler',
        'Session:logged-admin',
    );

    public function __construct() {
        parent::__construct();
    }

    public function getAll($active = 'true') {
        if (!$this->_session->get('admin')) {
            return $this->invalidSession();
        }

        $langModel = new \MODELS\SYS\LanguagesModel();
        return $langModel->getLanguages($active == 'true' ? true : false);
    }

    public function getDefault() {
        if (!$this->_session->get('admin')) {
            return $this->invalidSession();
        }

        $langModel = new \MODELS\SYS\LanguagesModel();
        return $langModel->getDefaultLanguage();
    }

    public function get($id) {
        if (!$this->_session->get('admin')) {
            return $this->invalidSession();
        }

        $langModel = new \MODELS\SYS\LanguagesModel();
        return $langModel->getLanguageById($id);
    }

    public function update() {
        if (!$this->_session->get('admin')) {
            return $this->invalidSession();
        }

        $post = $this->_inputHandler->post->data;
        $langModel = new \MODELS\SYS\LanguagesModel();

        return array(
            'state' => (bool) $langModel->updateLanguage($post)
        );
    }

    public function insert() {
        if (!$this->_session->get('admin')) {
            return $this->invalidSession();
        }

        $post = $this->_inputHandler->post->data;
        $langModel = new \MODELS\SYS\LanguagesModel();

        return array(
            'state' => (bool) $langModel->insertLanguage($post)
        );
    }

    public function changeStatus() {
        if (!$this->_session->get('admin')) {
            return $this->invalidSession();
        }

        $post = $this->_inputHandler->post->data;
        $langModel = new \MODELS\SYS\LanguagesModel();

        return array(
            'state' => (bool) $langModel->changeLangStatus($post)
        );
    }

    public function delete($id) {
        if (!$this->_session->get('admin')) {
            return $this->invalidSession();
        }

        $langModel = new \MODELS\SYS\LanguagesModel();
        return array(
            'state' => (bool) $langModel->deleteLanguage($id)
        );
    }
    
    public function getTranslate($langCode) {
        $result = [];
        
        $translatesModel = new \MODELS\SYS\TranslationsModel();
        
        foreach ($translatesModel->getTranslationByLanguage($langCode) as $translation) {
            $result[$translation->key] = $translation->translation;
        }
        
        return $result;
    }
}