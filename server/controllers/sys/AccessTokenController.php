<?php

namespace CONTROLLERS\SYS;

class AccessTokenController extends \CORE\BaseController {

    protected $_dependency = Array(
        'Session:user.access_tokens'
    );

    public function __construct() {
        parent::__construct();

        $this->set('admin.access_token');
    }

    public function get($access_token_key) {
        return $this->_session->get($access_token_key);
    }

    public function set($access_token_key) {
        //Fully random access_token
        $access_token = sha1(md5(time()) . $access_token_key . rand(-1024, 1024));
        $this->_session->set($access_token_key, 'access_token.' . $access_token);
        return $this;
    }

}
