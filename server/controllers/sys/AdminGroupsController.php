<?php

namespace CONTROLLERS\SYS;

class AdminGroupsController extends \CORE\BaseController {

    protected $_dependency = Array(
        'Session:logged-admin',
        'InputHandler'
    );

    public function __construct() {
        parent::__construct();
    }

    public function add() {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        if ($this->_session->get('admin')) {
            if (isset($this->_inputHandler->post->data->name, $this->_inputHandler->post->data->accessIds)) {

                $adminGroupsModel = new \MODELS\SYS\AdminGroupsModel();

                $group = $adminGroupsModel->addGroup($this->_inputHandler->post->data);

                return $group;
            }
        }

        return false;
    }

    public function edit($id) {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        $adminGroupsModel = new \MODELS\SYS\AdminGroupsModel();
        $group = $adminGroupsModel->editGroup($id, $this->_inputHandler->post->data);

        return $group;
    }

    public function delete($id) {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        if ($this->_session->get('admin')) {
            $adminGroupsModel = new \MODELS\SYS\AdminGroupsModel();
            $delete = $adminGroupsModel->deleteGroup($id);
            if($delete){
                $adminModel = new \MODELS\SYS\AdminModel();
                $adminModel->changeAdminsGroup($id, 0);
            }

            return $delete;
        }

        return false;
    }

    public function getAll() {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        if ($this->_session->get('admin')) {
            $adminGroupsModel = new \MODELS\SYS\AdminGroupsModel();
            $groups = $adminGroupsModel->getAllGroups();
            return $groups;
        }
    }

    public function getGroupById($id) {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        if ($this->_session->get('admin')) {
            $adminGroupsModel = new \MODELS\SYS\AdminGroupsModel();
            $group = $adminGroupsModel->getGroupById($id);
            return $group;
        }
    }
}
