<?php

namespace CONTROLLERS\SYS;

class ConfigurationController extends \CORE\BaseController {

    protected $_dependency = Array(
        'Session:logged-admin',
        'InputHandler'
    );

    public function __construct() {
        parent::__construct();
    }

    public function set($key) {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        $configData = \MODELS\SYS\ConfigModel::getInstance()->set($key, $this->_inputHandler->post->data->value);
        return $configData;
    }

    public function delete($key) {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        $configData = \MODELS\SYS\ConfigModel::getInstance()->delete($key);
        return $configData;
    }

    public function getAll($extendInfo = false) {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        $configData = \MODELS\SYS\ConfigModel::getInstance()->getAll($extendInfo);
        return $configData;
    }

    public function get($key = null, $toJson = false, $extendInfo = false) {
        if(isset($key)){
            $configData = \MODELS\SYS\ConfigModel::getInstance()->get($key, $toJson, $extendInfo);
            return $configData;
        }
    }

}
