<?php

namespace CONTROLLERS\SYS;

class AnalyticsController extends \CORE\BaseController {

    protected $_dependency = Array(
        'Session:logged-admin',
        'InputHandler',
        'Client'
    );

    public function __construct() {
        parent::__construct();
    }

    public function insert() {
        $analyticsModel = new \MODELS\SYS\AnalyticsModel();
        return $analyticsModel->addAnalytics($this->_client->ip, $this->_inputHandler->post->data->user_agent);
    }

    public function get($from = 0, $to = null) {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        $analyticsModel = new \MODELS\SYS\AnalyticsModel();
        $result = $analyticsModel->getAnalytics($from, $to);
        return $result;
    }

}
