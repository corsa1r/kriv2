<?php

namespace CONTROLLERS\SYS;

/**
 * Description of CaptchaController
 * This class implements image-captcha generator
 * @requires MODULES/SESSION class
 * @author corsair
 */
class CaptchaController extends \CORE\BaseController {

    private $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    protected $_dependency = Array(
        'Session:captcha'
    );

    public function __construct() {
        parent::__construct();
    }

    private function getCaptchaString() {
        $randomString = '';

        for ($i = 0; $i < 5; $i++) {
            $randomString .= $this->chars[rand(0, strlen($this->chars) - 1)];
        }

        $this->_session->set('code', strtolower($randomString));
        return strtolower($randomString);
    }

    /**
     * Return img with captcha code
     * @param type $width
     * @param type $height
     * 
     * exit
     */
    public function getCaptcha($width = 100, $height = 30) {
        $captcha = imagecreate($width, $height);
        $background = imagecolorallocate($captcha, 255, 255, 255);
        $textColour = imagecolorallocate($captcha, 0, 0, 0);
        
        imagestring($captcha, 5, 10, 7, implode(' ', str_split($this->getCaptchaString())), $textColour);
        imagesetthickness($captcha, 5);
        header("Content-type: image/png");
        imagepng($captcha);
        exit;
    }

}
