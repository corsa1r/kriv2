<?php

namespace CONTROLLERS\SYS;

class ExplorerController extends \CORE\BaseController {

    protected $_dependency = Array(
        'Session:logged-admin',
        'FileTransfer',
        'InputHandler',
    );
    protected $_baseDir = null;
    protected $_copyString = '_copy';

    public function __construct() {
        parent::__construct();

        $this->_baseDir = BASE_PATH . DS . '..' . DS;
    }

    private function recurse_copy($src, $dst) {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        $dir = opendir($src);
        if($dir === false){
            return false;
        }

        mkdir($dst);
        while (false !== ( $file = readdir($dir))){
            if(( $file != '.' ) && ( $file != '..' )){
                if(is_dir($src . DS . $file)){
                    $this->recurse_copy($src . DS . $file, $dst . DS . $file);
                } else{
                    copy($src . DS . $file, $dst . DS . $file);
                }
            }
        }
        closedir($dir);

        return true;
    }

    public function isWritable($filename) {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        return is_writable($filename);
    }

    public function getDir() {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        

        $dir = isset($this->_GET['dir']) ? $this->_baseDir . urldecode($this->_GET['dir']) : $this->_baseDir;

        if(is_dir($dir)){
            return array('isWritableDir' => $this->isWritable($dir), 'files' => \MODULES\DirectoryScanner::scanDirAssoc($dir));
        }

        return null;
    }

    public function create() {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        $item = $this->_inputHandler->post->data;

        $created = false;
        $message = '';

        switch ($item->type){
            case 'file':
                if(file_exists($this->_baseDir . $item->path)){
                    $message = 'File already exist!';
                } else{
                    $created = touch($this->_baseDir . $item->path);
                }
                break;
            case 'folder':
                if(is_dir($this->_baseDir . $item->path)){
                    $message = 'Folder exist!';
                } else{
                    $created = mkdir($this->_baseDir . $item->path);
                }
                break;
            default:
                break;
        }

        return array('created' => $created, 'errorMessage' => $message);
    }

    public function delete() {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        $items = $this->_inputHandler->post->data->items;

        $result = array();

        foreach ($items as $item){
            $deleted = false;

            switch ($item->type){
                case 'file':
                    $deleted = unlink($this->_baseDir . $item->path);
                    break;
                case 'folder':
                    $deleted = rmdir($this->_baseDir . $item->path);
                    break;
                default:
                    break;
            }

            $result[$item->path] = $deleted;
        }

        return $result;
    }

    public function rename() {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        $item = $this->_inputHandler->post->data;

        $renamed = false;
        $message = '';

        switch ($item->type){
            case 'file':
                if($this->_fileTransfer->isFileExist($this->_baseDir, $item->new_name)){
                    $message = 'File already exist in this directory!';
                } else{
                    $renamed = rename($this->_baseDir . $item->old_name, $this->_baseDir . $item->new_name);
                }
                break;
            case 'folder':
                if(is_dir($this->_baseDir . $item->new_name)){
                    $message = 'Directory with that name already exist!';
                } else{
                    $renamed = rename($this->_baseDir . $item->old_name, $this->_baseDir . $item->new_name);
                }
                break;
            default:
                break;
        }

        return array('renamed' => $renamed, 'errorMessage' => $message);
    }

    public function copy() {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        $items = $this->_inputHandler->post->data->items;

        $result = array();

        foreach ($items as $item){
            $copied = false;

            switch ($item->type){
                case 'file':
                    $info = pathinfo($this->_baseDir . $item->path);
                    $info['dirname'] = dirname($item->path);
                    $info['extension'] = isset($info['extension']) ? '.' . $info['extension'] : '';
                    $counter = 0;
                    while ($this->_fileTransfer->isFileExist($this->_baseDir . $info['dirname'] . DS, $info['filename'] . $this->_copyString . ($counter > 0 ? $counter : '') . $info['extension'])){
                        $counter++;
                    }

                    $copied = copy($this->_baseDir . $item->path, $this->_baseDir . $info['dirname'] . DS . $info['filename'] . $this->_copyString . ($counter > 0 ? $counter : '') . $info['extension']);
                    break;
                case 'folder':
                    $counter = 0;
                    while (is_dir($this->_baseDir . $item->path . $this->_copyString . ($counter > 0 ? $counter : ''))){
                        $counter++;
                    }

                    $copied = $this->recurse_copy($this->_baseDir . $item->path, $this->_baseDir . $item->path . $this->_copyString . ($counter > 0 ? $counter : ''));
                    break;
                default:
                    break;
            }

            $result[$item->path] = $copied;
        }

        return $result;
    }

    public function upload() {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        $response = array(
            'uploaded' => false,
            'errorMessage' => 'Something went wrong while uploading the file'
        );
        $path = $this->_inputHandler->post->data['path'];
        $action = isset($this->_inputHandler->post->data['action']) ? $this->_inputHandler->post->data['action'] : 'error';
        $file = $this->_fileTransfer->setDestination($path)->setFile('file');
        if($this->_fileTransfer->isFileExist($path . DS, $file->getFilename())){
            switch ($action){
                case 'leaveBoth':
                    $info = pathinfo($path . DS . $file->getFilename());
                    $info['dirname'] = dirname($path);
                    $info['extension'] = isset($info['extension']) ? '.' . $info['extension'] : '';
                    $counter = 1;
                    do{
                        $file->rename($info['filename'] . '_' . $counter);
                    } while ($this->_fileTransfer->isFileExist($path . DS, $info['filename'] . '_' . $counter++ . $info['extension']));
                    break;
                case 'replace':
                    break;
                default:
                    $response['errorMessage'] = 'File with name "' . $file->getFilename() . '" already exist!';
                    return $response;
            }
        }

        if($file->upload()){
            $response['uploaded'] = true;
            $response['errorMessage'] = '';
        }

        return $response;
    }

}
