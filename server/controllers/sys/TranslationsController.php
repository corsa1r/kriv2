<?php

/**
 * @author Vladimir Enchev <vladimir.enchev@interactive-share.com>
 * @time 2014-10-9
 */

namespace CONTROLLERS\SYS;

class TranslationsController extends \CORE\BaseController {

    private $translationsModel = null;
    private $languagesModel = null;
    protected $_dependency = Array(
        'Session:logged-admin',
        'InputHandler'
    );

    public function __construct() {
        parent::__construct();

        $this->translationsModel = new \MODELS\SYS\TranslationsModel();
        $this->languagesModel = new \MODELS\SYS\LanguagesModel();
    }

    public function getTranslate($langCode) {

        if (!$this->_session->get('admin')) {
            return $this->invalidSession();
        }

        return $this->translationsModel->getTranslationByLanguage($langCode);
    }

    public function getTranslates() {

        if (!$this->_session->get('admin')) {
            return $this->invalidSession();
        }

        $result = Array();

        foreach ($this->languagesModel->getLanguages(true) as $language) {

            $resultObject = new \stdClass();

            $resultObject->language = $language;
            $resultObject->translations = $this->translationsModel->getTranslationByLanguage($language->short);

            $result[] = $resultObject;
        }

        return $result;
    }

    public function updateRow() {
        if (!$this->_session->get('admin')) {
            return $this->invalidSession();
        }

        $response = new \stdClass();
        $response->state = $this->translationsModel->updateRow($this->_inputHandler->post->data);

        return $response;
    }

    public function addKey() {
        if (!$this->_session->get('admin')) {
            return $this->invalidSession();
        }

        $response = new \stdClass();
        $response->state = $this->translationsModel->addKey($this->_inputHandler->post->data);

        return $response;
    }

}
