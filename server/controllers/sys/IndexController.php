<?php

namespace CONTROLLERS\SYS;

class IndexController extends \CORE\BaseController {

    protected $_dependency = array(
        'Session:user',
        'Mailer',
        'PhpExcel',
        'PdfGenerator'
    );

    public function __construct() {
        parent::__construct();
    }

    public function getXls() {
        // Create new PHPExcel object
        $objPHPExcel = $this->_phpExcel->getInstance();

        $objPHPExcel->getProperties()->setTitle("Kriv2 framewrok - " . date('d.m.Y'));

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Text on A1')
                ->setCellValue('B1', 'Text on B1')
                ->setCellValue('C1', 'Text on C1');

        $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true);

        $dataArray = array(
            'Text From Array on A2',
            'Text From Array on B2',
            'Text From Array on C2',
        );

        $objPHPExcel->getActiveSheet()->fromArray($dataArray, NULL, 'A2');

        $objPHPExcel->getActiveSheet()->setTitle('Kriv2 title');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        foreach (range('A', 'C') as $columnID){
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }

// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $objPHPExcel->getProperties()->getTitle() . '.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    public function page() {
        return Array(
            (object) Array(
                "name" => "item 1",
                "age" => 33
            ),
            (object) Array(
                "name" => "item 2",
                "age" => 30
            ),
            (object) Array(
                "name" => "item 3",
                "age" => 57
            ),
            (object) Array(
                "name" => "item 4",
                "age" => 0.5
            )
        );
    }

    public function sendMail() {
        $message = $this->_mailer->createMessage();
        $message->setSubject('Заглавие!')
                ->setBcc(array('vasil.bogoev@interactive-share.com'))
                ->setBody('<p>Здравей!<br /> Това е съобщение от Kriv2!</p>');
        
        echo '<pre>';
        print_r($this->_mailer->send($message));
        echo '</pre>';
    }
    
    public function getPdf() {
     
        $css = 'h1{ color: red; } div{ width: 100px; height: 100px; border: 1px solid black; background-color: red; text-align: center; color: white;}';
        $html = '<h1>Welcome Sahip!</h1><br /><div>Test</div><br /><img src="https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xaf1/t1.0-1/c25.58.414.414/s50x50/269694_1798649974796_3861517_n.jpg" />';
        
        $this->_pdfGenerator->addExternalCSS($css)->generateFromHtml($html)->output('Test.pdf');
    }

}
