<?php

class Upload_Storage_FileSystem implements Upload_StorageInterface
{
    /**
     * Path to upload destination directory (with trailing slash)
     * @var string
     */
    protected $directory;

    /**
     * Overwrite existing files?
     * @var bool
     */
    protected $overwrite;

    /**
     * Constructor
     *
     * @param  string                    $directory Relative or absolute path to upload directory
     * @param  bool                      $overwrite Should this overwrite existing files?
     * @throws InvalidArgumentException            If directory does not exist
     * @throws InvalidArgumentException            If directory is not writable
     */
    public function __construct($directory = null, $createIfNotExist = false, $overwrite = false)
    {
        if(!is_null($directory)){
            $this->directory = $this->setDirectory($directory, $createIfNotExist);
        }
        $this->overwrite = $this->setOverwrite($overwrite);
    }

    /**
     * Upload
     *
     * @param  Upload_FileInfoInterface $file The file object to upload
     * @throws Upload_Exception               If overwrite is false and file already exists
     * @throws Upload_Exception               If error moving file to destination
     */
    public function upload(Upload_FileInfoInterface $fileInfo)
    {
        if(!is_null($this->directory)){
        $destinationFile = $this->directory . $fileInfo->getNameWithExtension();
        if ($this->overwrite === false && file_exists($destinationFile) === true) {
            throw new Upload_Exception('File already exists', $fileInfo);
        }

        if ($this->moveUploadedFile($fileInfo->getPathname(), $destinationFile) === false) {
            throw new Upload_Exception('File could not be moved to final destination.', $fileInfo);
        }
        } else{
            throw new Upload_Exception('You must set directory path befor uploading a file!');
        }
    }

    /**
     * Move uploaded file
     *
     * This method allows us to stub this method in unit tests to avoid
     * hard dependency on the `move_uploaded_file` function.
     *
     * @param  string $source      The source file
     * @param  string $destination The destination file
     * @return bool
     */
    protected function moveUploadedFile($source, $destination)
    {
        return move_uploaded_file($source, $destination);
    }
    
    public function getOverwrite() {
        return $this->overwrite;
    }
    
    public function getDirectory() {
        return $this->directory;
    }
    
    public function setOverwrite($overwrite) {
        $this->overwrite = (bool)$overwrite;
        return $this;
    }
    
    public function setDirectory($directory, $createIfNotExist = false) {
        if (!is_dir($directory)) {
            if(!($createIfNotExist && mkdir($directory, 0777, true))){
                throw new InvalidArgumentException('Directory "' . $directory . '" does not exist');
            }
        }
        if (!is_writable($directory)) {
            throw new InvalidArgumentException('Directory is not writable');
        }
        
        $this->directory = rtrim($directory, '/') . DIRECTORY_SEPARATOR;
        return $this;
    }
}
