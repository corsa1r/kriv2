<?php

interface Upload_ValidationInterface
{
    /**
     * Validate file
     *
     * This method is responsible for validating an `\Upload\FileInfoInterface` instance.
     * If validation fails, an exception should be thrown.
     *
     * @param  Upload_FileInfoInterface $fileInfo
     * @throws Eception                If validation fails
     */
    public function validate(Upload_FileInfoInterface $fileInfo);
}
