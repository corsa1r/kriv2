<?php

class Upload_Exception extends \RuntimeException
{
    /**
     * @var Upload_FileInfoInterface
     */
    protected $fileInfo;

    /**
     * Constructor
     *
     * @param string                    $message  The Exception message
     * @param Upload_FileInfoInterface $fileInfo The related file instance
     */
    public function __construct($message, Upload_FileInfoInterface $fileInfo = null)
    {
        $this->fileInfo = $fileInfo;

        parent::__construct($message);
    }

    /**
     * Get related file
     *
     * @return Upload_FileInfoInterface
     */
    public function getFileInfo()
    {
        return $this->fileInfo;
    }
}
