<?php

class Upload_Validation_Mimetype implements Upload_ValidationInterface
{
    /**
     * Valid media types
     * @var array
     */
    protected $mimetypes;

    /**
     * Constructor
     *
     * @param string|array $mimetypes
     */
    public function __construct($mimetypes)
    {
        if (is_string($mimetypes) === true) {
            $mimetypes = array($mimetypes);
        }
        $this->mimetypes = $mimetypes;
    }

    /**
     * Validate
     *
     * @param  Upload_FileInfoInterface  $fileInfo
     * @throws RuntimeException          If validation fails
     */
    public function validate(Upload_FileInfoInterface $fileInfo)
    {
        if (in_array($fileInfo->getMimetype(), $this->mimetypes) === false) {
            throw new Upload_Exception(sprintf('Invalid mimetype. Must be one of: %s', implode(', ', $this->mimetypes)), $fileInfo);
        }
    }
}
