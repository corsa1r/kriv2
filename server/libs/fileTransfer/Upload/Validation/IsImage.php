<?php

class Upload_Validation_IsImage extends Upload_Validation_Mimetype
{
    /**
     * Valid media types
     * @var array
     */
    // http://de.wikipedia.org/wiki/Liste_von_Dateiendungen
    // http://www.iana.org/assignments/media-types/image/
    protected $mimetypes = array(
            'application/cdf',
            'application/dicom',
            'application/fractals',
            'application/postscript',
            'application/vnd.hp-hpgl',
            'application/vnd.oasis.opendocument.graphics',
            'application/x-cdf',
            'application/x-cmu-raster',
            'application/x-ima',
            'application/x-inventor',
            'application/x-koan',
            'application/x-portable-anymap',
            'application/x-world-x-3dmf',
            'image/bmp',
            'image/c',
            'image/cgm',
            'image/fif',
            'image/gif',
            'image/jpeg',
            'image/jpm',
            'image/jpx',
            'image/jp2',
            'image/naplps',
            'image/pjpeg',
            'image/png',
            'image/svg',
            'image/svg+xml',
            'image/tiff',
            'image/vnd.adobe.photoshop',
            'image/vnd.djvu',
            'image/vnd.fpx',
            'image/vnd.net-fpx',
            'image/x-cmu-raster',
            'image/x-cmx',
            'image/x-coreldraw',
            'image/x-cpi',
            'image/x-emf',
            'image/x-ico',
            'image/x-icon',
            'image/x-jg',
            'image/x-ms-bmp',
            'image/x-niff',
            'image/x-pict',
            'image/x-pcx',
            'image/x-portable-anymap',
            'image/x-portable-bitmap',
            'image/x-portable-greymap',
            'image/x-portable-pixmap',
            'image/x-quicktime',
            'image/x-rgb',
            'image/x-tiff',
            'image/x-unknown',
            'image/x-windows-bmp',
            'image/x-xpmi',
        );
    
    /**
     * Constructor
     *
     * @param string|array $mimetypes
     */
    public function __construct($mimetypes = array())
    {
        if (count($mimetypes) > 0) {
            $this->mimetypes = $mimetypes;
        }
        
    }

    /**
     * Validate
     *
     * @param  Upload_FileInfoInterface  $fileInfo
     * @throws RuntimeException          If validation fails
     */
    public function validate(Upload_FileInfoInterface $fileInfo)
    {
        if (in_array($fileInfo->getMimetype(), $this->mimetypes) === false) {
            throw new Upload_Exception(sprintf('Invalid file. Must be an image.'), $fileInfo);
        }
    }
}
