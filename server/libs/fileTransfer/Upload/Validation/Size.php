<?php

class Upload_Validation_Size implements Upload_ValidationInterface
{
    /**
     * Minimum acceptable file size (bytes)
     * @var int
     */
    protected $minSize;

    /**
     * Maximum acceptable file size (bytes)
     * @var int
     */
    protected $maxSize;

    /**
     * Constructor
     *
     * @param int $maxSize Maximum acceptable file size in bytes (inclusive)
     * @param int $minSize Minimum acceptable file size in bytes (inclusive)
     */
    public function __construct($maxSize, $minSize = 0)
    {
        if (is_string($maxSize)) {
            $maxSize = Upload_File::humanReadableToBytes($maxSize);
        }
        $this->maxSize = $maxSize;

        if (is_string($minSize)) {
            $minSize = Upload_File::humanReadableToBytes($minSize);
        }
        $this->minSize = $minSize;
    }

    /**
     * Validate
     *
     * @param  Upload_FileInfoInterface  $fileInfo
     * @throws RuntimeException          If validation fails
     */
    public function validate(Upload_FileInfoInterface $fileInfo)
    {
        $fileSize = $fileInfo->getSize();

        if ($fileSize < $this->minSize) {
            throw new Upload_Exception('File size is too small. Must be greater than or equal to: ' . Upload_File::bytesToHumanReadable($this->minSize), $fileInfo);
        }

        if ($fileSize > $this->maxSize) {
            throw new Upload_Exception('File size is too large. Must be less than: ' . Upload_File::bytesToHumanReadable($this->maxSize), $fileInfo);
        }
    }
}
