<?php

class Upload_Validation_Extension implements Upload_ValidationInterface
{
    /**
     * Array of acceptable file extensions without leading dots
     * @var array
     */
    protected $allowedExtensions;

    /**
     * Constructor
     *
     * @param string|array $allowedExtensions Allowed file extensions
     * @example new Upload_ValidationExtension(array('png','jpg','gif'))
     * @example new Upload_ValidationExtension('png')
     */
    public function __construct($allowedExtensions)
    {
        if (is_string($allowedExtensions) === true) {
            $allowedExtensions = array($allowedExtensions);
        }

        $this->allowedExtensions = array_map('strtolower', $allowedExtensions);
    }

    /**
     * Validate
     *
     * @param  Upload_FileInfoInterface $fileInfo
     * @throws RuntimeException         If validation fails
     */
    public function validate(Upload_FileInfoInterface $fileInfo)
    {
        $fileExtension = strtolower($fileInfo->getExtension());

        if (in_array($fileExtension, $this->allowedExtensions) === false) {
            throw new Upload_Exception(sprintf('Invalid file extension. Must be one of: %s', implode(', ', $this->allowedExtensions)), $fileInfo);
        }
    }
}
