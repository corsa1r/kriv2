<?php

class Upload_Validation_ImageSize implements Upload_ValidationInterface {

    /**
     * @const string Error constants
     */
    const WIDTH_TOO_BIG = 'fileImageSizeWidthTooBig';
    const WIDTH_TOO_SMALL = 'fileImageSizeWidthTooSmall';
    const HEIGHT_TOO_BIG = 'fileImageSizeHeightTooBig';
    const HEIGHT_TOO_SMALL = 'fileImageSizeHeightTooSmall';
    const NOT_DETECTED = 'fileImageSizeNotDetected';

    /**
     * @var array Error message template
     */
    protected $_messageTemplates = array(
        self::WIDTH_TOO_BIG => "Maximum allowed width is '%maxwidth%px' but '%width%px' detected",
        self::WIDTH_TOO_SMALL => "Minimum expected width is '%minwidth%px' but '%width%px' detected",
        self::HEIGHT_TOO_BIG => "Maximum allowed height is '%maxheight%px' but '%height%px' detected",
        self::HEIGHT_TOO_SMALL => "Minimum expected height is '%minheight%px' but '%height%px' detected",
        self::NOT_DETECTED => "The size of image could not be detected"
    );

    /**
     * @var array Error message template variables
     */
    protected $_messageVariables = array(
        'minwidth' => '_minwidth',
        'maxwidth' => '_maxwidth',
        'minheight' => '_minheight',
        'maxheight' => '_maxheight',
        'width' => '_width',
        'height' => '_height'
    );

    /**
     * Minimum image width
     *
     * @var integer
     */
    protected $_minwidth;

    /**
     * Maximum image width
     *
     * @var integer
     */
    protected $_maxwidth;

    /**
     * Minimum image height
     *
     * @var integer
     */
    protected $_minheight;

    /**
     * Maximum image height
     *
     * @var integer
     */
    protected $_maxheight;

    /**
     * Detected width
     *
     * @var integer
     */
    protected $_width;

    /**
     * Detected height
     *
     * @var integer
     */
    protected $_height;

    /**
     * Sets validator options
     *
     * Accepts the following option keys:
     * - minheight
     * - minwidth
     * - maxheight
     * - maxwidth
     *
     * @param  Zend_Config|array $options
     * @return void
     */
    public function __construct($options) {
        if(!is_array($options)){
            throw new RuntimeException('Invalid options to validator ImageSize provided');
        }

        if(isset($options['minheight']) || isset($options['minwidth'])){
            $this->setImageMin($options);
        }

        if(isset($options['maxheight']) || isset($options['maxwidth'])){
            $this->setImageMax($options);
        }
    }

    /**
     * Returns the set minimum image sizes
     *
     * @return array
     */
    public function getImageMin() {
        return array('minwidth' => $this->_minwidth, 'minheight' => $this->_minheight);
    }

    /**
     * Returns the set maximum image sizes
     *
     * @return array
     */
    public function getImageMax() {
        return array('maxwidth' => $this->_maxwidth, 'maxheight' => $this->_maxheight);
    }

    /**
     * Returns the set image width sizes
     *
     * @return array
     */
    public function getImageWidth() {
        return array('minwidth' => $this->_minwidth, 'maxwidth' => $this->_maxwidth);
    }

    /**
     * Returns the set image height sizes
     *
     * @return array
     */
    public function getImageHeight() {
        return array('minheight' => $this->_minheight, 'maxheight' => $this->_maxheight);
    }

    /**
     * Sets the minimum image size
     */
    public function setImageMin($options) {
        if(isset($options['minwidth'])){
            if(($this->_maxwidth !== null) and ( $options['minwidth'] > $this->_maxwidth)){
                throw new RuntimeException("The minimum image width must be less than or equal to the "
                . " maximum image width, but {$options['minwidth']} > {$this->_maxwidth}");
            }
        }

        if(isset($options['maxheight'])){
            if(($this->_maxheight !== null) and ( $options['minheight'] > $this->_maxheight)){
                throw new RuntimeException("The minimum image height must be less than or equal to the "
                . " maximum image height, but {$options['minheight']} > {$this->_maxheight}");
            }
        }

        if(isset($options['minwidth'])){
            $this->_minwidth = (int) $options['minwidth'];
        }

        if(isset($options['minheight'])){
            $this->_minheight = (int) $options['minheight'];
        }

        return $this;
    }

    /**
     * Sets the maximum image size
     */
    public function setImageMax($options) {
        if(isset($options['maxwidth'])){
            if(($this->_minwidth !== null) and ( $options['maxwidth'] < $this->_minwidth)){
                throw new RuntimeException("The maximum image width must be greater than or equal to the "
                . "minimum image width, but {$options['maxwidth']} < {$this->_minwidth}");
            }
        }

        if(isset($options['maxheight'])){
            if(($this->_minheight !== null) and ( $options['maxheight'] < $this->_minheight)){
                throw new RuntimeException("The maximum image height must be greater than or equal to the "
                . "minimum image height, but {$options['maxheight']} < {$this->_minwidth}");
            }
        }

        if(isset($options['maxwidth'])){
            $this->_maxwidth = (int) $options['maxwidth'];
        }
        if(isset($options['maxheight'])){
            $this->_maxheight = (int) $options['maxheight'];
        }

        return $this;
    }

    /**
     * Sets the mimimum and maximum image width
     *
     * @param  array $options               The image width dimensions
     */
    public function setImageWidth($options) {
        $this->setImageMin($options);
        $this->setImageMax($options);

        return $this;
    }

    /**
     * Sets the mimimum and maximum image height
     *
     * @param  array $options               The image height dimensions
     */
    public function setImageHeight($options) {
        $this->setImageMin($options);
        $this->setImageMax($options);

        return $this;
    }

    public function validate(Upload_FileInfoInterface $fileInfo) {
        $value = $fileInfo->getPathname();

        $size = @getimagesize($value);

        if(empty($size) or ( $size[0] === 0) or ( $size[1] === 0)){
            throw new Upload_Exception($this->_throw(self::NOT_DETECTED), $fileInfo);
        }

        $this->_width = $size[0];
        $this->_height = $size[1];
        if($this->_width < $this->_minwidth){
            throw new Upload_Exception(self::NOT_DETECTED, $fileInfo);
        }

        if(($this->_maxwidth !== null) and ( $this->_maxwidth < $this->_width)){
            throw new Upload_Exception($this->_throw(self::WIDTH_TOO_BIG), $fileInfo);
        }

        if($this->_height < $this->_minheight){
            throw new Upload_Exception($this->_throw(self::HEIGHT_TOO_SMALL), $fileInfo);
        }

        if(($this->_maxheight !== null) and ( $this->_maxheight < $this->_height)){
            throw new Upload_Exception($this->_throw(self::HEIGHT_TOO_BIG), $fileInfo);
        }
    }
    
    public function _throw($message) {
        $error = $this->_messageTemplates[$message];
        
        return strtr($error, array(
           '%width%' => $this->_width, 
           '%height%' => $this->_height, 
           '%maxwidth%' => $this->_maxwidth, 
           '%maxheight%' => $this->_maxheight, 
           '%minwidth%' => $this->_minwidth, 
           '%minheight%' => $this->_minheight, 
        ));
    }

}
