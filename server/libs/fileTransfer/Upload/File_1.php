<?php

class Upload_File implements ArrayAccess, IteratorAggregate, Countable
{
    /**
     * Upload error code messages
     * @var array
     */
    protected static $errorCodeMessages = array(
        1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
        2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
        3 => 'The uploaded file was only partially uploaded',
        4 => 'No file was uploaded',
        6 => 'Missing a temporary folder',
        7 => 'Failed to write file to disk',
        8 => 'A PHP extension stopped the file upload'
    );

    /**
     * Storage delegate
     * @var Upload_StorageInterface
     */
    protected $storage;

    /**
     * File information
     * @var array[Upload_FileInfoInterface]
     */
    protected $objects = array();

    /**
     * Validators
     * @var array[Upload_ValidatorInterface]
     */
    protected $validators = array();

    /**
     * Validator errors
     * @var array[String]
     */
    protected $errors = array();

    /**
     * Before validator callback
     * @var callable
     */
    protected $beforeValidatorCallback;

    /**
     * After validator callback
     * @var callable
     */
    protected $afterValidatorCallback;

    /**
     * Before upload callback
     * @var callable
     */
    protected $beforeUploadCallback;

    /**
     * After upload callback
     * @var callable
     */
    protected $afterUploadCallback;

    /**
     * Constructor
     *
     * @param  string                    $key     The $_FILES[] key
     * @param  Upload_StorageInterface  $storage The upload delegate instance
     * @throws RuntimeException                  If file uploads are disabled in the php.ini file
     * @throws InvalidArgumentException          If $_FILES[] does not contain key
     */
    public function __construct($key, Upload_StorageInterface $storage)
    {
        // Check if file uploads are allowed
        if (ini_get('file_uploads') == false) {
            throw new RuntimeException('File uploads are disabled in your PHP.ini file');
        }

        // Check if key exists
        if (isset($_FILES[$key]) === false) {
            throw new InvalidArgumentException("Cannot find uploaded file(s) identified by key: $key");
        }

        // Collect file info
        if (is_array($_FILES[$key]['tmp_name']) === true) {
            foreach ($_FILES[$key]['tmp_name'] as $index => $tmpName) {
                if ($_FILES[$key]['error'][$index] !== UPLOAD_ERR_OK) {
                    $this->errors[] = sprintf(
                        '%s: %s',
                        $_FILES[$key]['name'][$index],
                        static::$errorCodeMessages[$_FILES[$key]['error'][$index]]
                    );
                    continue;
                }

                $this->objects[] = Upload_FileInfo::createFromFactory(
                    $_FILES[$key]['tmp_name'][$index],
                    $_FILES[$key]['name'][$index]
                );
            }
        } else {
            if ($_FILES[$key]['error'] !== UPLOAD_ERR_OK) {
                $this->errors[] = sprintf(
                    '%s: %s',
                    $_FILES[$key]['name'],
                    static::$errorCodeMessages[$_FILES[$key]['error']]
                );
            }

            $this->objects[] = Upload_FileInfo::createFromFactory(
                $_FILES[$key]['tmp_name'],
                $_FILES[$key]['name']
            );
        }

        $this->storage = $storage;
    }

    /********************************************************************************
     * Callbacks
     *******************************************************************************/

    /**
     * Set `beforeValidator` callable
     *
     * @param  callable                  $callable Should accept one `Upload_FileInfoInterface` argument
     * @return Upload_File                        Self
     * @throws InvalidArgumentException           If argument is not a Closure or invokable object
     */
    public function beforeValidate($callable)
    {
        if (is_object($callable) === false || method_exists($callable, '__invoke') === false) {
            throw new InvalidArgumentException('Callback is not a Closure or invokable object.');
        }
        $this->beforeValidator = $callable;

        return $this;
    }

    /**
     * Set `afterValidator` callable
     *
     * @param  callable                  $callable Should accept one `Upload_FileInfoInterface` argument
     * @return Upload_File                        Self
     * @throws InvalidArgumentException           If argument is not a Closure or invokable object
     */
    public function afterValidate($callable)
    {
        if (is_object($callable) === false || method_exists($callable, '__invoke') === false) {
            throw new InvalidArgumentException('Callback is not a Closure or invokable object.');
        }
        $this->afterValidator = $callable;

        return $this;
    }

    /**
     * Set `beforeUpload` callable
     *
     * @param  callable                  $callable Should accept one `Upload_FileInfoInterface` argument
     * @return Upload_File                        Self
     * @throws InvalidArgumentException           If argument is not a Closure or invokable object
     */
    public function beforeUpload($callable)
    {
        if (is_object($callable) === false || method_exists($callable, '__invoke') === false) {
            throw new InvalidArgumentException('Callback is not a Closure or invokable object.');
        }
        $this->beforeUpload = $callable;

        return $this;
    }

    /**
     * Set `afterUpload` callable
     *
     * @param  callable                  $callable Should accept one `Upload_FileInfoInterface` argument
     * @return Upload_File                        Self
     * @throws InvalidArgumentException           If argument is not a Closure or invokable object
     */
    public function afterUpload($callable)
    {
        if (is_object($callable) === false || method_exists($callable, '__invoke') === false) {
            throw new InvalidArgumentException('Callback is not a Closure or invokable object.');
        }
        $this->afterUpload = $callable;

        return $this;
    }

    /**
     * Apply callable
     *
     * @param  string                    $callbackName
     * @param  Upload_FileInfoInterface $file
     * @return Upload_File              Self
     */
    protected function applyCallback($callbackName, Upload_FileInfoInterface $file)
    {
        if (in_array($callbackName, array('beforeValidator', 'afterValidator', 'beforeUpload', 'afterUpload')) === true) {
            if (isset($this->$callbackName) === true) {
                call_user_func_array($this->$callbackName, array($file));
            }
        }
    }

    /********************************************************************************
     * Validator and Error Handling
     *******************************************************************************/

    /**
     * Add file validators
     *
     * @param  array[Upload_ValidatorInterface] $validators
     * @return Upload_File                       Self
     */
    public function addValidators(array $validators)
    {
        foreach ($validators as $validator) {
            $this->addValidator($validator);
        }

        return $this;
    }

    public function addValidator($name, $validator)
    {
        $this->validators[$name] = $validator;

        return $this;
    }

    /**
     * Get file validators
     *
     * @return array[Upload_ValidatorInterface]
     */
    public function getValidators()
    {
        return $this->validators;
    }

    /**
     * Is this collection valid and without errors?
     *
     * @return bool
     */
    public function isValid()
    {
        foreach ($this->objects as $fileInfo) {
            // Before validator callback
            $this->applyCallback('beforeValidator', $fileInfo);

            // Check is uploaded file
            if ($fileInfo->isUploadedFile() === false) {
                $this->errors[] = sprintf(
                    '%s: %s',
                    $fileInfo->getNameWithExtension(),
                    'Is not an uploaded file'
                );
                continue;
            }

            // Apply user validators
            foreach ($this->validators as $validator) {
                try {
                    $validator->validate($fileInfo);
                } catch (Upload_Exception $e) {
                    $this->errors[] = sprintf(
                        '%s: %s',
                        $fileInfo->getNameWithExtension(),
                        $e->getMessage()
                    );
                }
            }

            // After validator callback
            $this->applyCallback('afterValidator', $fileInfo);
        }

        return empty($this->errors);
    }

    /**
     * Get file validator errors
     *
     * @return array[String]
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /********************************************************************************
     * Helper Methods
     *******************************************************************************/

    public function __call($name, $arguments)
    {
        $count = count($this->objects);
        $result = null;

        if ($count) {
            if ($count > 1) {
                $result = array();
                foreach ($this->objects as $object) {
                    $result[] = call_user_func_array(array($object, $name), $arguments);
                }
            } else {
                $result = call_user_func_array(array($this->objects[0], $name), $arguments);
            }
        }

        return $result;
    }

    /********************************************************************************
    * Upload
    *******************************************************************************/

    /**
     * Upload file (delegated to storage object)
     *
     * @return bool
     * @throws Upload_Exception If validator fails
     * @throws Upload_Exception If upload fails
     */
    public function upload()
    {
        if ($this->isValid() === false) {
            throw new Upload_Exception('File validator failed');
        }

        foreach ($this->objects as $fileInfo) {
            $this->applyCallback('beforeUpload', $fileInfo);
            $this->storage->upload($fileInfo);
            $this->applyCallback('afterUpload', $fileInfo);
        }

        return true;
    }

    /********************************************************************************
     * Array Access Interface
     *******************************************************************************/

    public function offsetExists($offset)
    {
        return isset($this->objects[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->objects[$offset]) ? $this->objects[$offset] : null;
    }

    public function offsetSet($offset, $value)
    {
        $this->objects[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->objects[$offset]);
    }

    /********************************************************************************
     * Iterator Aggregate Interface
     *******************************************************************************/

    public function getIterator()
    {
        return new ArrayIterator($this->objects);
    }

    /********************************************************************************
     * Countable Interface
     *******************************************************************************/

    public function count()
    {
        return count($this->objects);
    }

    /********************************************************************************
    * Helpers
    *******************************************************************************/

    /**
     * Convert human readable file size (e.g. "10K" or "3M") into bytes
     *
     * @param  string $input
     * @return int
     */
    public static function humanReadableToBytes($input)
    {
        $number = (int)$input;
        $units = array(
            'b' => 1,
            'k' => 1024,
            'm' => 1048576,
            'g' => 1073741824
        );
        $unit = strtolower(substr($input, -1));
        if (isset($units[$unit])) {
            $number = $number * $units[$unit];
        }

        return $number;
    }
}
