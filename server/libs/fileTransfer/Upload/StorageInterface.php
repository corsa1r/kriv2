<?php

interface Upload_StorageInterface
{
    /**
     * Upload file
     *
     * This method is responsible for uploading an `Upload_FileInfoInterface` instance
     * to its intended destination. If upload fails, an exception should be thrown.
     *
     * @param  Upload_FileInfoInterface $fileInfo
     * @throws Exception                If upload fails
     */
    public function upload(Upload_FileInfoInterface $fileInfo);
}
