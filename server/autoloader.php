<?php

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

define('DS', DIRECTORY_SEPARATOR);
define('BASE_PATH', dirname(__FILE__));

require './modules/DirectoryScanner.php';

$autoloadModules = array('client', 'admin');
$autoloadIncludeDirs = array('controllers', 'factories', 'services', 'directives', '../core');

$module = isset($_GET['module']) ? $_GET['module'] : '';

if(in_array($module, $autoloadModules)) {
    $mainDir = '../' . $module;

    $output = '';
    
    foreach ($autoloadIncludeDirs as $dirName){
        $files = \MODULES\DirectoryScanner::scanDir($mainDir . DS . $dirName, 'js');
        $output .= "\n/* " . ucfirst($dirName) . ' files: */';
        foreach($files as $file){
            $output .= "\n\n/* " . $file . ": */\n";
            $output .= file_get_contents(BASE_PATH . DS . $file);
        }
    }
    
    header('Content-Type: application/javascript');
    echo $output;
    exit;
}