<?php

namespace MODULES;

require 'libs/swift/swift_init.php';

class Mailer {
    private $__accountId = null;
    private $__configs = array();
    private $__requiredConfigs = array(
        'username' => '', 
        'password' => '', 
        'port' => '', 
        'smtp_server' => ''
    );
    private $__optionalConfigs = array(
        'from_email' => 'unknown@unknown.com', 
        'from_name' => 'Unknown', 
        'reply_to_email' => 'unknown@unknown.com', 
        'reply_to_name' => 'Unknown', 
        'encription' => null, 
        'subject' => 'Subject'
    );
    private $__transport = null;
    private $__mailer = null;
    
    public function __construct($params = array()) {
        
        if(is_array($params)){
            $this->__accountId = isset($params[0]) ? $params[0] : 'mailer';
        } else {
            $this->__accountId = $params;
        }
        
        $configsData = \MODELS\SYS\ConfigModel::getInstance()->getGroup($this->__accountId);
        
        $this->__configs = $configsData;

        $missingRequiredConfigs = array_diff_key($this->__requiredConfigs, $this->__configs);
        if(count($missingRequiredConfigs) > 0){
            throw new \Exception('Add config for ' . $this->__accountId . '.' . key($missingRequiredConfigs));
        }
        
        $this->__configs = array_merge($this->__optionalConfigs, $this->__configs);

        $this->__transport = \Swift_SmtpTransport::newInstance($this->__configs['smtp_server'], $this->__configs['port'], $this->__configs['encription'])
                ->setUsername($this->__configs['username'])
                ->setPassword($this->__configs['password']);
        $this->__mailer = \Swift_Mailer::newInstance($this->__transport);
    }
    
    public function getAccountId() {
        return $this->__accountId;
    }
    
    public function createMessage($isHtml = true){
        $message = \Swift_Message::newInstance($this->__configs['subject']);

        if(isset($this->__configs['from_email']) && strlen($this->__configs['from_email']) > 0){
            $message->setFrom($this->__configs['from_email'], $this->__configs['from_name']);
        }
        
        if(isset($this->__configs['reply_to_email']) && strlen($this->__configs['reply_to_email']) > 0){
            $message->setReplyTo($this->__configs['reply_to_email'], $this->__configs['reply_to_name']);
        }

        $message->setContentType($isHtml ? 'text/html' : 'text/plain');
        return $message;
    }
    
    public function send($message) {
        $failedMessages = null;
        $sendMessages = $this->__mailer->send($message, $failedMessages);
        return array('send' => $sendMessages, 'failed' => $failedMessages);
    }
}