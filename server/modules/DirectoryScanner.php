<?php

namespace MODULES;

/*
 * Kriv2 Framework
 */

/**
 * Description of DirectoryScanner
 *
 * @author Vladimir <vladimir.enchev@interactive-share.com>
 * @version 2014-2-10
 */
class DirectoryScanner {
    
    private function __construct() {
        
    }

    public static function scanDir($dir, $extension = null) {
        $arrfiles = array();
        if (is_dir($dir)) {
            if ($handle = opendir($dir)) {
                chdir($dir);
                while (false !== ($file = readdir($handle))) {
                    if(is_file($file) && $extension) {
                        if(self::arrayLastElement(explode(".", $file)) !== $extension) {
                            continue;
                        }
                    }
                    if ($file != "." && $file != "..") {
                        if (is_dir($file)) {
                            $arr = self::scanDir($file, $extension);
                            foreach ($arr as $value) {
                                $arrfiles[] = $dir . DS . $value;
                            }
                        } else {
                            $arrfiles[] = $dir . DS . $file;
                        }
                    }
                }
                chdir("../");
            }
            closedir($handle);
        }
        return $arrfiles;
    }
    
    public static function scanDirAssoc($dir_path) {
        $ritit = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir_path), \RecursiveIteratorIterator::CHILD_FIRST);
        $r = array();
        foreach ($ritit as $splFileInfo) {
            if($splFileInfo->getFilename() != '.' && $splFileInfo->getFilename() != "..") {
                $path = $splFileInfo->isDir() ? array($splFileInfo->getFilename() => array()) : array($splFileInfo->getFilename());

                for ($depth = $ritit->getDepth() - 1; $depth >= 0; $depth--) {
                    $path = array($ritit->getSubIterator($depth)->current()->getFilename() => $path);
                }
                $r = array_merge_recursive($r, $path);
            }
        }
        
        return $r;
    }

    public static function isEmptyDir($dir) {
        return (($files = @scandir($dir)) && count($files) <= 2);
    }
    
    private static function arrayLastElement($arr) {
        return $arr[count($arr) - 1];
    }
}