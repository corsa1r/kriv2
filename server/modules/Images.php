<?php

namespace MODULES;

define('BASEDIR', BASE_PATH . DS . '..' . DS);
define('IMAGES_APP_IMAGES', '' . DS);

/**
 * Description of Images
 *
 * @author petrov
 */
class Images {

    private $_image;
    private $_width;
    private $_height;
    private $_crop = false;
    private $_bw = false;
    private $_suffix = '';
    private $_thumb;
    private $_path = '';
    private $_currentDir = null;

    public function __construct() {
        
    }

    // Return thumbnail based on given width/height
    public function getThumb($image, $width, $height, $crop = false, $bw = false) {
        $this->_image = IMAGES_APP_IMAGES . $image;
        $this->_currentDir = dirname($this->_image);
        //echo '<pre>';print_r($this->_currentDir);echo '</pre>';
        //echo '<pre>';print_r('http://interactive-share.com/facebook_pages/maternea_pregnancy/application/uploads/admin/thumbs/img2_40x40_mc.jpg');echo '</pre>';

        $this->_width = $width;
        $this->_height = $height;
        $this->_crop = $crop;
        $this->_bw = $bw;
        $this->_path = DS . 'thumbs' . DS;

        if ($this->_bw) {
            $this->_suffix = "_bw_" . $this->_width . 'x' . $this->_height . ($this->_crop ? '_' . $this->_crop : '');
        } else {
            $this->_suffix = "_" . $this->_width . 'x' . $this->_height . ($this->_crop ? '_' . $this->_crop : '');
        }

        if ($this->_thumb = $this->_getImageThumb()) {
            return $this->_thumb;
        } elseif ($this->_thumb = $this->_makeThumbnail()) {
            return $this->_thumb;
        } else {
            return IMAGES_APP_IMAGES . 'images' . DS . 'pix.gif';
        }
    }

    private function _makeThumbnail() {
        //ini_set("memory_limit","64M");
        //$o_file = BASEDIR.DS.$this->_currentDir.DS.basename($this->_image);
        $o_file = BASEDIR . DS . $this->_currentDir . DS . basename($this->_image);
        //echo '<pre>';print_r($o_file);echo '</pre>';
        if (!is_file($o_file))
            return "";
        //echo '<pre>';print_r($o_file);echo '</pre>';
        list($width, $height, $type) = getimagesize($o_file);
        switch ($type) {
            case IMAGETYPE_GIF:
                if (imagetypes() & IMG_GIF) { // not the same as IMAGETYPE
                    $o_im = imageCreateFromGIF($o_file);
                } else {
                    $ermsg = 'GIF images are not supported<br />';
                }
                break;
            case IMAGETYPE_JPEG:
                if (imagetypes() & IMG_JPG) {
                    $o_im = imageCreateFromJPEG($o_file);
                } else {
                    $ermsg = 'JPEG images are not supported<br />';
                }
                break;
            case IMAGETYPE_PNG:
                if (imagetypes() & IMG_PNG) {
                    $o_im = imageCreateFromPNG($o_file);
                } else {
                    $ermsg = 'PNG images are not supported<br />';
                }
                break;
            case IMAGETYPE_WBMP:
                if (imagetypes() & IMG_WBMP) {
                    $o_im = imageCreateFromWBMP($o_file);
                } else {
                    $ermsg = 'WBMP images are not supported<br />';
                }
                break;
            default:
                $ermsg = $type . ' images are not supported<br />';
                break;
        }

        if ($this->_bw) {
            imagefilter($o_im, IMG_FILTER_GRAYSCALE);
        }

        if (!isset($ermsg)) {
            if ($width <= $this->_width && $height <= $this->_height) {
                return $this->_image;
            }
            list($newWidth, $newHeight, $src_x, $src_y, $src_w, $src_h) = $this->_calcNewSize($width, $height);

            if ($newWidth == 0) {
                $newWidth = 1;
            }

            if ($newHeight == 0) {
                $newHeight = 1;
            }

            $t_im = imageCreateTrueColor($newWidth, $newHeight);

            if (($type == IMAGETYPE_GIF) || ($type == IMAGETYPE_PNG)) {
                imagealphablending($t_im, false);
                imagesavealpha($t_im, true);
                $transparent = imagecolorallocatealpha($t_im, 255, 255, 255, 127);
                imagefilledrectangle($t_im, 0, 0, $newWidth, $newHeight, $transparent);
            }

            imagecopyresampled($t_im, $o_im, 0, 0, $src_x, $src_y, $newWidth, $newHeight, $src_w, $src_h);

            if (!is_dir(BASEDIR . $this->_currentDir . $this->_path)) {
                @mkdir(BASEDIR . $this->_currentDir . $this->_path, 0777);
                @chmod(BASEDIR . $this->_currentDir . $this->_path, 0777);
            }

            if (!empty($this->_suffix)) {
                $pos = strrpos(basename($this->_image), ".");
                $newName = substr(basename($this->_image), 0, $pos) . $this->_suffix . substr(basename($this->_image), $pos);
            }

            $imageName = $this->_path . ($newName ? $newName : basename($this->_image));

            if (($type == IMAGETYPE_GIF) || ($type == IMAGETYPE_PNG)) {
                $success = imagepng($t_im, BASEDIR . $this->_currentDir . $imageName);
            } else {
                $success = imageJPEG($t_im, BASEDIR . $this->_currentDir . $imageName, 99);
            }

            imageDestroy($o_im);
            imageDestroy($t_im);
        }

        //echo '<pre>';print_r(parent::getUrl());echo '</pre>';

        return isset($ermsg) || !$success ? "" : $this->_currentDir . $imageName;
    }

    // returns array($newWidth, $newHeight, $dst_x, $dst_y)
    // $crop = false, tl, tc, tr, ml, mc, mr, bl, bc, br
    private function _calcNewSize($width, $height, $enlargeSmaller = false) {

        // villy's comment  if (!$enlargeSmaller && ($width <= $boxWidth) && ($height <= $boxHeight)) return array($width, $height, 0, 0);
        if (!$enlargeSmaller && ($width <= $this->_width) && ($height <= $this->_height)) {
            return array($width, $height, 0, 0, $width, $height);
        }

        $cropV = "m";
        $cropH = "c";

        if (strlen($this->_crop)) {
            $cropV = isset($this->_crop[0]) ? $this->_crop[0] : '';
            $cropH = isset($this->_crop[1]) ? $this->_crop[1] : '';
        }

        $adjX = $adjY = 0;
        $src_x = $src_y = 0;
        $src_w = $width;
        $src_h = $height;

        if ($this->_crop) {
            if ($width <= $this->_width && !$enlargeSmaller) {
                $final_width = $width;
                $final_height = $this->_height;
                $src_h = $this->_height;
            } elseif ($height <= $this->_height && !$enlargeSmaller) {
                $final_width = $this->_width;
                $final_height = $height;
                $src_w = $this->_width;
            } else {
                $factor = max($this->_width / $width, $this->_height / $height);
                $final_width = round($width * $factor);

                if ($final_width > $this->_width) {
                    $final_width = $this->_width;
                }

                $final_height = round($height * $factor);

                if ($final_height > $this->_height) {
                    $final_height = $this->_height;
                }

                $src_w = $this->_width / $factor;
                $src_h = $this->_height / $factor;
            }

            switch ($cropV) {
                case 't' : $src_y = 0;
                    break;
                case 'b' : $src_y = $height - $src_h;
                    break;
                default :
                case 'm' : $src_y = ($height - $src_h) / 2;
                    break;
            }
            switch ($cropH) {
                case 'l' : $src_x = 0;
                    break;
                case 'r' : $src_x = $width - $src_w;
                    break;
                default :
                case 'c' : $src_x = ($width - $src_w) / 2;
                    break;
            }
        } else {

            if ($this->_width == 0) {
                $factor = $this->_height / $height;
            } elseif ($this->_height == 0) {
                $factor = $this->_width / $width;
            } else {
                $factor = min($this->_width / $width, $this->_height / $height);
            }

            $final_width = round($width * $factor);
            $final_height = round($height * $factor);
        }

        return array($final_width, $final_height, $src_x, $src_y, $src_w, $src_h);
    }

    private function _getImageThumb() {
        $pos = strrpos(basename($this->_image), ".");
        $pic = substr(basename($this->_image), 0, $pos) . $this->_suffix . substr(basename($this->_image), $pos);
        if (is_file(BASEDIR . $this->_currentDir . $this->_path . $pic) && (filemtime(BASEDIR . $this->_currentDir . $this->_path . $pic) >= filemtime(BASEDIR . $this->_image))) {
            //return dirname($this->_image).$this->_path.$pic;
            return $this->_currentDir . $this->_path . $pic;
        } else {
            return false;
        }
    }

}
