<?php

namespace MODULES;

class PdfGenerator {

    protected $_generator = null;

    public function __construct($params = array()) {
        $mpdfParams = array(
            'mode' => isset($params[0]) ? $params[0] : '',
            'format' => isset($params[1]) ? $params[1] :  'A4',
            'default_font_size' => isset($params[2]) ? (int) $params[2] :  0,
            'default_font' => isset($params[3]) ? $params[3] :  '',
            'mgl' => isset($params[4]) ? (int) $params[4] :  15,
            'mgr' => isset($params[5]) ? (int) $params[5] :  15,
            'mgt' => isset($params[6]) ? (int) $params[6] :  16,
            'mgb' => isset($params[7]) ? (int) $params[7] :  16,
            'mgh' => isset($params[8]) ? (int) $params[8] :  9,
            'mgf' => isset($params[9]) ? (int) $params[9] :  9,
            'orientation' => isset($params[10]) ? $params[10] :  'P',
        );
        extract($mpdfParams);
        $this->_generator = new \mPDF($mode, $format, $default_font_size, $default_font, $mgl, $mgr, $mgt, $mgb, $mgh, $mgf, $orientation);
    }

    public function getGenerator() {
        return $this->_generator;
    }
    
    public function generateFromHtml($html, $sub = 0, $init = true, $close = true) {
        $this->_generator->WriteHTML($html, $sub, $init, $close);
        
        return $this;
    }
    
    public function addExternalCSS($css) {
        $this->_generator->WriteHTML($css, 1);
        
        return $this;
    }
    
    public function output($name = '', $dest = '') {
        
        if(strlen($name) > 0 && strlen($dest) == 0){
            $dest = 'I';
        }
        
        $this->_generator->Output($name, $dest);
        
        return $this;
    }

}
