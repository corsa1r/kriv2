<?php

namespace MODULES;

class FileTransfer {

    protected $_storage = null;
    protected $_file = null;
    protected static $_dirs = null;
    protected static $_baseDir = '';

    public function __construct($params = array()) {
        $storage = new \Upload_Storage_FileSystem();

        self::$_baseDir = BASE_PATH . DS . '..' . DS;

        if(is_null(self::$_dirs)){
            self::$_dirs = array_map(function($val){
                return self::$_baseDir . $val;
            }, \MODELS\SYS\ConfigModel::getInstance()->getGroup('fileTransferDirs'));
        }
        $this->_storage = $storage;
    }

    public function getStorage() {
        return $this->_storage;
    }

    public function setDestination($directory, $createIfNotExist = false) {
        if(isset(self::$_dirs[$directory])){
            $this->_storage->setDirectory(self::$_dirs[$directory], true);
        } else{
            $this->_storage->setDirectory(self::$_baseDir . $directory, $createIfNotExist);
        }
        return $this;
    }

    public function setFile($filename) {
        if(!is_null($this->_storage->getDirectory())){
            $this->_file = new \Upload_File($filename, $this->_storage);
        } else{
            throw new Exception('You must set upload directory before setting the file!');
        }

        return $this;
    }

    public function rename($filename) {
        if(!is_null($this->_file)){
            $this->_file->setName($filename);
        } else{
            throw new Exception('You must set file before rename!');
        }
        return $this;
    }

    public function getFile() {
        return $this->_file;
    }

    public function addValidator($validator) {
        $this->_file->addValidator($validator);
        
        return $this;
    }
    
    public function addValidators() {
        throw new Exception('Method not implemented;');
    }

    public function getFileInfo() {
        return $this->_file->getInfo();
    }

    public function getFilename() {
        return $this->_file->getNameWithExtension();
    }

    public function upload() {
        return $this->_file->upload();
    }
    
    public function isValid() {
        return $this->_file->isValid();
    }

    public function hasUploadedFiles() {
        return count($_FILES);
    }

    public function isUploadedFile($file) {
        return isset($_FILES[$file]);
    }
    
    public function getUploadedFilenames() {
        return array_keys($_FILES);
    }
    
    public function getErrors() {
        return $this->_file->getErrors();
    }
    
    public function isFileExist($path, $filename) {
        return file_exists(self::$_baseDir . $path . DS . $filename);
    }
}
