<?php
namespace MODULES;

/*
 * Kriv Framework
 */

/**
 * Description of Client
 *
 * @author Vladimir <vladimir.enchev@interactive-share.com>
 * @version 2014-2-18
 */
class Client {
    
    public $ip;         // Client IP, proxies

    public function __construct($params = array()) {
        $this->ip = $this->_getClientIP();
    }

    private function _getClientIP() {
        if (isset($this->input->server)) {
            if (isset($this->input->server["HTTP_X_FORWARDED_FOR"])) {
                return $this->input->server["HTTP_X_FORWARDED_FOR"];
            }

            if (isset($this->input->server["HTTP_CLIENT_IP"])) {
                return $this->input->server["HTTP_CLIENT_IP"];
            }

            return $this->input->server["REMOTE_ADDR"];
        }

        if (getenv('HTTP_X_FORWARDED_FOR')) {
            return getenv('HTTP_X_FORWARDED_FOR');
        }

        if (getenv('HTTP_CLIENT_IP')) {
            return getenv('HTTP_CLIENT_IP');
        }

        return getenv('REMOTE_ADDR');
    }
}