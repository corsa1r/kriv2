<?php

namespace MODULES;

class PhpExcel {

    private $__phpexcel = null;
    private $__configs = array();
    private $__optionalConfigs = array(
        'creator' => 'Kriv2 framework', 
    );

    public function __construct($params = array()) {

        $configsData = \MODELS\SYS\ConfigModel::getInstance()->getGroup('phpexcel');
      
        $this->__configs = array_merge($this->__optionalConfigs, $configsData);

        $this->__phpexcel = new \PHPExcel();
        $this->__phpexcel->getProperties()->setCreator($this->__configs['creator']);
    }

    public function getInstance() {
        return $this->__phpexcel;
    }

}
