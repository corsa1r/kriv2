<?php

namespace MODULES;

class InputHandler {
    /**
     *
     * @var MODULES\InputHandler
     */
    public $post = null;

    public function __construct() {
        if(isset($_SERVER["CONTENT_TYPE"]) && strpos($_SERVER["CONTENT_TYPE"], "application/json") !== false){
            $postdata = file_get_contents("php://input");
            $data = json_decode($postdata);
        } else{
            $data = $_POST;
        }
        $this->post = new \CORE\InputSource($data);
    }

}
