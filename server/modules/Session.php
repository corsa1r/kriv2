<?php
namespace MODULES;
/*
 * Kriv Framework
 */

/**
 * Description of Session
 * 
 * Implements SESSION
 * @author Vladimir <vladimir.enchev@interactive-share.com>
 * @version 2014-2-10
 */
class Session {

    private $_key;
    private $_id;
    private $_originalID;

    public function __construct($params) {
        
        if(is_array($params)) {
            $id = $params[0]; //First element is session ID
        } else {
            $id = $params;
        }
        
        //check session exists  in PHP >= 5.4
        if(function_exists('session_status') && session_status() == PHP_SESSION_NONE) {
            session_start();
        } else if(session_id() == '') {
            session_start();
        }
        
        $this->_originalID = $id;
        $this->_id = md5($this->_originalID);
        
        $key = \MODELS\SYS\ConfigModel::getInstance()->get('session_key');
        if(is_null($key)){
            $key = md5(time() . '@kriv2@' . time());
            \MODELS\SYS\ConfigModel::getInstance()->set('session_key', $key);
        }
        
        $this->_key = $key;
        
        $this->init(false);
    }

    public function getInstanceID() {
        return $this->_originalID;
    }
    
    private function init($useForce = false) {
        if(!isset($_SESSION[$this->_key][$this->_id]) || $useForce) {
            $_SESSION[$this->_key][$this->_id] = array();
        }
    }

    public function clean() {
        $this->init(true);
    }

    public function exists($key) {
        return array_key_exists($key, $_SESSION[$this->_key][$this->_id]);
    }

    public function get($key) {
        if($this->exists($key)) {
            return $_SESSION[$this->_key][$this->_id][$key];
        }
        
        return null;
    }

    public function getAll() {
        return $_SESSION[$this->_key][$this->_id];
    }
    
    public function getAllData() {
        return $_SESSION[$this->_key];
    }
    
    public function remove($key) {
        if($this->exists($key)) {
            unset($_SESSION[$this->_key][$this->_id][$key]);
        }
    }

    public function set($key, $value) {
        if(isset($key) && isset($value)) {
            $_SESSION[$this->_key][$this->_id][$key] = $value;
        }
    }

    public function destroy() {
        $this->init(true);
        session_destroy();
    }
}