<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

define('DS', DIRECTORY_SEPARATOR);
define('BASE_PATH', dirname(__FILE__));

require("core/Autoloader.php");

\CORE\Autoloader::registerAutoloader();

$routeParser = new \CORE\RouteParser($_SERVER);

new \CORE\Router($routeParser);