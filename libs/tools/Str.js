/**
 * @file Implements Str class
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @version 2014-10-7 12:43:11
 */
/**
 * @param {Object} $root    - window
 * @param {Object} $dom     - document
 * @param {undefined} $u    - undefined
 * @returns {undefined}
 */
(function ($root, $dom, $u) {

    "use strict";

    var Str = function () {
    };

    Str.prototype.addcslashes = function (str, charlist) {
        var target = '',
                chrs = [],
                i = 0,
                j = 0,
                c = '',
                next = '',
                rangeBegin = '',
                rangeEnd = '',
                chr = '',
                begin = 0,
                end = 0,
                octalLength = 0,
                postOctalPos = 0,
                cca = 0,
                escHexGrp = [],
                encoded = '',
                percentHex = /%([\dA-Fa-f]+)/g;

        var _pad = function (n, c) {
            if ((n = n + '').length < c) {
                return new Array(++c - n.length).join('0') + n;
            }
            return n;
        };

        for (i = 0; i < charlist.length; i++) {
            c = charlist.charAt(i);
            next = charlist.charAt(i + 1);
            if (c === '\\' && next && (/\d/).test(next)) {
                // Octal
                rangeBegin = charlist.slice(i + 1).match(/^\d+/)[0];
                octalLength = rangeBegin.length;
                postOctalPos = i + octalLength + 1;
                if (charlist.charAt(postOctalPos) + charlist.charAt(postOctalPos + 1) === '..') {
                    // Octal begins range
                    begin = rangeBegin.charCodeAt(0);
                    if ((/\\\d/).test(charlist.charAt(postOctalPos + 2) + charlist.charAt(postOctalPos + 3))) {
                        // Range ends with octal
                        rangeEnd = charlist.slice(postOctalPos + 3).match(/^\d+/)[0];
                        // Skip range end backslash
                        i += 1;
                    } else if (charlist.charAt(postOctalPos + 2)) {
                        // Range ends with character
                        rangeEnd = charlist.charAt(postOctalPos + 2);
                    } else {
                        throw 'Range with no end point';
                    }
                    end = rangeEnd.charCodeAt(0);
                    if (end > begin) {
                        // Treat as a range
                        for (j = begin; j <= end; j++) {
                            chrs.push(String.fromCharCode(j));
                        }
                    } else {
                        // Supposed to treat period, begin and end as individual characters only, not a range
                        chrs.push('.', rangeBegin, rangeEnd);
                    }
                    // Skip dots and range end (already skipped range end backslash if present)
                    i += rangeEnd.length + 2;
                } else {
                    // Octal is by itself
                    chr = String.fromCharCode(parseInt(rangeBegin, 8));
                    chrs.push(chr);
                }
                // Skip range begin
                i += octalLength;
            } else if (next + charlist.charAt(i + 2) === '..') {
                // Character begins range
                rangeBegin = c;
                begin = rangeBegin.charCodeAt(0);
                if ((/\\\d/).test(charlist.charAt(i + 3) + charlist.charAt(i + 4))) {
                    // Range ends with octal
                    rangeEnd = charlist.slice(i + 4).match(/^\d+/)[0];
                    // Skip range end backslash
                    i += 1;
                } else if (charlist.charAt(i + 3)) {
                    // Range ends with character
                    rangeEnd = charlist.charAt(i + 3);
                } else {
                    throw 'Range with no end point';
                }
                end = rangeEnd.charCodeAt(0);
                if (end > begin) {
                    // Treat as a range
                    for (j = begin; j <= end; j++) {
                        chrs.push(String.fromCharCode(j));
                    }
                } else {
                    // Supposed to treat period, begin and end as individual characters only, not a range
                    chrs.push('.', rangeBegin, rangeEnd);
                }
                // Skip dots and range end (already skipped range end backslash if present)
                i += rangeEnd.length + 2;
            } else {
                // Character is by itself
                chrs.push(c);
            }
        }

        for (i = 0; i < str.length; i++) {
            c = str.charAt(i);
            if (chrs.indexOf(c) !== -1) {
                target += '\\';
                cca = c.charCodeAt(0);
                if (cca < 32 || cca > 126) {
                    // Needs special escaping
                    switch (c) {
                        case '\n':
                            target += 'n';
                            break;
                        case '\t':
                            target += 't';
                            break;
                        case '\u000D':
                            target += 'r';
                            break;
                        case '\u0007':
                            target += 'a';
                            break;
                        case '\v':
                            target += 'v';
                            break;
                        case '\b':
                            target += 'b';
                            break;
                        case '\f':
                            target += 'f';
                            break;
                        default:
                            //target += _pad(cca.toString(8), 3);break; // Sufficient for UTF-16
                            encoded = encodeURIComponent(c);

                            // 3-length-padded UTF-8 octets
                            if ((escHexGrp = percentHex.exec(encoded)) !== null) {
                                target += _pad(parseInt(escHexGrp[1], 16).toString(8), 3); // already added a slash above
                            }
                            while ((escHexGrp = percentHex.exec(encoded)) !== null) {
                                target += '\\' + _pad(parseInt(escHexGrp[1], 16).toString(8), 3);
                            }
                            break;
                    }
                } else {
                    // Perform regular backslashed escaping
                    target += c;
                }
            } else {
                // Just add the character unescaped
                target += c;
            }
        }
        return target;
    };

    Str.prototype.addslashes = function (str) {
        return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
    };

    Str.prototype.bin2hex = function (s) {
        var i, l, o = '',
                n;

        s += '';

        for (i = 0, l = s.length; i < l; i++) {
            n = s.charCodeAt(i)
                    .toString(16);
            o += n.length < 2 ? '0' + n : n;
        }

        return o;
    };

    Str.prototype.explode = function (delimiter, string, limit) {
        if (arguments.length < 2 || typeof delimiter === 'undefined' || typeof string === 'undefined') {
            return null;
        }
        if (delimiter === '' || delimiter === false || delimiter === null) {
            return false;
        }
        if (typeof delimiter === 'function' || typeof delimiter === 'object' || typeof string === 'function' || typeof string === 'object') {
            return {
                0: ''
            };
        }
        if (delimiter === true) {
            delimiter = '1';
        }

        // Here we go...
        delimiter += '';
        string += '';

        var s = string.split(delimiter);

        if (typeof limit === 'undefined') {
            return s;
        }

        // Support for limit
        if (limit === 0) {
            limit = 1;
        }

        // Positive limit
        if (limit > 0) {
            if (limit >= s.length) {
                return s;
            }

            return s.slice(0, limit - 1).concat([s.slice(limit - 1).join(delimiter)]);
        }

        // Negative limit
        if (-limit >= s.length) {
            return [];
        }

        s.splice(s.length + limit);
        return s;
    };

    Str.prototype.htmlspecialchars = function (string, quote_style, charset, double_encode) {
        var optTemp = 0,
                i = 0,
                noquotes = false;
        if (typeof quote_style === 'undefined' || quote_style === null) {
            quote_style = 2;
        }
        string = string.toString();
        if (double_encode !== false) {
            // Put this first to avoid double-encoding
            string = string.replace(/&/g, '&amp;');
        }
        string = string.replace(/</g, '&lt;')
                .replace(/>/g, '&gt;');

        var OPTS = {
            'ENT_NOQUOTES': 0,
            'ENT_HTML_QUOTE_SINGLE': 1,
            'ENT_HTML_QUOTE_DOUBLE': 2,
            'ENT_COMPAT': 2,
            'ENT_QUOTES': 3,
            'ENT_IGNORE': 4
        };
        if (quote_style === 0) {
            noquotes = true;
        }
        if (typeof quote_style !== 'number') {
            // Allow for a single string or an array of string flags
            quote_style = [].concat(quote_style);
            for (i = 0; i < quote_style.length; i++) {
                // Resolve string input to bitwise e.g. 'ENT_IGNORE' becomes 4
                if (OPTS[quote_style[i]] === 0) {
                    noquotes = true;
                } else if (OPTS[quote_style[i]]) {
                    optTemp = optTemp | OPTS[quote_style[i]];
                }
            }
            quote_style = optTemp;
        }
        if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
            string = string.replace(/'/g, '&#039;');
        }
        if (!noquotes) {
            string = string.replace(/"/g, '&quot;');
        }

        return string;
    };

    Str.prototype.htmlspecialchars_decode = function (string, quote_style) {
        var optTemp = 0,
                i = 0,
                noquotes = false;
        if (typeof quote_style === 'undefined') {
            quote_style = 2;
        }
        string = string.toString()
                .replace(/&lt;/g, '<')
                .replace(/&gt;/g, '>');
        var OPTS = {
            'ENT_NOQUOTES': 0,
            'ENT_HTML_QUOTE_SINGLE': 1,
            'ENT_HTML_QUOTE_DOUBLE': 2,
            'ENT_COMPAT': 2,
            'ENT_QUOTES': 3,
            'ENT_IGNORE': 4
        };
        if (quote_style === 0) {
            noquotes = true;
        }
        if (typeof quote_style !== 'number') {
            // Allow for a single string or an array of string flags
            quote_style = [].concat(quote_style);
            for (i = 0; i < quote_style.length; i++) {
                // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
                if (OPTS[quote_style[i]] === 0) {
                    noquotes = true;
                } else if (OPTS[quote_style[i]]) {
                    optTemp = optTemp | OPTS[quote_style[i]];
                }
            }
            quote_style = optTemp;
        }
        if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
            string = string.replace(/&#0*39;/g, "'"); // PHP doesn't currently escape if more than one 0, but it should
            // string = string.replace(/&apos;|&#x0*27;/g, "'"); // This would also be useful here, but not a part of PHP
        }
        if (!noquotes) {
            string = string.replace(/&quot;/g, '"');
        }
        // Put this in last place to avoid escape being double-decoded
        string = string.replace(/&amp;/g, '&');

        return string;
    };

    Str.prototype.lcfirst = function (str) {
        str += '';
        var f = str.charAt(0).toLowerCase();
        return f + str.substr(1);
    };

    Str.prototype.ucfirst = function (str) {
        str += '';
        var f = str.charAt(0).toUpperCase();
        return f + str.substr(1);
    };

    Str.prototype.ltrim = function (str, charlist) {
        charlist = !charlist ? ' \\s\u00A0' : (charlist + '')
                .replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '$1');
        var re = new RegExp('^[' + charlist + ']+', 'g');
        return (str + '')
                .replace(re, '');
    };

    Str.prototype.ltrim = function (str, charlist) {
        charlist = !charlist ? ' \\s\u00A0' : (charlist + '').replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\\$1');
        var re = new RegExp('[' + charlist + ']+$', 'g');
        return (str + '').replace(re, '');
    };

    Str.prototype.sha1 = function (str) {
        var rotate_left = function (n, s) {
            var t4 = (n << s) | (n >>> (32 - s));
            return t4;
        };

        /*var lsb_hex = function (val) {
         // Not in use; needed?
         var str="";
         var i;
         var vh;
         var vl;
         
         for ( i=0; i<=6; i+=2 ) {
         vh = (val>>>(i*4+4))&0x0f;
         vl = (val>>>(i*4))&0x0f;
         str += vh.toString(16) + vl.toString(16);
         }
         return str;
         };*/

        var cvt_hex = function (val) {
            var str = '';
            var i;
            var v;

            for (i = 7; i >= 0; i--) {
                v = (val >>> (i * 4)) & 0x0f;
                str += v.toString(16);
            }
            return str;
        };

        var blockstart;
        var i, j;
        var W = new Array(80);
        var H0 = 0x67452301;
        var H1 = 0xEFCDAB89;
        var H2 = 0x98BADCFE;
        var H3 = 0x10325476;
        var H4 = 0xC3D2E1F0;
        var A, B, C, D, E;
        var temp;

        // utf8_encode
        str = unescape(encodeURIComponent(str));
        var str_len = str.length;

        var word_array = [];
        for (i = 0; i < str_len - 3; i += 4) {
            j = str.charCodeAt(i) << 24 | str.charCodeAt(i + 1) << 16 | str.charCodeAt(i + 2) << 8 | str.charCodeAt(i + 3);
            word_array.push(j);
        }

        switch (str_len % 4) {
            case 0:
                i = 0x080000000;
                break;
            case 1:
                i = str.charCodeAt(str_len - 1) << 24 | 0x0800000;
                break;
            case 2:
                i = str.charCodeAt(str_len - 2) << 24 | str.charCodeAt(str_len - 1) << 16 | 0x08000;
                break;
            case 3:
                i = str.charCodeAt(str_len - 3) << 24 | str.charCodeAt(str_len - 2) << 16 | str.charCodeAt(str_len - 1) <<
                        8 | 0x80;
                break;
        }

        word_array.push(i);

        while ((word_array.length % 16) != 14) {
            word_array.push(0);
        }

        word_array.push(str_len >>> 29);
        word_array.push((str_len << 3) & 0x0ffffffff);

        for (blockstart = 0; blockstart < word_array.length; blockstart += 16) {
            for (i = 0; i < 16; i++) {
                W[i] = word_array[blockstart + i];
            }
            for (i = 16; i <= 79; i++) {
                W[i] = rotate_left(W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16], 1);
            }

            A = H0;
            B = H1;
            C = H2;
            D = H3;
            E = H4;

            for (i = 0; i <= 19; i++) {
                temp = (rotate_left(A, 5) + ((B & C) | (~B & D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
                E = D;
                D = C;
                C = rotate_left(B, 30);
                B = A;
                A = temp;
            }

            for (i = 20; i <= 39; i++) {
                temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
                E = D;
                D = C;
                C = rotate_left(B, 30);
                B = A;
                A = temp;
            }

            for (i = 40; i <= 59; i++) {
                temp = (rotate_left(A, 5) + ((B & C) | (B & D) | (C & D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
                E = D;
                D = C;
                C = rotate_left(B, 30);
                B = A;
                A = temp;
            }

            for (i = 60; i <= 79; i++) {
                temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
                E = D;
                D = C;
                C = rotate_left(B, 30);
                B = A;
                A = temp;
            }

            H0 = (H0 + A) & 0x0ffffffff;
            H1 = (H1 + B) & 0x0ffffffff;
            H2 = (H2 + C) & 0x0ffffffff;
            H3 = (H3 + D) & 0x0ffffffff;
            H4 = (H4 + E) & 0x0ffffffff;
        }

        temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);
        return temp.toLowerCase();
    };

    Str.prototype.trim = function (str, charlist) {
        var whitespace, l = 0,
                i = 0;
        str += '';

        if (!charlist) {
            // default list
            whitespace =
                    ' \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000';
        } else {
            // preg_quote custom list
            charlist += '';
            whitespace = charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '$1');
        }

        l = str.length;
        for (i = 0; i < l; i++) {
            if (whitespace.indexOf(str.charAt(i)) === -1) {
                str = str.substring(i);
                break;
            }
        }

        l = str.length;
        for (i = l - 1; i >= 0; i--) {
            if (whitespace.indexOf(str.charAt(i)) === -1) {
                str = str.substring(0, i + 1);
                break;
            }
        }

        return whitespace.indexOf(str.charAt(0)) === -1 ? str : '';
    };

    Str.prototype.md5 = function (str) {
        var xl;

        var rotateLeft = function (lValue, iShiftBits) {
            return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
        };

        var addUnsigned = function (lX, lY) {
            var lX4, lY4, lX8, lY8, lResult;
            lX8 = (lX & 0x80000000);
            lY8 = (lY & 0x80000000);
            lX4 = (lX & 0x40000000);
            lY4 = (lY & 0x40000000);
            lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
            if (lX4 & lY4) {
                return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
            }
            if (lX4 | lY4) {
                if (lResult & 0x40000000) {
                    return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
                } else {
                    return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
                }
            } else {
                return (lResult ^ lX8 ^ lY8);
            }
        };

        var _F = function (x, y, z) {
            return (x & y) | ((~x) & z);
        };
        var _G = function (x, y, z) {
            return (x & z) | (y & (~z));
        };
        var _H = function (x, y, z) {
            return (x ^ y ^ z);
        };
        var _I = function (x, y, z) {
            return (y ^ (x | (~z)));
        };

        var _FF = function (a, b, c, d, x, s, ac) {
            a = addUnsigned(a, addUnsigned(addUnsigned(_F(b, c, d), x), ac));
            return addUnsigned(rotateLeft(a, s), b);
        };

        var _GG = function (a, b, c, d, x, s, ac) {
            a = addUnsigned(a, addUnsigned(addUnsigned(_G(b, c, d), x), ac));
            return addUnsigned(rotateLeft(a, s), b);
        };

        var _HH = function (a, b, c, d, x, s, ac) {
            a = addUnsigned(a, addUnsigned(addUnsigned(_H(b, c, d), x), ac));
            return addUnsigned(rotateLeft(a, s), b);
        };

        var _II = function (a, b, c, d, x, s, ac) {
            a = addUnsigned(a, addUnsigned(addUnsigned(_I(b, c, d), x), ac));
            return addUnsigned(rotateLeft(a, s), b);
        };

        var convertToWordArray = function (str) {
            var lWordCount;
            var lMessageLength = str.length;
            var lNumberOfWords_temp1 = lMessageLength + 8;
            var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
            var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
            var lWordArray = new Array(lNumberOfWords - 1);
            var lBytePosition = 0;
            var lByteCount = 0;
            while (lByteCount < lMessageLength) {
                lWordCount = (lByteCount - (lByteCount % 4)) / 4;
                lBytePosition = (lByteCount % 4) * 8;
                lWordArray[lWordCount] = (lWordArray[lWordCount] | (str.charCodeAt(lByteCount) << lBytePosition));
                lByteCount++;
            }
            lWordCount = (lByteCount - (lByteCount % 4)) / 4;
            lBytePosition = (lByteCount % 4) * 8;
            lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
            lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
            lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
            return lWordArray;
        };

        var wordToHex = function (lValue) {
            var wordToHexValue = '',
                    wordToHexValue_temp = '',
                    lByte, lCount;
            for (lCount = 0; lCount <= 3; lCount++) {
                lByte = (lValue >>> (lCount * 8)) & 255;
                wordToHexValue_temp = '0' + lByte.toString(16);
                wordToHexValue = wordToHexValue + wordToHexValue_temp.substr(wordToHexValue_temp.length - 2, 2);
            }
            return wordToHexValue;
        };

        var x = [],
                k, AA, BB, CC, DD, a, b, c, d, S11 = 7,
                S12 = 12,
                S13 = 17,
                S14 = 22,
                S21 = 5,
                S22 = 9,
                S23 = 14,
                S24 = 20,
                S31 = 4,
                S32 = 11,
                S33 = 16,
                S34 = 23,
                S41 = 6,
                S42 = 10,
                S43 = 15,
                S44 = 21;

        str = this.utf8_encode(str);
        x = convertToWordArray(str);
        a = 0x67452301;
        b = 0xEFCDAB89;
        c = 0x98BADCFE;
        d = 0x10325476;

        xl = x.length;
        for (k = 0; k < xl; k += 16) {
            AA = a;
            BB = b;
            CC = c;
            DD = d;
            a = _FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
            d = _FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
            c = _FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
            b = _FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
            a = _FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
            d = _FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
            c = _FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
            b = _FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
            a = _FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
            d = _FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
            c = _FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
            b = _FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
            a = _FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
            d = _FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
            c = _FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
            b = _FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
            a = _GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
            d = _GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
            c = _GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
            b = _GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
            a = _GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
            d = _GG(d, a, b, c, x[k + 10], S22, 0x2441453);
            c = _GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
            b = _GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
            a = _GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
            d = _GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
            c = _GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
            b = _GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
            a = _GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
            d = _GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
            c = _GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
            b = _GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
            a = _HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
            d = _HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
            c = _HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
            b = _HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
            a = _HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
            d = _HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
            c = _HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
            b = _HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
            a = _HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
            d = _HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
            c = _HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
            b = _HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
            a = _HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
            d = _HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
            c = _HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
            b = _HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
            a = _II(a, b, c, d, x[k + 0], S41, 0xF4292244);
            d = _II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
            c = _II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
            b = _II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
            a = _II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
            d = _II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
            c = _II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
            b = _II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
            a = _II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
            d = _II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
            c = _II(c, d, a, b, x[k + 6], S43, 0xA3014314);
            b = _II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
            a = _II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
            d = _II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
            c = _II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
            b = _II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
            a = addUnsigned(a, AA);
            b = addUnsigned(b, BB);
            c = addUnsigned(c, CC);
            d = addUnsigned(d, DD);
        }

        var temp = wordToHex(a) + wordToHex(b) + wordToHex(c) + wordToHex(d);

        return temp.toLowerCase();
    };

    Str.prototype.nl2br = function (str, is_xhtml) {
        //   example 1: nl2br('Kevin\nvan\nZonneveld');
        //   returns 1: 'Kevin<br />\nvan<br />\nZonneveld'
        //   example 2: nl2br("\nOne\nTwo\n\nThree\n", false);
        //   returns 2: '<br>\nOne<br>\nTwo<br>\n<br>\nThree<br>\n'
        //   example 3: nl2br("\nOne\nTwo\n\nThree\n", true);
        //   returns 3: '<br />\nOne<br />\nTwo<br />\n<br />\nThree<br />\n'

        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>'; // Adjust comment to avoid issue on phpjs.org display

        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    };

    Str.prototype.similar_text = function (first, second, percent) {
        //   example 1: similar_text('Hello World!', 'Hello phpjs!');
        //   returns 1: 7
        //   example 2: similar_text('Hello World!', null);
        //   returns 2: 0

        if (first === null || second === null || typeof first === 'undefined' || typeof second === 'undefined') {
            return 0;
        }

        first += '';
        second += '';

        var pos1 = 0,
                pos2 = 0,
                max = 0,
                firstLength = first.length,
                secondLength = second.length,
                p, q, l, sum;

        max = 0;

        for (p = 0; p < firstLength; p++) {
            for (q = 0; q < secondLength; q++) {
                for (l = 0;
                        (p + l < firstLength) && (q + l < secondLength) && (first.charAt(p + l) === second.charAt(q + l)); l++)
                    ;
                if (l > max) {
                    max = l;
                    pos1 = p;
                    pos2 = q;
                }
            }
        }

        sum = max;

        if (sum) {
            if (pos1 && pos2) {
                sum += this.similar_text(first.substr(0, pos1), second.substr(0, pos2));
            }

            if ((pos1 + max < firstLength) && (pos2 + max < secondLength)) {
                sum += this.similar_text(first.substr(pos1 + max, firstLength - pos1 - max), second.substr(pos2 + max,
                        secondLength - pos2 - max));
            }
        }

        if (!percent) {
            return sum;
        } else {
            return (sum * 200) / (firstLength + secondLength);
        }
    };

    Str.prototype.repeat = function (input, multiplier) {
        //   example 1: str_repeat('-=', 10);
        //   returns 1: '-=-=-=-=-=-=-=-=-=-='

        var y = '';
        while (true) {
            if (multiplier & 1) {
                y += input;
            }
            multiplier >>= 1;
            if (multiplier) {
                input += input;
            } else {
                break;
            }
        }
        return y;
    };

    Str.prototype.replace = function (search, replace, subject, count) {
        var i = 0,
                j = 0,
                temp = '',
                repl = '',
                sl = 0,
                fl = 0,
                f = [].concat(search),
                r = [].concat(replace),
                s = subject,
                ra = Object.prototype.toString.call(r) === '[object Array]',
                sa = Object.prototype.toString.call(s) === '[object Array]';
        s = [].concat(s);

        if (typeof (search) === 'object' && typeof (replace) === 'string') {
            temp = replace;
            replace = new Array();
            for (i = 0; i < search.length; i += 1) {
                replace[i] = temp;
            }
            temp = '';
            r = [].concat(replace);
            ra = Object.prototype.toString.call(r) === '[object Array]';
        }

        if (count) {
            this.window[count] = 0;
        }

        for (i = 0, sl = s.length; i < sl; i++) {
            if (s[i] === '') {
                continue;
            }
            for (j = 0, fl = f.length; j < fl; j++) {
                temp = s[i] + '';
                repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
                s[i] = (temp)
                        .split(f[j])
                        .join(repl);
                if (count) {
                    this.window[count] += ((temp.split(f[j])).length - 1);
                }
            }
        }
        return sa ? s : s[0];
    };

    Str.prototype.word_count = function (str, format, charlist) {
        var len = str.length,
                cl = charlist && charlist.length,
                chr = '',
                tmpStr = '',
                i = 0,
                c = '',
                wArr = [],
                wC = 0,
                assoc = {},
                aC = 0,
                reg = '',
                match = false;

        // BEGIN STATIC
        var _preg_quote = function (str) {
            return (str + '')
                    .replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!<>\|\:])/g, '\\$1');
        };
        _getWholeChar = function (str, i) {
            // Use for rare cases of non-BMP characters
            var code = str.charCodeAt(i);
            if (code < 0xD800 || code > 0xDFFF) {
                return str.charAt(i);
            }
            if (0xD800 <= code && code <= 0xDBFF) {
                // High surrogate (could change last hex to 0xDB7F to treat high private surrogates as single characters)
                if (str.length <= (i + 1)) {
                    throw 'High surrogate without following low surrogate';
                }
                var next = str.charCodeAt(i + 1);
                if (0xDC00 > next || next > 0xDFFF) {
                    throw 'High surrogate without following low surrogate';
                }
                return str.charAt(i) + str.charAt(i + 1);
            }
            // Low surrogate (0xDC00 <= code && code <= 0xDFFF)
            if (i === 0) {
                throw 'Low surrogate without preceding high surrogate';
            }
            var prev = str.charCodeAt(i - 1);
            if (0xD800 > prev || prev > 0xDBFF) {
                // (could change last hex to 0xDB7F to treat high private surrogates as single characters)
                throw 'Low surrogate without preceding high surrogate';
            }
            // We can pass over low surrogates now as the second component in a pair which we have already processed
            return false;
        };
        // END STATIC
        if (cl) {
            reg = '^(' + _preg_quote(_getWholeChar(charlist, 0));
            for (i = 1; i < cl; i++) {
                if ((chr = _getWholeChar(charlist, i)) === false) {
                    continue;
                }
                reg += '|' + _preg_quote(chr);
            }
            reg += ')$';
            reg = new RegExp(reg);
        }

        for (i = 0; i < len; i++) {
            if ((c = _getWholeChar(str, i)) === false) {
                continue;
            }
            match = this.ctype_alpha(c) || (reg && c.search(reg) !== -1) || ((i !== 0 && i !== len - 1) && c === '-') || // No hyphen at beginning or end unless allowed in charlist (or locale)
                    // No apostrophe at beginning unless allowed in charlist (or locale)
                            (i !== 0 && c === "'");
                    if (match) {
                        if (tmpStr === '' && format === 2) {
                            aC = i;
                        }
                        tmpStr = tmpStr + c;
                    }
                    if (i === len - 1 || !match && tmpStr !== '') {
                        if (format !== 2) {
                            wArr[wArr.length] = tmpStr;
                        } else {
                            assoc[aC] = tmpStr;
                        }
                        tmpStr = '';
                        wC++;
                    }
                }

                if (!format) {
                    return wC;
                } else if (format === 1) {
                    return wArr;
                } else if (format === 2) {
                    return assoc;
                }

                throw 'You have supplied an incorrect format';
            };

            Str.prototype.strip_tags = function (input, allowed) {
                allowed = (((allowed || '') + '')
                        .toLowerCase()
                        .match(/<[a-z][a-z0-9]*>/g) || [])
                        .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
                var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
                        commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
                return input.replace(commentsAndPhpTags, '')
                        .replace(tags, function ($0, $1) {
                            return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
                        });
            };

            Str.prototype.strpos = function (haystack, needle, offset) {
                var i = (haystack + '').indexOf(needle, (offset || 0));
                return i === -1 ? false : i;
            };

            Str.prototype.ucwords = function (str) {
                //   example 1: ucwords('kevin van  zonneveld');
                //   returns 1: 'Kevin Van  Zonneveld'
                //   example 2: ucwords('HELLO WORLD');
                //   returns 2: 'HELLO WORLD'

                return (str + '').replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
                    return $1.toUpperCase();
                });
            };

            Str.prototype.money_format = function (format, number) {
                //   example 1: money_format('%i', 1234.56);
                //   returns 1: ' USD 1,234.56'
                //   example 2: money_format('%14#8.2n', 1234.5678);
                //   returns 2: ' $     1,234.57'
                //   example 3: money_format('%14#8.2n', -1234.5678);
                //   returns 3: '-$     1,234.57'
                //   example 4: money_format('%(14#8.2n', 1234.5678);
                //   returns 4: ' $     1,234.57 '
                //   example 5: money_format('%(14#8.2n', -1234.5678);
                //   returns 5: '($     1,234.57)'
                //   example 6: money_format('%=014#8.2n', 1234.5678);
                //   returns 6: ' $000001,234.57'
                //   example 7: money_format('%=014#8.2n', -1234.5678);
                //   returns 7: '-$000001,234.57'
                //   example 8: money_format('%=*14#8.2n', 1234.5678);
                //   returns 8: ' $*****1,234.57'
                //   example 9: money_format('%=*14#8.2n', -1234.5678);
                //   returns 9: '-$*****1,234.57'
                //  example 10: money_format('%=*^14#8.2n', 1234.5678);
                //  returns 10: '  $****1234.57'
                //  example 11: money_format('%=*^14#8.2n', -1234.5678);
                //  returns 11: ' -$****1234.57'
                //  example 12: money_format('%=*!14#8.2n', 1234.5678);
                //  returns 12: ' *****1,234.57'
                //  example 13: money_format('%=*!14#8.2n', -1234.5678);
                //  returns 13: '-*****1,234.57'
                //  example 14: money_format('%i', 3590);
                //  returns 14: ' USD 3,590.00'

                // Per PHP behavior, there seems to be no extra padding for sign when there is a positive number, though my
                // understanding of the description is that there should be padding; need to revisit examples

                // Helpful info at http://ftp.gnu.org/pub/pub/old-gnu/Manuals/glibc-2.2.3/html_chapter/libc_7.html and http://publib.boulder.ibm.com/infocenter/zos/v1r10/index.jsp?topic=/com.ibm.zos.r10.bpxbd00/strfmp.htm

                if (typeof number !== 'number') {
                    return null;
                }
                // 1: flags, 3: width, 5: left, 7: right, 8: conversion
                var regex = /%((=.|[+^(!-])*?)(\d*?)(#(\d+))?(\.(\d+))?([in%])/g;

                // Ensure the locale data we need is set up
                this.setlocale('LC_ALL', 0);
                var monetary = this.php_js.locales[this.php_js.localeCategories['LC_MONETARY']]['LC_MONETARY'];

                var doReplace = function (n0, flags, n2, width, n4, left, n6, right, conversion) {
                    var value = '',
                            repl = '';
                    if (conversion === '%') {
                        // Percent does not seem to be allowed with intervening content
                        return '%';
                    }
                    var fill = flags && (/=./)
                            .test(flags) ? flags.match(/=(.)/)[1] : ' '; // flag: =f (numeric fill)
                    // flag: ! (suppress currency symbol)
                    var showCurrSymbol = !flags || flags.indexOf('!') === -1;
                    // field width: w (minimum field width)
                    width = parseInt(width, 10) || 0;

                    var neg = number < 0;
                    // Convert to string
                    number = number + '';
                    // We don't want negative symbol represented here yet
                    number = neg ? number.slice(1) : number;

                    var decpos = number.indexOf('.');
                    // Get integer portion
                    var integer = decpos !== -1 ? number.slice(0, decpos) : number;
                    // Get decimal portion
                    var fraction = decpos !== -1 ? number.slice(decpos + 1) : '';

                    var _str_splice = function (integerStr, idx, thous_sep) {
                        var integerArr = integerStr.split('');
                        integerArr.splice(idx, 0, thous_sep);
                        return integerArr.join('');
                    };

                    var init_lgth = integer.length;
                    left = parseInt(left, 10);
                    var filler = init_lgth < left;
                    if (filler) {
                        var fillnum = left - init_lgth;
                        integer = new Array(fillnum + 1)
                                .join(fill) + integer;
                    }
                    if (flags.indexOf('^') === -1) {
                        // flag: ^ (disable grouping characters (of locale))
                        // use grouping characters
                        // ','
                        var thous_sep = monetary.mon_thousands_sep;
                        // [3] (every 3 digits in U.S.A. locale)
                        var mon_grouping = monetary.mon_grouping;

                        if (mon_grouping[0] < integer.length) {
                            for (var i = 0, idx = integer.length; i < mon_grouping.length; i++) {
                                // e.g., 3
                                idx -= mon_grouping[i];
                                if (idx <= 0) {
                                    break;
                                }
                                if (filler && idx < fillnum) {
                                    thous_sep = fill;
                                }
                                integer = _str_splice(integer, idx, thous_sep);
                            }
                        }
                        if (mon_grouping[i - 1] > 0) {
                            // Repeating last grouping (may only be one) until highest portion of integer reached
                            while (idx > mon_grouping[i - 1]) {
                                idx -= mon_grouping[i - 1];
                                if (filler && idx < fillnum) {
                                    thous_sep = fill;
                                }
                                integer = _str_splice(integer, idx, thous_sep);
                            }
                        }
                    }

                    // left, right
                    if (right === '0') {
                        // No decimal or fractional digits
                        value = integer;
                    } else {
                        // '.'
                        var dec_pt = monetary.mon_decimal_point;
                        if (right === '' || right === undefined) {
                            right = conversion === 'i' ? monetary.int_frac_digits : monetary.frac_digits;
                        }
                        right = parseInt(right, 10);

                        if (right === 0) {
                            // Only remove fractional portion if explicitly set to zero digits
                            fraction = '';
                            dec_pt = '';
                        } else if (right < fraction.length) {
                            fraction = Math.round(parseFloat(fraction.slice(0, right) + '.' + fraction.substr(right, 1))) + '';
                            if (right > fraction.length) {
                                fraction = new Array(right - fraction.length + 1)
                                        .join('0') + fraction; // prepend with 0's
                            }
                        } else if (right > fraction.length) {
                            fraction += new Array(right - fraction.length + 1)
                                    .join('0'); // pad with 0's
                        }
                        value = integer + dec_pt + fraction;
                    }

                    var symbol = '';
                    if (showCurrSymbol) {
                        // 'i' vs. 'n' ('USD' vs. '$')
                        symbol = conversion === 'i' ? monetary.int_curr_symbol : monetary.currency_symbol;
                    }
                    var sign_posn = neg ? monetary.n_sign_posn : monetary.p_sign_posn;

                    // 0: no space between curr. symbol and value
                    // 1: space sep. them unless symb. and sign are adjacent then space sep. them from value
                    // 2: space sep. sign and value unless symb. and sign are adjacent then space separates
                    var sep_by_space = neg ? monetary.n_sep_by_space : monetary.p_sep_by_space;

                    // p_cs_precedes, n_cs_precedes // positive currency symbol follows value = 0; precedes value = 1
                    var cs_precedes = neg ? monetary.n_cs_precedes : monetary.p_cs_precedes;

                    // Assemble symbol/value/sign and possible space as appropriate
                    if (flags.indexOf('(') !== -1) {
                        // flag: parenth. for negative
                        // Fix: unclear on whether and how sep_by_space, sign_posn, or cs_precedes have
                        // an impact here (as they do below), but assuming for now behaves as sign_posn 0 as
                        // far as localized sep_by_space and sign_posn behavior
                        repl = (cs_precedes ? symbol + (sep_by_space === 1 ? ' ' : '') : '') + value + (!cs_precedes ? (
                                sep_by_space === 1 ? ' ' : '') + symbol : '');
                        if (neg) {
                            repl = '(' + repl + ')';
                        } else {
                            repl = ' ' + repl + ' ';
                        }
                    } else {
                        // '+' is default
                        // ''
                        var pos_sign = monetary.positive_sign;
                        // '-'
                        var neg_sign = monetary.negative_sign;
                        var sign = neg ? (neg_sign) : (pos_sign);
                        var otherSign = neg ? (pos_sign) : (neg_sign);
                        var signPadding = '';
                        if (sign_posn) {
                            // has a sign
                            signPadding = new Array(otherSign.length - sign.length + 1)
                                    .join(' ');
                        }

                        var valueAndCS = '';
                        switch (sign_posn) {
                            // 0: parentheses surround value and curr. symbol;
                            // 1: sign precedes them;
                            // 2: sign follows them;
                            // 3: sign immed. precedes curr. symbol; (but may be space between)
                            // 4: sign immed. succeeds curr. symbol; (but may be space between)
                            case 0:
                                valueAndCS = cs_precedes ? symbol + (sep_by_space === 1 ? ' ' : '') + value : value + (sep_by_space ===
                                        1 ? ' ' : '') + symbol;
                                repl = '(' + valueAndCS + ')';
                                break;
                            case 1:
                                valueAndCS = cs_precedes ? symbol + (sep_by_space === 1 ? ' ' : '') + value : value + (sep_by_space ===
                                        1 ? ' ' : '') + symbol;
                                repl = signPadding + sign + (sep_by_space === 2 ? ' ' : '') + valueAndCS;
                                break;
                            case 2:
                                valueAndCS = cs_precedes ? symbol + (sep_by_space === 1 ? ' ' : '') + value : value + (sep_by_space ===
                                        1 ? ' ' : '') + symbol;
                                repl = valueAndCS + (sep_by_space === 2 ? ' ' : '') + sign + signPadding;
                                break;
                            case 3:
                                repl = cs_precedes ? signPadding + sign + (sep_by_space === 2 ? ' ' : '') + symbol + (sep_by_space ===
                                        1 ? ' ' : '') + value : value + (sep_by_space === 1 ? ' ' : '') + sign + signPadding + (
                                        sep_by_space === 2 ? ' ' : '') + symbol;
                                break;
                            case 4:
                                repl = cs_precedes ? symbol + (sep_by_space === 2 ? ' ' : '') + signPadding + sign + (sep_by_space ===
                                        1 ? ' ' : '') + value : value + (sep_by_space === 1 ? ' ' : '') + symbol + (sep_by_space === 2 ?
                                        ' ' : '') + sign + signPadding;
                                break;
                        }
                    }

                    var padding = width - repl.length;
                    if (padding > 0) {
                        padding = new Array(padding + 1)
                                .join(' ');
                        // Fix: How does p_sep_by_space affect the count if there is a space? Included in count presumably?
                        if (flags.indexOf('-') !== -1) {
                            // left-justified (pad to right)
                            repl += padding;
                        } else {
                            // right-justified (pad to left)
                            repl = padding + repl;
                        }
                    }
                    return repl;
                };

                return format.replace(regex, doReplace);
            };

            var Base64 = function () {
                this.key = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
            };

            Base64.prototype.encode = function (data) {
                var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
                        ac = 0,
                        enc = '',
                        tmp_arr = [];

                if (!data) {
                    return data;
                }

                do { // pack three octets into four hexets
                    o1 = data.charCodeAt(i++);
                    o2 = data.charCodeAt(i++);
                    o3 = data.charCodeAt(i++);

                    bits = o1 << 16 | o2 << 8 | o3;

                    h1 = bits >> 18 & 0x3f;
                    h2 = bits >> 12 & 0x3f;
                    h3 = bits >> 6 & 0x3f;
                    h4 = bits & 0x3f;

                    // use hexets to index into b64, and append result to encoded string
                    tmp_arr[ac++] = this.key.charAt(h1) + this.key.charAt(h2) + this.key.charAt(h3) + this.key.charAt(h4);
                } while (i < data.length);

                enc = tmp_arr.join('');

                var r = data.length % 3;

                return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
            };

            Base64.prototype.decode = function (data) {
                var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
                        ac = 0,
                        dec = '',
                        tmp_arr = [];

                if (!data) {
                    return data;
                }

                data += '';

                do { // unpack four hexets into three octets using index points in b64
                    h1 = this.key.indexOf(data.charAt(i++));
                    h2 = this.key.indexOf(data.charAt(i++));
                    h3 = this.key.indexOf(data.charAt(i++));
                    h4 = this.key.indexOf(data.charAt(i++));

                    bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;

                    o1 = bits >> 16 & 0xff;
                    o2 = bits >> 8 & 0xff;
                    o3 = bits & 0xff;

                    if (h3 == 64) {
                        tmp_arr[ac++] = String.fromCharCode(o1);
                    } else if (h4 == 64) {
                        tmp_arr[ac++] = String.fromCharCode(o1, o2);
                    } else {
                        tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
                    }
                } while (i < data.length);

                dec = tmp_arr.join('');

                return dec.replace(/\0+$/, '');
            };

            var str = new Str();
            str.Base64 = new Base64();

            $root.$namespace.tools.Str = str;
        })(window, document);
