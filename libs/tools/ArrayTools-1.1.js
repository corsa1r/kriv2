/**
 * @file implements Array Tools
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * 
 * @version 18.07.2014
 */
;
(function($root) {

    "use strict";

    var ArrayTool = function() {
    };

    /**
     * This method creates an temporary array with keys of @param
     * @param {Array} array - the Array
     * @return {Array} array with keys
     */
    ArrayTool.prototype.keys = function(array) {
        var temp = [];

        this.each(array, function(key, value) {
            temp.push(key);
        });

        return temp;
    };

    ArrayTool.prototype.generateNumeric = function(from, to) {
        var temp = [];

        while (from <= to) {
            temp.push(from);
            from++;
        }

        return temp;
    };

    /**
     * This method creates an temporary array with values of @param
     * @param {Array} array - the Array
     * @return {Array} array with values
     */
    ArrayTool.prototype.values = function(array) {
        var temp = [];

        this.each(array, function(key, value) {
            temp.push(value);
        });

        return temp;
    };

    /**
     * This method tell you if @param is an valid Array
     * @param {Array} array - the Array
     * @return {Boolean} true if @param is array false if not
     */
    ArrayTool.prototype.isValid = function(array) {
        return Boolean(array instanceof Array || array instanceof Object || typeof array === "object");
    };


    /**
     * This method clean the array @param
     * @param {Array} array - the Array
     * @return {Number} new length ex. 0
     */
    ArrayTool.prototype.empty = function(array) {
        return array.length = 0;
    };

    /**
     * This method loops the @param array
     * @param {Array} array - the Array
     * @param {Function} stepCallback - this is callback function wich
     is called every single step with two assigned parameters 
     (key, value) of current iteration
     * @return {undefined}
     */
    ArrayTool.prototype.each = function(array, stepCallback) {
        if (this.isValid(array) && (stepCallback instanceof Function || typeof stepCallback === "function")) {
            for (var key in array) {
                if (array.hasOwnProperty(key)) {
                    var result = stepCallback.call(stepCallback, key, array[key]);
                    if (result !== undefined) {
                        return result;
                    }
                }
            }
        }

        return undefined;
    };

    /**
     * @param {Array} array - the Array
     * @param {*} - the key of array
     * @return {Boolean} true if key exists
     */
    ArrayTool.prototype.keyExists = function(array, keySearch) {
        var res = this.each(array, function(key, value) {
            if (key == keySearch) {
                return true;
            }
        });

        return !!res;
    };

    /**
     * @param {Array} array - the Array
     * @param {*} - the value of array
     * @return {Boolean} true if value exists
     */
    ArrayTool.prototype.valueExists = function(array, valueSearch) {

        var state = false;

        this.each(array, function(key, value) {
            if (value == valueSearch) {
                state = true;
                return;
            }
        });

        return state;
    };

    /**
     * This method search in array for given value
     * @param {Array} array - the Array
     * @param {*} - the value of array
     * @return {*} key on found, -1 if not found an element 
     */
    ArrayTool.prototype.indexOf = function(array, valueSearch) {
        var index = this.each(array, function(key, value) {
            if (value == valueSearch) {
                return key;
            }
        });

        return index || -1;
    };

    /**
     * This method search in array for given key
     * @param {Array} array - the Array
     * @param {*} - the key of array
     * @return {*} value on found, -1 if not found an element 
     */
    ArrayTool.prototype.valueOf = function(array, keySearch) {
        var value = this.each(array, function(key, value) {
            if (key == keySearch) {
                return value;
            }
        });

        return value || -1;
    };

    ArrayTool.prototype.firstKey = function(array) {
        return this.each(array, function(key, value) {
            return key;
        });
    };

    ArrayTool.prototype.firstValue = function(array) {
        return this.each(array, function(key, value) {
            return value;
        });
    };

    ArrayTool.prototype.max = function(array) {
        var max = this.firstValue(array);

        this.each(array, function(key, value) {
            if (value > max) {
                max = value;
            }
        });

        return max;
    };

    ArrayTool.prototype.min = function(array) {
        var min = this.firstValue(array);

        this.each(array, function(key, value) {
            if (value < min) {
                min = value;
            }
        });

        return min;
    };

    /**
     * @param {Array} - the array
     * @returns {Array} - the copy of @param
     */
    ArrayTool.prototype.copy = function(array) {
        var temp = [];

        this.each(array, function(key, value) {
            temp[key] = value;
        });

        return temp;
    };

    ArrayTool.prototype.merge = function(array1, array2, toRight) {
        toRight = Boolean(toRight);

        this.each(toRight ? array1 : array2, function(key, value) {
            if (toRight) {
                array2[key] = value;
            } else {
                array1[key] = value;
            }
        });

        return toRight ? array2 : array1;
    };

    $root.$namespace.tools.ArrayTool = new ArrayTool();
})(window);