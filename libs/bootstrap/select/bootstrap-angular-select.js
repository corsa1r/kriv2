;(function($root) {

    "use strict";

    $root.$namespace.get('app').directive('selectpicker', function($interval) {
        return {
            restrict: 'EAC',
            compile: function(element) {

                if($root.$namespace.get('.selectpicker-interval')) {
                    clearInterval($root.$namespace.get('.selectpicker-interval'));
                }  

                var lastValues = [];
                lastValues[element] = null;

                var cancel = setInterval(function() {
                    if(lastValues[element] !== angular.element(element).children('option').length) {
                        angular.element('.selectpicker').selectpicker('refresh');
                        lastValues[element] = angular.element(element).children('option').length;
                    }
                }, 150);

                $root.$namespace.add(cancel, '.selectpicker-interval');
            }
        };
    });

})(window);