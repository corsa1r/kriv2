/**
 * @file implements namespace system
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @version 14.07.2014
 */

/*
 * @requires Container
 *
 *
 * Using $namespace
 * $root is a root directory
 * $namespace is a global namespace
 * {key} is your namespace
 * @see 
 * 	$root.$namespace.test = {};
 * 	test is your object in global namespace
 * 	$root.$namespace.test.otherTest = Function
 * 	"otherValue" is a child namespace of "test"
 * 	$root.$namespace.test.otherTest(etc..); etc...
 */
;
(function($root) {
    //Create $namespace as new Container
    $root.$namespace = new $root.$preNamespace.Container();
    //Add sub namespace called tools
    $root.$namespace.tools = {};
    //Added Container and EventSet in the tools namespace
    $root.$namespace.tools.Container = $root.$preNamespace.Container;
    $root.$namespace.tools.EventSet = $root.$preNamespace.EventSet;
    //Remove predefined namespace, to clear dublicates of instances
    delete $root.$preNamespace;
})(window);