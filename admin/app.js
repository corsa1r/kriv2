/**
 * @file implements Application init
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @version 14.07.2014
 */

;
(function($root) {

    $root.$namespace.add(new $root.$namespace.tools.Container, 'dependencies');
    var $dependencies = $root.$namespace.get('dependencies');

    //-----------------------------------------------------------------------------------------------------------------
        $dependencies.add('ui.bootstrap');
        $dependencies.add('ngRoute');
        $dependencies.add('ngCookies');
        $dependencies.add('ngSanitize');
        $dependencies.add('angularFileUpload');
        //$dependencies.add('google-maps');
    //-----------------------------------------------------------------------------------------------------------------

    $root.$namespace.add(angular.module(document.getElementsByTagName('html')[0].getAttribute('ng-app'), $dependencies.toArray()), 'app');

    $root.$namespace.get('app').config(function($routeProvider, $locationProvider, $httpProvider) {
        $root.$namespace.get('routes').each(function(route) {
            $routeProvider.when(route.when, route);
        });

        $routeProvider.otherwise({
            redirectTo: $root.$namespace.get('routes').get($root.$namespace.get('routes').indexOf('default')).when
        });
    });

})(window);