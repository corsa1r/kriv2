;(function($root) {

    "use strict";

    var routes = new $root.$namespace.tools.Container();
    
    //---------------------------------------------------------//
    
    
    routes.add({
        when: '/dashboard',
        controller: 'DashboardController',
        templateUrl: 'templates/sys/dashboard.html',
        linkName: 'Dashboard'
    }, 'default');
    
    routes.add({
        when: '/languages',
        templateUrl: 'templates/sys/languages.html',
        controller: 'LanguagesController',
        linkName: 'Languages',
        accessId: '0'
    });
    
    routes.add({
        when: '/languages/edit/:lang_id',
        templateUrl: 'templates/sys/languages-edit.html',
        controller: 'LanguagesEditController',
        accessId: '-1'
    });
    
    routes.add({
        when: '/languages/add',
        templateUrl: 'templates/sys/languages-add.html',
        controller: 'LanguagesAddController',
        accessId: '-1'
    });

    routes.add({
        when: '/code/generator',
        controller: 'CodeGeneratorController',
        templateUrl: 'templates/sys/code_generator.html',
        linkName: 'Code Generator',
        accessId: "0"
    });

    routes.add({
        when: '/explorer/:path',
        controller: 'ExplorerController',
        templateUrl: 'templates/sys/explorer.html',
        linkName: 'Explorer',
        accessId: "0"
    });

    routes.add({
        when: '/configurations',
        controller: 'ConfigurationController',
        templateUrl: 'templates/sys/configuration.html',
        linkName: 'Configurations',
        accessId: "0"
    });

    routes.add({
        when: '/editor/open/:file_path',
        controller: 'EditorController',
        templateUrl: 'templates/sys/editor.html',
        linkName: 'Editor',
        accessId: '-1'
    });

    routes.add({
        when: '/admins',
        templateUrl: 'templates/sys/admins.html',
        controller: 'AdminsController',
        accessId: '0',
        linkName: 'Administrators'
    });

    routes.add({
        when: '/admin/edit/user/:user_id',
        templateUrl: 'templates/sys/edit_admin.html',
        controller: 'EditAdminController',
        accessId: '-1'
    });
    
    routes.add({
        when: '/add/admin/user',
        controller: 'AddAdminController',
        templateUrl: "templates/sys/add_admin.html",
        accessId: "-1"
    });

    routes.add({
        when: '/groups',
        templateUrl: 'templates/sys/groups.html',
        controller: 'GroupsController',
        accessId: '0',
        linkName: 'Admin groups'
    });

    routes.add({
        when: '/admin/edit/group/:group_id',
        templateUrl: 'templates/sys/edit_group.html',
        controller: 'EditGroupController',
        accessId: '-1'
    });

    routes.add({
        when: '/login',
        templateUrl: 'templates/sys/login.html',
        controller: 'LoginController',
        accessId: '-1'
    });
    
    routes.add({
        when: '/translations',
        controller: 'TranslationsController',
        templateUrl: 'templates/sys/translations.html',
        linkName: ' Translations',
        accessId: '0'
    });
    
    /*-CodeGenerator_marker-*/
    
    routes.add({
        when: '/logout',
        template: '',
        controller: function($rootScope, $location, $cookieStore, http) {
            http.getNative('../server/sys@AdminController/logout').then(function() {
                $rootScope.userInfo = null;
                $rootScope.isLogged = false;
                localStorage.user = $rootScope.userInfo;
                $location.path('/login');
                $cookieStore.remove('admin.logged');
            });
        },
        linkName: 'Logout'
    });

    $root.$namespace.add(routes, 'routes');
})(window);
