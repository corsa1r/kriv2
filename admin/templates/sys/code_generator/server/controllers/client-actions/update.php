    public function update($id) {
        $data = $this->_inputHandler->post->data;
    
        ${component_name_lcfirst}Model = new \MODELS\{component_name_ucfirst}Model();
        return ${component_name_lcfirst}Model->update($id, $data);
    }

