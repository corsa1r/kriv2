    public function add() {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }
        
        $data = $this->_inputHandler->post->data;

        ${component_name_lcfirst}Model = new \MODELS\{component_name_ucfirst}Model();
        return ${component_name_lcfirst}Model->add($data);
    }

