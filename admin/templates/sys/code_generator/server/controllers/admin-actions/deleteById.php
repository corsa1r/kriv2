    public function deleteById($id) {
        if(!$this->_session->get('admin')){
            return $this->invalidSession();
        }

        ${component_name_lcfirst}Model = new \MODELS\{component_name_ucfirst}Model();
        return ${component_name_lcfirst}Model->deleteById($id);
    }

