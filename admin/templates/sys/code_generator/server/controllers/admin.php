<?php

namespace CONTROLLERS\ADMIN;

/**
 * Description of {component_name_ucfirst}Controller
 *
 * @version {date_added}
 */
class {component_name_ucfirst}Controller extends \CORE\BaseController {

    protected $_dependency = Array(
        'Session:logged-admin',
        'InputHandler'
    );

    public function __construct() {
        parent::__construct();
    }

    /*-CodeGenerator_marker-*/

}
