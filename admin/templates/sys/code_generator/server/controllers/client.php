<?php

namespace CONTROLLERS\CLIENT;

/**
 * Description of HomeController
 *
 * @version {date_added}
 */
class {component_name_ucfirst}Controller extends \CORE\BaseController {

    protected $_dependency = array(
        'InputHandler'
    );
    
    public function __construct() {
        parent::__construct();
    }

    /*-CodeGenerator_marker-*/
    
}
