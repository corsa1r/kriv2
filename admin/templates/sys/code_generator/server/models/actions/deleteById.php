    public function deleteById($id) {
        return $this->prepare('DELETE FROM `{table_name}` WHERE id = :id')->execute(array(':id' => (int) $id));
    }

