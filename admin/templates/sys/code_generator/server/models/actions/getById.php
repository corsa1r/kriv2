    public function getById($id) {
        $result = $this->prepare('SELECT * FROM {table_name} WHERE id = :id')->execute(array(':id' => $id))->fetchRowAssoc();
        return $result;
    }

