    public function getAll(){
        $result = $this->prepare('SELECT * FROM `{table_name}` ORDER BY id DESC')->execute()->fetchAllAssoc();
        return $result;
    }
     
     