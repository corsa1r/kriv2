<?php

/*
 * 
 * @version {date_added}
 */

namespace MODELS;

class {component_name_ucfirst}Model extends \CORE\Model {

    public function __construct() {
        parent::__construct('link');
    }

/*-CodeGenerator_marker-*/
    
}
