    $scope.getAll = function() {
        http.get('../server/admin@{component_name_ucfirst}Controller/getAll/').then(function (response) {
            if (!response.state || !response.data) {
                return false;
            }

            $scope.items = response.data;
        });
    }

