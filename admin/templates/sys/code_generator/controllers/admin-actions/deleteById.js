$scope.deleteById = function (id) {
    //bootbox.confirm("Are you sure you want to delete this item?", function (state) {
    //if (state) {
    $root.$namespace.tools.ArrayTool.each($scope.items, function (key, value) {
        if (value.id == id) {
            http.get('../server/admin@{component_name_ucfirst}Controller/deleteById/' + id).then(function (response) {
            if (!response.state || !response.data) {
                return false;
            }
            
            $scope.items.splice(key, 1);
            return true;
        });
        }
    });
//}
//        });

    
}

