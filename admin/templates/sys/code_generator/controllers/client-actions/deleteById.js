    $scope.deleteById = function (id) {
        http.get('./server/client@{component_name_ucfirst}Controller/deleteById/' + id).then(function (response) {
            if (!response.state || !response.data) {
                return false;
            }
            
            return true;
        });
    }

