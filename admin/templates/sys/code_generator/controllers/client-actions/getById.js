    $scope.getById = function(id) {
        http.get('./server/client@{component_name_ucfirst}Controller/getById/' + id).then(function (response) {
            if (!response.state || !response.data) {
                return false;
            }

            $scope.currentItem = response.data;
        });
    }

