    $scope.getAll = function() {
        http.get('./server/client@{component_name_ucfirst}Controller/getAll/').then(function (response) {
            if (!response.state || !response.data) {
                return false;
            }

            $scope.items = response.data;
        });
    }

