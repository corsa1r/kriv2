/**
 *
 * @file implements {component_name_ucfirst}Controller
 * @version {date_added}
 */
;
(function ($root, $dom, $u, $angular) {

    "use strict";

    $root.$namespace.get('app').controller('{component_name_ucfirst}Controller', ['$scope', '$rootScope', '$timeout', '$interval', 'http', '$location', '$routeParams', 'ToolsFactory', function ($scope, $rootScope, $timeout, $interval, http, $location, $routeParams, ToolsFactory) {

        $scope.items = [];
        $scope.currentItem = {};
        
/*-CodeGenerator_marker-*/

        }]);

})(window, document, undefined, window.angular);