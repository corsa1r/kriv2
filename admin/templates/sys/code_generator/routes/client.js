routes.add({
        when: '{when}',
        controller: '{component_name_ucfirst}Controller',
        templateUrl: 'templates/{component_name_underscore_lowercase}.html'
    });
    
    