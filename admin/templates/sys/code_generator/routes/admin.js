routes.add({
        when: '{when}',
        controller: '{component_name_ucfirst}Controller',
        templateUrl: 'templates/{component_name_underscore_lowercase}.html',
        linkName: '{link_name}',
        accessId: '{access_id}'
    });
    
    