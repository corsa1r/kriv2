/**
 *
 * @file implements TranslationsController
 * @version 09.10.2014, 17:29:30
 */
;
(function ($root, $dom, $u, $angular) {

    "use strict";

    $root.$namespace.get('app').controller('TranslationsController', ['$scope', '$rootScope', '$timeout', '$interval', 'http', '$location', '$routeParams', 'ToolsFactory', '$q', function ($scope, $rootScope, $timeout, $interval, http, $location, $routeParams, ToolsFactory, $q) {

            $scope.langTranslations = [];
            $scope.newLangKey = "";
            $scope.keysTags = [];

            $scope.getTranslates = function () {
                http.get('../server/sys@TranslationsController/getTranslates').then(function (response) {
                    if (response.state) {
                        $scope.langTranslations = response.data;

                        var temp = [];

                        ToolsFactory.ArrayTool.each(response.data, function (i, language) {
                            language.updateAllProgress = 0;
                            temp = language.translations;
                            return;
                        });

                        ToolsFactory.ArrayTool.each(temp, function (i, v) {
                            var tagName = v.key.split('.').shift();

                            if ($scope.keysTags.indexOf(tagName) === -1) {
                                $scope.keysTags.push(tagName);
                            }
                        });

                        $timeout(function () {
                            $scope.$apply();
                        });
                    }
                });
            };

            $scope.getTranslates();

            $scope.updateAllProgress = 0;

            $scope.updateTotalAll = function () {
                var defer = $q.defer();

                $scope.updateAllProgress = 0;

                var current = 0;
                var max = $scope.langTranslations.length;

                ToolsFactory.ArrayTool.each($scope.langTranslations, function (i, translation) {
                    $scope.updateAllRows(translation).then(function () {

                        current++;

                        $scope.updateAllProgress = current / max * 100;
                        if ($scope.updateAllProgress === 100) {
                            defer.resolve();
                        }
                    });
                });

                return defer.promise;
            };

            $scope.updateAllRows = function (translation) {
                var defer = $q.defer();

                translation.updateAllProgress = 0;
                translation.updateInProgress = true;

                if (!$scope.canUpdateAll(translation)) {
                    bootbox.alert('Cannot update right now. Please wait until all updates which are in progress are finished');
                    translation.updateInProgress = false;
                    return false;
                }

                var current = 0;
                var max = translation.translations.length;

                ToolsFactory.ArrayTool.each(translation.translations, function (r, translationRow) {
                    $scope.updateRow(translationRow).then(function () {
                        current++;

                        var percents = current / max * 100;
                        translation.updateAllProgress = parseFloat(percents.toFixed(2));

                        if (percents === 100) {
                            defer.resolve();
                        }
                    });
                });

                return defer.promise;
            };

            $scope.canUpdateAll = function (translation) {
                var can = ToolsFactory.ArrayTool.each(translation.translations, function (r, translationRow) {
                    if (translationRow.updateInProgress) {
                        return false;
                    }
                });

                return can === false ? false : true;
            };

            $scope.updateRow = function (translationRow) {
                var defer = $q.defer();

                translationRow.updateInProgress = true;

                http.post('../server/sys@TranslationsController/updateRow', translationRow).then(function (response) {
                    if (response.state) {
                        if (response.data.state) {
                            translationRow.updateInProgress = false;
                            defer.resolve();
                        } else {
                            bootbox.alert("Server error: " + JSON.stringify(response));
                        }
                    } else {
                        bootbox.alert("Server error: " + response.errorMessage);
                    }
                });

                return defer.promise;
            };

            $scope.addNewKey = function () {
                if (!$scope.newLangKey.match(/^[a-z\_\.]+$/)) {
                    bootbox.alert("Invalid translation key must match [a-z _ .]");
                    return false;
                }

                http.post('../server/sys@TranslationsController/addKey', {
                    key: $scope.newLangKey
                }).then(function (response) {
                    if (response.state) {
                        if (response.data.state) {
                            $scope.newLangKey = "";
                            $scope.updateTotalAll().then(function () {
                                $scope.getTranslates();
                            });
                        } else {
                            bootbox.alert('Cannot add right now');
                        }
                    } else {
                        bootbox.alert('Cannot add right now');
                    }
                });
            };

            $scope.enterChanges = function (language) {
                language.updateAllProgress = 0;
                $scope.updateAllProgress = 0;
            };

            $scope.checkAvailableKey = function () {
                $scope.keyExists = false;

                ToolsFactory.ArrayTool.each($scope.langTranslations, function (i, v) {
                    ToolsFactory.ArrayTool.each(v.translations, function (l, vv) {
                        if (vv.key === $scope.newLangKey) {
                            $scope.keyExists = true;
                        }
                    });

                    return false;
                });

            };
        }]);

})(window, document, undefined, window.angular);