;
(function($root) {

    "use strict";

    $root.$namespace.get('app').controller('GroupsController', function($rootScope, $scope, http) {

        $scope.newGroupName = "";
        $scope.groups = [];
        $scope.addNewGroupSuccess = false;

        $scope.addNewGroup = function() {
            var max = angular.element(".group-id").first().text();

            angular.element(".group-id").each(function() {
                var val = parseInt(angular.element(this).text());

                if (val > max) {
                    max = val;
                }
            });

            var groupParams = {
                id: parseInt(max) + 1,
                name: $scope.newGroupName,
                accessIds: []
            }

            http.post('../server/sys@AdminGroupsController/add', groupParams).then(function(response) {
                if (response.state) {
                    $scope.groups.push(groupParams);
                    $scope.newGroupName = "";
                }
            });
        };

        http.get('../server/sys@AdminGroupsController/getAll').then(function(response) {
            if (response.state) {
                $scope.groups = response.data;
            }
        });

        $scope.removeGroup = function(id) {
            bootbox.confirm("Are you sure you want to delete this Group ?", function(state) {
                if (state) {
                    $root.$namespace.tools.ArrayTool.each($scope.groups, function(key, value) {
                        if (value.id == id) {
                            http.get('../server/sys@AdminGroupsController/delete/' + id).then(function(response) {
                                if (response.state) {
                                    $scope.groups.splice(key, 1);
                                }
                            });
                            return;
                        }
                    });
                }
            });
        };
    });
})(window);