;
(function($root) {

    "use strict";

    $root.$namespace.get('app').controller('AddAdminController', function($rootScope, $scope, http, $interval) {
        $scope.groups = [];

        http.get('../server/sys@AdminGroupsController/getAll').then(function(response) {
            if (response.state) {
                $scope.groups = response.data;
            }
        });

        $scope.form = {};

        $scope.re_password_error_class = "";

        var interval = $interval(function() {
            if ($scope.form.group_id && $scope.form.username && $scope.form.password && $scope.form.re_password && $scope.form.password === $scope.form.re_password) {
                angular.element('#add-button-submit').removeAttr('disabled');
            } else {
                angular.element('#add-button-submit').attr('disabled', 'true');
            }

            if ($scope.form.password === $scope.form.re_password) {
                $scope.re_password_error_class = "";
            } else {
                $scope.re_password_error_class = "has-error";
            }
        }, 50);

        $scope.submitForm = function() {
            http.post('../server/sys@AdminController/add', $scope.form).then(function(response) {
                if (response.state) {
                    if (response.data.state) {
                        $scope.errorRegister = false;
                        $scope.successRegister = true;
                        $scope.form = {};
                    } else {
                        $scope.successRegister = false;
                        $scope.errorRegister = true;
                        $scope.errorMessageRegister = response.data.errorMessage;
                    }
                }
            });
        };

        $scope.$on('$destroy', function() {
            $interval.cancel(interval);
        });
    });
})(window);