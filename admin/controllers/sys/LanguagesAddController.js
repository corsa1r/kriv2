;(function($root, $dom, $u){
    
    $root.$namespace.get('app').controller('LanguagesAddController', ['$scope', 'http', '$routeParams', 'ToolsFactory', function($scope, http, $routeParams, ToolsFactory) {
        $scope.insertSuccess = false;
        $scope.insertFailure = false;
        $scope.default_language = null;
        $scope.hasDefault = false;
        $scope.language = {};
        
        http.get('../server/sys@LanguagesController/getDefault/').then(function(response) {
            if (response.state && response.data) {
                $scope.default_language = response.data;
                if (parseInt($scope.default_language.id) > 0) {
                    $scope.hasDefault = true;
                }
            }
        });
        
        $scope.insertLanguage = function() {
            var data = $scope.language;
            
            if (data) {
                http.post('../server/sys@LanguagesController/insert', data).then(function(response) {
                    if (response.data.state) {
                        $scope.insertSuccess = true;
                    } else {
                        $scope.insertFailure = true;
                    }
                });
            }
        };
    }]);
    
})(window, document);