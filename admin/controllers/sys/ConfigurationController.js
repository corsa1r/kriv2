/**
 * @file Implements ConfigurationController class
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @version 2014-7-23 14:18:58
 */
/**
 * @param {Object} $root    - window
 * @param {Object} $dom     - document
 * @param {undefined} $u    - undefined
 * @returns {undefined}
 */
(function($root, $dom, $u) {

    "use strict";

    $root.$namespace.get('app').controller('ConfigurationController', function($scope, Config, ToolsFactory, $timeout) {
        $scope.configurations = [];

        $scope.readOnlyList = new $root.$namespace.tools.Container();

        $scope.readOnlyList.add('session_key');

        $scope.getAll = function() {

            $scope.configurations = [];

            Config.getAll().then(function(data) {
                ToolsFactory.ArrayTool.each(data, function(key, value) {
                    $scope.configurations.push({
                        key: key,
                        value: value,
                        readOnly: Boolean($scope.readOnlyList.indexOf(key) !== -1),
                        updated: false
                    });
                });
            });
        };

        $scope.update = function(config) {
            Config.set(config.key, config.value).then(function() {
                config.updated = true;
                $timeout(function() {
                    config.updated = false;
                }, 1400);
            });
        };

        $scope.remove = function(config) {
            bootbox.confirm('Are you sure you want to delete ' + config.key, function(state) {
                if (state) {
                    Config.remove(config.key).then(function() {
                        var index = ToolsFactory.ArrayTool.each($scope.configurations, function(index, value) {
                            if (value.key === config.key) {
                                return index;
                            }
                        });

                        $scope.configurations.splice(index, 1);
                        $timeout(function() {
                            $scope.$apply();
                        });
                    });
                }
            });
        };

        $scope.add = function() {
            bootbox.prompt('Enter key name', function(keyName) {
                if (keyName) {
                    Config.set(keyName, '').then(function() {
                        $scope.getAll();
                    });
                }
            });
        };

        $scope.getAll();
    });

    $root.$namespace.get('app').filter('Configurations_StrongifyGroups', function($sce) {
        return function(input) {
            var exploded = input.split('.');
            var groupName = "";
            var keyName = exploded[0];
            var hasGroupName = false;
            
            if (!angular.isUndefined(exploded[1])) {
                hasGroupName = true;
                keyName = exploded[1];
                groupName = exploded[0];
            }
            
            var span = $dom.createElement('span');
            angular.element(span).attr('class', 'label label-default').html(groupName);
            
            return hasGroupName ? $sce.trustAsHtml(span.outerHTML + " " + keyName) : keyName;            
        };
    });

})(window, document);