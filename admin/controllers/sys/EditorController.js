/**
 * @file Implements EditorController class
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @version 2014-7-22 9:59:55
 */
/**
 * @param {Object} $root    - window
 * @param {Object} $dom     - document
 * @param {undefined} $u    - undefined
 * @returns {undefined}
 */
(function($root, $dom, $u) {

    "use strict";

    $root.$namespace.get('app').controller('EditorController', function($rootScope, $scope, $routeParams, http, $timeout) {

        var editor = ace.edit("editor");
        editor.setReadOnly(true);
        editor.setTheme("ace/theme/monokai");

        $scope.file_path = $routeParams.file_path.split('&').slice(0, -1).join('&');
        $scope.file_path_original = $routeParams.file_path.split('&').join('/');
        $scope.explorerAddon = "=" + $scope.file_path_original.split('/').pop();
        $scope.code = '';

        http.get('../server/sys@EditorController/getFile/?file=' + $routeParams.file_path.split('&').join('/')).then(function(response) {
            if (response.state) {
                if (response.data.errorMessage) {
                    $scope.isError = true;
                    bootbox.alert(response.data.errorMessage);
                    return;
                }

                if (response.data.isWritable) {
                    $scope.isWritable = true;
                }

                editor.setReadOnly(Boolean(!response.data.isWritable));

                editor.getSession().setMode("ace/mode/" + getLanguage(response.data.extension));
                editor.setValue(response.data.content);
                editor.clearSelection();
            }
        });

        var savedTimeout = null;

        $scope.save = function() {
            http.post('../server/sys@EditorController/saveFile', {
                file: $routeParams.file_path.split('&').join('/'),
                content: editor.getValue()
            }).then(function(response) {
                if (!response.state || !response.data.state) {
                    bootbox.alert(response.data.errorMessage);
                    return;
                } else {
                    $scope.savedBytes = response.data.savedBytes;
                    $scope.saved = true;
                    $timeout.cancel(savedTimeout);
                    savedTimeout = $timeout(function() {
                        $scope.saved = false;
                    }, 1700);
                    return;
                }
            });
        };

        function getLanguage(extension) {
            switch (extension) {
                case 'js' :
                    {
                        return 'javascript';
                        break;
                    }
                case 'php' :
                    {
                        return 'php';
                        break;
                    }
                case 'html' :
                    {
                        return 'html';
                        break;
                    }
                case 'css' :
                    {
                        return 'css';
                        break;
                    }
                default :
                    {
                        return 'plain_text';
                    }
            }
        }
        ;

        var keys = {};

        angular.element($root).on('keydown', function(e) {
            keys[e.keyCode] = true;

            if (keys[17] && keys[83]) {
                $scope.save();
                e.preventDefault();
            }
        });

        angular.element($root).on('keyup', function(e) {
            delete keys[e.keyCode];
        });

        $scope.$on('$destroy', function() {
            angular.element($root).off('keydown').off('keyup');
            editor.destroy();
        });
    });

})(window, document);