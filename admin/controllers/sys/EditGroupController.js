;
(function($root) {

    "use strict";

    $root.$namespace.get('app').controller('EditGroupController', function($rootScope, $scope, http, $routeParams) {
        $scope.group = {};

        $scope.routes = [];

        $scope.selection = [];

        http.get('../server/sys@AdminGroupsController/getGroupById/' + $routeParams.group_id).then(function(response) {
            if (response.state) {
                $scope.group = response.data;
                $scope.selection = response.data.accessIds;

                for (var x in $scope.selection) {
                    $scope.selection[x] = $scope.selection[x].toString();
                }
            }
        });

        $root.$namespace.get('routes').each(function(route) {
            $scope.routes.push(route);
        });

        $scope.updateGroup = function() {
            var accessIds = [];

            angular.element('input[name="selectedAccessIds[]"]').each(function() {
                if (angular.element(this).val() > -1 && angular.element(this).val() !== '' && angular.element(this).is(':checked')) {
                    accessIds.push(angular.element(this).val());
                }
            });

            http.post('../server/sys@AdminGroupsController/edit/' + $routeParams.group_id, {
                name: $scope.group.name,
                accessIds: accessIds
            }).then(function(response) {
                if (response.state) {
                    $scope.updateSuccess = true;
                }
            });
        };

        $scope.toggleSelection = function toggleSelection(routeIndex) {
            var idx = $scope.selection.indexOf(routeIndex);
            if (idx > -1) {
                $scope.selection.splice(idx, 1);
            } else {
                $scope.selection.push(routeIndex);
            }
        };
    });
})(window);