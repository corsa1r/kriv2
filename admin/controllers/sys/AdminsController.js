;
(function($root) {

    "use strict";

    $root.$namespace.get('app').controller('AdminsController', function($rootScope, $scope, http, $location, $timeout) {
        $scope.adminUsers = [];

        http.get('../server/sys@AdminController/getAll/1').then(function(response) {
            if (response.state) {
                $scope.adminUsers = response.data;
            }
        });

        $scope.confirm = function(message, id) {
            bootbox.confirm(message, function(state) {
                if (state) {
                    http.get('../server/sys@AdminController/delete/' + id).then(function(response) {
                        if (response.state) {
                            var indexOfUser;

                            for (var x in $scope.adminUsers) {
                                if ($scope.adminUsers[x].id === id) {
                                    indexOfUser = x;
                                    break;
                                }
                            }

                            $scope.adminUsers.splice(indexOfUser, 1);

                            $timeout(function() {
                                $scope.$digest();
                            });
                        }
                    });
                }
            });
        };
    });
})(window);
