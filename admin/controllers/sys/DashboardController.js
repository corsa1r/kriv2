;
(function($root) {

    "use strict";

    $root.$namespace.get('app').controller('DashboardController', function($scope, ToolsFactory, http) {
        var noData = [
            {name: 'No data !', total: 0}
        ];

        var browsersChart = Morris.Bar({
            element: 'browsersChart',
            data: noData,
            xkey: 'name',
            ykeys: ['total'],
            labels: ['Total hits by period']
        });

        var platformChart = Morris.Bar({
            element: 'platformChart',
            data: noData,
            xkey: 'name',
            ykeys: ['total'],
            labels: ['Total hits by period']
        });

        var donutNoData = [
            {label: "No data !", value: 0}
        ];

        var donutPlatforms = Morris.Donut({
            element: 'donut-platforms',
            data: donutNoData
        });

        var timeInterval = new ToolsFactory.EventSet();

        timeInterval.on('change', getAnalyticsData);

        function translateTime(from, to) {
            return {
                from: isNaN(from) ? 0 : from / 1000,
                to: isNaN(to) ? 0 : to / 1000
            };
        }
        
        var date_from = angular.element('.date_from').datetimepicker({pickTime: false}).on('dp.change', function (e) {
            var time = translateTime(new Date(date_from.val()).getTime(), new Date(date_to.val()).getTime());
            timeInterval.fire('change', time.from, time.to);
        });
        
        var date_to = angular.element('.date_to').datetimepicker({pickTime: false}).on('dp.change', function (e) {
            var time = translateTime(new Date(date_from.val()).getTime(), new Date(date_to.val()).getTime());
            timeInterval.fire('change', time.from, time.to);
        });
        
        $('.date_to').datetimepicker();
        
        timeInterval.fire('change', 0, 0);

        function getAnalyticsData(from, to) {
            if ((!from && from !== 0) || (!to && to !== 0)) {
                return;
            }

            http.get('../server/sys@AnalyticsController/get/' + from + "/" + to).then(function(response) {
                if (response.state) {
                    browsersChart.setData(response.data.browsers.length ? response.data.browsers : noData);
                    platformChart.setData(response.data.platforms.length ? response.data.platforms : noData);

                    var data = [
                        {label: "Desktop", value: response.data.desktopCount},
                        {label: "Mobile", value: response.data.mobileCount}
                    ];

                    if (data[0].value == 0 && data[1].value == 0) {
                        data = donutNoData;
                    }

                    $scope.desktopTotal = data[0].value || 0;
                    $scope.mobileTotal = data[1] ? data[1].value : 0;

                    donutPlatforms.setData(data);
                }
            });
        }

        getAnalyticsData();
    });
})(window);