/**
 * @file Implements CodeGeneratorController class
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @version 2014-10-6 16:25:25
 */
/**
 * @param {Object} $root    - window
 * @param {Object} $dom     - document
 * @param {undefined} $u    - undefined
 * @returns {undefined}
 */
(function ($root, $dom, $u) {

    "use strict";

    $root.$namespace.get('app').controller('CodeGeneratorController', function ($scope, http, ToolsFactory, $timeout, $location) {

        $scope.component = {};

        $scope.component.name = "";
        $scope.component.template_name = "";
        $scope.component.dbTable = "0";

        $scope.component.model = [];
        $scope.component.model_enabled = false;

        var modelMethod = function (name) {
            this.name = name;
            this.selected = false;

            this.inClientController = false;
            this.inAdminController = false;
        };

        $scope.component.model.push(new modelMethod('add'));
        $scope.component.model.push(new modelMethod('getById'));
        $scope.component.model.push(new modelMethod('getAll'));
        $scope.component.model.push(new modelMethod('deleteById'));
        $scope.component.model.push(new modelMethod('deleteAll'));
        $scope.component.model.push(new modelMethod('update'));

        $scope.component.client = {
            url_path: '',
            enabled: false
        };

        $scope.component.admin = {
            url_path: '',
            enabled: false,
            accessId: '0',
            link_name: ''
        };

        $scope.typeComponentName = function ($event) {
            var url_path = angular.element($event.target).val();

            $scope.component.name = url_path;

            $scope.component.client.url_path = $scope.buildName(url_path, "/");
            $scope.component.admin.url_path = $scope.buildName(url_path, "/");

            $scope.component.admin.link_name = ToolsFactory.Str.ucwords($scope.buildName(url_path, " "));
            $scope.component.template_name = $scope.buildName(url_path, "_");
        };

        $scope.isUpperCase = function (char) {
            return !!/[A-Z]/.exec(char[0]);
        };

        $scope.buildName = function (stringPath, SEPARATOR) {
            var urlLetters = [];
            var urlPathSeparator = SEPARATOR || "/";

            for (var i = 0; i < stringPath.length; i++) {
                if ($scope.isUpperCase(stringPath.charAt(i)) && i !== 0) {
                    urlLetters.push(urlPathSeparator);
                }

                urlLetters.push(stringPath.charAt(i).toLowerCase());
            }

            return urlPathSeparator + urlLetters.join('');
        };

        $scope.selectAllToggle = function (m) {
            ToolsFactory.ArrayTool.each(m, function (i, value) {
                m[i].selected = !value.selected;
            });

            $timeout(function () {
                $scope.$digest();
            });
        };

        $scope.selectAll = function (model, reverse) {
            ToolsFactory.ArrayTool.each(model, function (i) {
                model[i].selected = reverse === true ? false : true;
            });

            $timeout(function () {
                $scope.$digest();
            });
        };

        $scope.databaseTables = [];

        http.get('../server/sys@CodeGeneratorController/getDbTables').then(function (response) {
            if (response.state) {
                $scope.databaseTables = response.data;
            }
        });

        $scope.generatedFiles = [];

        $scope.generate = function () {
            if (!$scope.component.name.match(/^[a-zA-Z]+$/gi)) {
                bootbox.alert('Invalid component name. Only a-zA-Z');
                return false;
            }

            if (!$scope.component.client.enabled && !$scope.component.admin.enabled) {
                bootbox.alert('Select client or admin component');
                return false;
            }

            if ($scope.component.model_enabled) {
                if ($scope.component.dbTable === null || $scope.component.dbTable.toString() === '0') {
                    bootbox.alert('Select database table !');
                    return false;
                }
            }

            http.post('../server/sys@CodeGeneratorController/generate', $scope.formatVaskoFormatInfo($scope.component)).then(function (response) {
                if (response.state) {

                    $scope.generatedFiles = [];

                    ToolsFactory.ArrayTool.each(response.data.updated, function (i, value) {
                        $scope.generatedFiles.push({
                            path: value,
                            status: 'updated'
                        });
                    });

                    ToolsFactory.ArrayTool.each(response.data.created, function (i, value) {
                        $scope.generatedFiles.push({
                            path: value,
                            status: 'created'
                        });
                    });

                    $timeout(function () {
                        $scope.$digest();
                    });
                    return;
                }

                bootbox.alert('Server error, contact Vasko for this thing !');
            });
        };

        $scope.formatVaskoFormatInfo = function (information) {
            var info = {
                componentName: information.name,
                scopes: {},
                actions: {
                    client: [],
                    admin: [],
                    model: []
                },
                dbTable: $scope.component.dbTable
            };

            if (information.client.enabled) {
                info.scopes.client = {
                    when: information.client.url_path
                };
            }

            if (information.admin.enabled) {
                info.scopes.admin = {
                    when: information.admin.url_path,
                    linkName: information.admin.link_name,
                    accessId: information.admin.accessId
                };
            }

            ToolsFactory.ArrayTool.each($scope.component.model, function (i, method) {
                if (method.selected) {
                    info.actions.model.push(method.name);

                    if (method.inClientController) {
                        info.actions.client.push(method.name);
                    }

                    if (method.inAdminController) {
                        info.actions.admin.push(method.name);
                    }
                }
            });

            return info;
        };

        $scope.openInEditor = function (generatedFile) {
            $location.path('/editor/open/' + ToolsFactory.Str.replace('/', '&', generatedFile.path));
        };
    });

})(window, document);