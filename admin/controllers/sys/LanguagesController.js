;(function($root, $dom, $u){
    
    $root.$namespace.get('app').controller('LanguagesController', ['$scope', 'http', '$routeParams', '$timeout','ToolsFactory', function($scope, http, $routeParams, $timeout, ToolsFactory) {
        
        $scope.languages = [];
        
        http.get('../server/sys@LanguagesController/getAll/false').then(function(response) {
            if (response && response.data) {
                $scope.languages = ToolsFactory.ArrayTool.values(response.data);
            }
        });
        
        $scope.changeStatus = function(lang) {
            
            if(lang.default === '1') {
                bootbox.alert('Cannot disable default language !');
                return false;
            }
            
            var data = {
                id: lang.id,
                active: lang.active === '1' ? '0' : '1'
            };
            
            lang.active = data.active;
            
            $timeout(function () {
                $scope.$digest();
            });
            
            http.post('../server/sys@LanguagesController/changeStatus', data).then(function(response) {
                if (response.data.state) {
                    
                }
            });
        };
        
        $scope.deleteLanguage = function(lang) {
            bootbox.confirm("Are you sure you want to delete " + lang.title + " ?", function (state) {
                if(state) {
                    
                    http.get('../server/sys@LanguagesController/delete/' + lang.id).then(function(response) {
                        if (response && response.state) {
                            $scope.languages.splice($scope.languages.indexOf(lang), 1);
                    
                            $timeout(function () {
                                $scope.$digest();
                            });
                        }
                    });
                }
            });
        };
    }]);
    
})(window, document);