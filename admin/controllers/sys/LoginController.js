;
(function($root) {

    "use strict";

    $root.$namespace.get('app').controller('LoginController', function($scope, $rootScope, http, $location, $cookieStore, $log) {
        if ($rootScope.isLogged === true) {
            $location.path('/dashboard');
        }

        $scope.username = "";
        $scope.password = "";


        $scope.loginError = false;

        $scope.login = function() {

            angular.element("#login-submit").attr('disabled', true);

            http.post('../server/sys@AdminController/login', {
                username: $scope.username,
                password: $scope.password
            }).then(function(response) {

                $scope.loginError = !response.state;

                angular.element("#login-submit").attr('disabled', false);

                if (response.state) {
                    $rootScope.userInfo = response.data;
                    //$rootScope.userInfo.accessIds = angular.fromJson($rootScope.userInfo.accessIds);
                    $rootScope.isLogged = true;

                    localStorage.user = angular.toJson($rootScope.userInfo);

                    $cookieStore.put('admin.logged', true);
                    $location.path('/dashboard');
                }
            });
        };

        $scope.keydown = function(event) {
            if (event.keyCode === 13) {
                $scope.login();
            }
        };
    });
})(window);