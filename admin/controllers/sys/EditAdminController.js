;
(function($root) {

    "use strict";
    $root.$namespace.get('app').controller('EditAdminController',
            function($rootScope, $scope, $routeParams, http, $location, $interval, $timeout) {
                $scope.id = $routeParams.user_id;
                $scope.userInfo = {};

                http.get('../server/sys@AdminController/getAdminById/' + $routeParams.user_id).then(function(response) {

                    if (!response.state) {
                        $location.path('404');
                    }

                    $scope.form = {
                        username: response.data.username,
                        password: "",
                        re_password: "",
                        first_name: response.data.first_name,
                        last_name: response.data.last_name
                    };

                    $scope.re_password_error_class = "";

                    $interval(function() {
                        $scope.re_password_error_class = $scope.form.password === $scope.form.re_password ? "" : "has-error";
                    }, 100);

                    $scope.userInfo = response.data;

                    $scope.successUpdate = false;
                    $scope.errorUpdate = false;
                    $scope.errorMessageUpdate = "";

                    $scope.submitForm = function() {
                        angular.element('#edit-button-submit').attr('disabled', 'true');

                        http.post('../server/sys@AdminController/edit/' + $scope.userInfo.id, $scope.form).then(function(response) {
                            angular.element('#edit-button-submit').removeAttr('disabled');
                            if (response.data.state) {
                                $scope.successUpdate = true;
                            } else {
                                $scope.errorMessageUpdate = response.data.errorMessage;
                                $scope.errorUpdate = true;
                            }
                        });
                    };

                    $scope.groups = [];
                    $scope.selectForm = undefined;

                    http.get('../server/sys@AdminGroupsController/getAll').then(function(response) {
                        if (response.state) {
                            $scope.groups = response.data;
                        }
                    });
                });
            });
})(window);