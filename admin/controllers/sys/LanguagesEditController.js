;(function($root, $dom, $u){
    
    $root.$namespace.get('app').controller('LanguagesEditController', ['$scope', 'http', '$routeParams', 'ToolsFactory', function($scope, http, $routeParams, ToolsFactory) {
        $scope.language = null;
        $scope.hasDefault = false;
        
        http.get('../server/sys@LanguagesController/get/' + $routeParams.lang_id).then(function(response) {
            if (response && response.data) {
                $scope.language = response.data;
            }
        });
        
        http.get('../server/sys@LanguagesController/getDefault/').then(function(response) {
            if (response.state && response.data) {
                $scope.default_language = response.data;
                if (parseInt($scope.default_language.id) > 0) {
                    $scope.hasDefault = true;
                }
            }
        });
        
        $scope.updateLanguage = function() {
            var data = $scope.language;
            
            if (data) {
                http.post('../server/sys@LanguagesController/update', data).then(function(response) {
                    if (response.data.state) {
                        $scope.updateSuccess = true;
                    } else {
                        $scope.updateFailure = true;
                    }
                });
            }
        };
    }]);
    
})(window, document);