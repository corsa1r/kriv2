;(function($root) {

    "use strict";

    $root.$namespace.get('app').controller('AdminBaseController', function($scope, $rootScope, $location, $cookieStore) {
        $rootScope.isLogged = Boolean($cookieStore.get('admin.logged'));
        $rootScope.userInfo = localStorage.user ? angular.fromJson(localStorage.user) : null;
        
        //Check if user is Logged in
        if (!$rootScope.isLogged) {
            $location.path('/login');
        }
    });
})(window);