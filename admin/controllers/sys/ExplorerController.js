/**
 * @file implements File explorer
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @param {type} $root - window
 * @returns {undefined}
 */
;(function($root) {

    "use strict";

    $root.$namespace.get('app').controller('ExplorerController', function($rootScope, $scope, http, ToolsFactory, $timeout, $routeParams, $upload) {

        $scope.explorerView = 'icons';

        $scope.view = function(name) {
            $scope.explorerView = name;
        };

        var ExplorerItem = function(type, path, items, name, tags) {
            this.type = type || null;
            this.path = path || null;
            this.items = items || 0;
            this.name = name || this.path;
            this.tags = tags || null;

            var tempExtension = this.name.split('.').pop().toLowerCase();

            this.extensionType = tempExtension === 'png' || tempExtension === 'jpg' ? 'image-for-preview' : null;
        };

        ExplorerItem.TYPE_FOLDER  = 'folder';
        ExplorerItem.TYPE_FILE    = 'file';

        $scope.files = [];
        $scope.path = $scope.startDir ? $scope.startDir : '';

        if ($routeParams.path) {
            var url = $routeParams.path.split('=');
            $scope.path = $routeParams.path !== ":path" ? url[0].split('&').join('/') : "";
        }

        $scope.forwardPath = null;
        $scope.selectedItems = new ToolsFactory.Container();
        $scope.lastlySelectedItem = null;

        $scope.loadDir = function() {
            http.get('../server/sys@ExplorerController/getDir/?dir=' + $scope.path).then(function(response) {

                $scope.selectedItems.empty();

                $timeout(function() {
                    $scope.$apply();
                });

                var files = [];
                $scope.files = files;

                if (response.state) {
                    ToolsFactory.ArrayTool.each(response.data.files, function(key, value) {
                        if (ToolsFactory.ArrayTool.isValid(value)) {
                            files.push(new ExplorerItem(ExplorerItem.TYPE_FOLDER, $scope.path + '/' + key, ToolsFactory.ArrayTool.values(value).length, "./" + key, value));
                        } else {
                            var expFile = new ExplorerItem(ExplorerItem.TYPE_FILE, $scope.path + '/' + value, null, value);
                            if ($scope.selectedFileType) {
                                if (expFile.type === $scope.selectedFileType) {
                                    files.push(expFile);
                                }
                            } else {
                                files.push(expFile);
                            }
                        }
                    });
                }

                if ($routeParams.path && url) {
                    if (url[1]) {
                        ToolsFactory.ArrayTool.each(files, function(key, explorerItem) {
                            if (explorerItem.name === url[1]) {
                                $scope.selectedItems.add(explorerItem, explorerItem.name);
                            }
                        });
                    }
                }
            });
        };

        $scope.$watch('path', function() {
            $scope.loadDir();
        });

        $scope.open = function(explorerItem) {
            if (explorerItem.type === 'folder') {
                $scope.path = explorerItem.path;
                $scope.forwardPath = null;
            }
        };

        $scope.canBack = true;

        $scope.back = function() {
            if ($scope.canBack) {
                $scope.forwardPath = $scope.path;
                $scope.path = $scope.path.split('/').slice(0, -1).join('/');

                if ($scope.startDir) {
                    if ($scope.path.substring(0, $scope.startDir) !== $scope.startDir && $scope.path.split('/').length === 1) {
                        $scope.path = $scope.startDir;
                    }
                }

                $scope.searchFileFolder = "";
                $timeout(function() {
                    $scope.$apply();
                });
            }

            return false;
        };

        $scope.toggleBackActionButton = function() {
            $scope.canBack = !$scope.canBack;
        };

        $scope.forward = function() {
            if ($scope.forwardPath) {
                $scope.path = $scope.forwardPath;
            }
        };

        $scope.select = function(explorerItem) {
            if (explorerItem.type === $scope.selectedFileType) {
                $scope.$broadcast('explorer.selected.one', $scope, $scope.explorerID, explorerItem);
            }
        };

        $scope.selectItems = function(items) {

            if (!items.length) {
                return;
            }

            var filteredItems = [];

            ToolsFactory.ArrayTool.each(items, function(key, value) {
                if (value.type === $scope.selectedFileType) {
                    filteredItems.push(value);
                }
            });

            $scope.$broadcast('explorer.selected.multiple', $scope, $scope.explorerID, filteredItems);
        };

        $scope.toggleSelect = function(explorerItem, event) {
            var lastyEmpty = true;

            if ($scope.selectSingleFile && $scope.selectedItems.len() === 1) {
                $scope.selectedItems.empty();
                lastyEmpty = false;
            }

            if (!$scope.selectedItems.get(explorerItem.name) && ($scope.selectSingleFile && lastyEmpty || !($scope.lastlySelectedItem === explorerItem && $scope.selectedItems.len() > 0))) {// Add
                if (explorerItem.type === $scope.selectedFileType || !$scope.selectedFileType) {
                    $scope.selectedItems.add(explorerItem, explorerItem.name);
                    angular.element(event.currentTarget).addClass('success');
                    $scope.lastlySelectedItem = explorerItem;
                } else {
                    return;
                }
            } else {                                          // Remove
                $scope.selectedItems.remove(explorerItem.name);
                angular.element(event.currentTarget).removeClass('success');
            }

            $timeout(function() {
                $scope.$apply();
            });

            switch ($scope.selectedItems.len()) {
                case 0 :
                    $scope.$emit('explorer.deselected.all');
                    break;
                case 1 :
                    $scope.select(explorerItem);
                    break;
                default :
                    $scope.selectItems($scope.selectedItems.toArray());
            }
        };

        $scope.isSelected = function(file) {
            return $scope.selectedItems.has(file.name) ? "success" : null;
        };

        $scope.deleteItems = function() {
            var falsyFolders = 0;
            var mustBeEmpty = [];

            $scope.selectedItems.each(function(explorerItem) {
                if (explorerItem.type === 'folder' && explorerItem.items > 0) {
                    mustBeEmpty.push(explorerItem.name);
                    falsyFolders++;
                }
            });

            if (falsyFolders > 0) {
                bootbox.alert(mustBeEmpty.join(', ') + " must be empty folder" + (mustBeEmpty.length > 1 ? "s" : "") + "!");
                return;
            }

            bootbox.confirm('Are you sure you want to delete ' + $scope.selectedItems.len() + " items?", function(state) {
                if (state) {
                    var items = [];

                    $scope.selectedItems.each(function(explorerItem) {
                        items.push(explorerItem);
                    });

                    http.post('../server/sys@ExplorerController/delete', {
                        items: items
                    }).then(function() {
                        $scope.refresh();
                    });
                }
            });
        };

        $scope.refresh = function() {
            $scope.loadDir();
        };

        $scope.create = function(type) {
            bootbox.prompt('Create new ' + type, function(name) {
                if (name === null) {
                    return;
                }
                if (name.match(/[a-zA-Z0-9\ \.\-\_]+/i)) {
                    http.post('../server/sys@ExplorerController/create', {
                        type: type || null,
                        path: $scope.path + "/" + name
                    }).then(function(response) {
                        if (response.state) {
                            if (response.data.created) {
                                $scope.refresh();
                                angular.element(event.target).val('');
                            } else {
                                bootbox.alert(response.data.errorMessage);
                            }
                        }
                    });
                }
            });
        };

        $scope.rename = function() {
            var explorerItem = null;

            $scope.selectedItems.each(function(item) {
                explorerItem = item;
                return false;
            });

            if (!explorerItem) {
                return;
            }

            bootbox.prompt({
                title: 'Rename ' + explorerItem.name,
                value: explorerItem.name.substr(explorerItem.type === 'folder' ? 2 : 0),
                callback: function(newName) {
                    if (!newName) {
                        return;
                    }
                    http.post('../server/sys@ExplorerController/rename', {
                        type: explorerItem.type,
                        old_name: $scope.path + "/" + explorerItem.name,
                        new_name: $scope.path + "/" + (explorerItem.type === 'folder' ? "./" : "") + newName
                    }).then(function(response) {
                        if (response.state) {
                            if (response.data.renamed) {
                                $scope.refresh();
                            } else {
                                bootbox.alert(response.data.errorMessage);
                            }
                        }
                    });
                }
            });
        };

        $scope.copy = function() {
            var items = [];

            $scope.selectedItems.each(function(item) {
                items.push(item);
            });

            http.post('../server/sys@ExplorerController/copy', {
                items: items
            }).then(function(response) {
                if (response.state) {
                    $scope.refresh();
                }
            });
        };

        $scope.canOpenInEditor = function() {
            if ($scope.fromModal === true) {
                return true;
            }

            if ($scope.selectedItems.len() === 1) {

                var item = null;

                $scope.selectedItems.each(function(explorerItem) {
                    item = explorerItem;
                    return;
                });

                if (item && item.type === 'file') {
                    $scope.openFileInEditorUrl = item.path.split('/').join('&');
                    return false;
                }
            }

            return true;
        };

        $scope.collision = {};
        $scope.collisionQueue = [];
        $scope.collisionQueueNextAction = null;

        $scope.onFileSelect = function($files) {
            $scope.collisionQueue = [];
            $scope.collisionQueueNextAction = null;

            for (var i = 0; i < $files.length; i++) {

                $scope.collision = {};

                var file = $files[i];

                if ($scope.isFileExists(file.name)) {
                    $scope.collisionQueue.push(file);
                } else {
                    $scope.uploadFile(file);
                }
            }

            if ($scope.collisionQueue.length) {
                $scope.cutQueue($scope.collisionQueue);
            }
        };

        $scope.cutQueue = function(queue) {
            if ((queue instanceof Array || angular.isArray(queue)) && queue.length) {

                $scope.collision = {};
                $scope.collision.file = ToolsFactory.ArrayTool.firstValue(queue).name;

                $timeout(function() {
                    $scope.$digest();
                });

                if ($scope.collisionQueueNextAction !== null) {
                    $scope.collision.action = $scope.collisionQueueNextAction;
                    $scope.replaceModalQueue(queue);
                } else {
                    angular.element('#replaceModal-' + ($scope.explorerID || '') + '-id').modal('show').off('hidden.bs.modal').on('hidden.bs.modal', function() {
                        $scope.replaceModalQueue(queue);
                    });
                }
            }
        };

        $scope.replaceModalQueue = function(queue) {
            var file = queue.shift();

            if ($scope.collision.action && $scope.collision.action !== 'cancel') {
                $scope.uploadFile(file, $scope.collision.action);
            }

            $scope.cutQueue(queue);
        };

        $scope.uploadFile = function(file, action) {
            $scope.upload = $upload.upload({
                url: '../server/sys@ExplorerController/upload',
                data: {
                    path: $scope.path,
                    action: action || null
                },
                file: file
            }).success($scope.refresh);
        };

        $scope.isFileExists = function(fileName) {
            var exists = false;

            ToolsFactory.ArrayTool.each($scope.files, function(key, file) {
                if (fileName === file.name) {
                    exists = true;
                    return;
                }
            });

            return exists;
        };

        $scope.replaceActions = function(action) {
            $scope.collision.action = action || 'cancel';
            $scope.collisionQueueNextAction = angular.element('.explorer-check-same-action').is(':checked') ? action : null;
            angular.element('#replaceModal-' + ($scope.explorerID || '') + '-id').modal('hide');
        };

        $scope.loadDir();
    });
})(window);
