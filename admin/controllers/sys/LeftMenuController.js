;
(function ($root) {

    "use strict";

    $root.$namespace.get('app').controller('LeftMenuController', function ($rootScope, $scope) {

        var Link = function (name, when, accessId) {
            this.name = name || null;
            this.when = when || "dashboard";
            this.accessId = accessId || undefined;
        };

        $scope.links = [];

        $root.$namespace.get('routes').each(function (route) {
            $scope.links.push(new Link(route.linkName, route.when, route.accessId));
        });

        $scope.canAccess = function (link) {
            var accessId = parseInt(link.accessId);

            if (isNaN(accessId) || link.accessId === undefined) {
                return true;
            }

            if (!$rootScope.userInfo || accessId === -1) {
                return false;
            }

            return $root.$namespace.tools.ArrayTool.each($rootScope.userInfo.accessIds, function (key, value) {
                if (accessId === value) {
                    return true;
                }
            });
        };
    });
})(window);