;(function($root) {

    "use strict";

    $root.$namespace.get('app').factory('ToolsFactory', function() {
        return {
            Container   : $root.$namespace.tools.Container,
            EventSet    : $root.$namespace.tools.EventSet,
            ArrayTool   : $root.$namespace.tools.ArrayTool,
            Str         : $root.$namespace.tools.Str
        };
    });
})(window);