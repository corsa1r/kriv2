/**
 * @file Implements ConfigService class
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @version 2014-7-23 13:54:23
 */
/**
 * @param {Object} $root    - window
 * @returns {undefined}
 */
(function($root) {

    "use strict";

    $root.$namespace.get('app').service('Config', function($q, http) {

        var SERVER_PATH = '../server/sys@ConfigurationController/';

        /**
         * This method get value by key from database configs
         * 
         * @param {String} key - the key wich you search for
         * @param {Boolean} toJSON - result to json
         * @see Config.get('facebook').then(function(response) {
         *  //Do something with OutputResponse|response
         * });
         * @returns {$q@call;defer.promise}
         */
        this.get = function(key, toJSON) {
            return this.doPromise('get', "get/" + key + (toJSON ? "/1" : ""));
        };

        /**
         * This method add new configuration or update existing one
         * 
         * @param {String} key - the key wich you want to set/add
         * @param {type} value - new value of key
         * @returns {$q@call;defer.promise}
         */
        this.set = function(key, value) {
            return this.doPromise('post', "set/" + key, {value: value});
        };

        /**
         * This method get all configurations from database by key value
         * @returns {$q@call;defer.promise}
         */
        this.getAll = function() {
            return this.doPromise('get', 'getAll');
        };

        /**
         * This method remove from config
         * @param {String} key - the key wich you want to delete
         * @returns {$q@call;defer.promise}
         */
        this.remove = function(key) {
            return this.doPromise('get', "delete/" + key);
        };

        this.doPromise = function(method, path, data) {
            var deferred = $q.defer();

            http[method](SERVER_PATH + path, data)
                    .then(function(response) {
                        if (response.state) {
                            deferred.resolve(response.data, response);
                        } else {
                            deferred.reject(response);
                        }
                    }, deferred.reject);

            return deferred.promise;
        };

        return this;
    });

})(window, document);