/**
 * @file implements Box service
 * @requires Bootbox
 * @see 
 *   Box.confirm('YOUR_MESSAGE').then(function() { *//* AFTER_CONFIRM  *//* });
  *   Box.alert('YOUR_MESSAGE');
  *
  * @author CORSAIR <vladimir.corsair@gmail.com>
  * @version 29.10.2014 - 11:06
  */
;
(function ($root, $dom, $angular, $u) {

    "use strict";

    $root.$namespace.get('app').service('Box', ['$q', 'http', function ($q, http) {

            if (!$root.bootbox) {
                throw new Error('Bootbox is required for Box service!');
            }

            var $box = $root.bootbox;
            var $message_confirm = "Are you sure?";
            var $message_alert = "Alert!";

            /**
             * This method invokes the bootbox.confirm
             * @param {[String]} message
             * @returns {$q@call;defer.promise}
             */
            this.confirm = function (message) {
                var defered = $q.defer();

                message = message || $message_confirm;

                $box.confirm(message, function ($confirmState) {
                    defered[$confirmState ? 'resolve' : 'reject']($confirmState);
                });

                return defered.promise;
            };

            /**
             * This method invokes confirmation prompt, after resolve 
             * the confirmation new http/GET request will be send and resolved its output response
             * @param {String} path http.get path
             * @see Box.confirmHttp('../server/admin@UsersController/removeById/1').then(function($data) {
             *      //Do something with $data
             * })
             * @param {[String]} message
             * @returns {$q@call;defer.promise}
             */
            this.confirmHttp = function (path, message) {
                var defered = $q.defer();

                this.confirm(message || $u).then((function () {
                    http.get(path).then((function ($response) {
                        if ($response.state) {
                            defered.resolve($response.data);
                        }

                        if ($response.errorMessage) {
                            defered.reject($response);
                            this.alert($response.errorMessage);
                        }
                    }).bind(this), defered.reject);
                }).bind(this), defered.reject);

                return defered.promise;
            };

            /**
             * This method invoke bootbox alert
             * @param {[String]} message - optional message
             * @returns Box reference
             */
            this.alert = function (message) {
                $box.alert(message || $message_alert);
                return this;
            };

        }]);

})(window, document, window.angular);
