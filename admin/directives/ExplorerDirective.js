/**
 * @file Implements ExplorerDirective class
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @version 2014-8-11 10:33:17
 */
/**
 * @param {Object} $root    - window
 * @param {Object} $dom     - document
 * @param {undefined} $u    - undefined
 * @returns {undefined}
 */
(function($root, $dom, $u) {

    "use strict";

    $root.$namespace.get('app').directive('explorerDirective', function($timeout) {
        return {
            restrict: 'E',
            controller: 'ExplorerController',
            templateUrl: 'templates/sys/explorer.html',
            scope: {
                selectedFileType: '@selectFileType',
                fromModal: '=fromModal',
                selectSingleFile: '=selectSingleFile',
                startDir: '@startDir'
            },
            link: function(scope, element, attributes) {
                var selectedExplorerItem = null;
                var selectedExplorerItems = [];
                var selectButton = element.parent('div').parent('div').children('.modal-footer').children('.btn');
                var explorerID = selectButton.parent('div').parent('div').parent('div').parent('div').attr('id');

                scope.explorerID = explorerID;
                scope.startDir = attributes['startDir'] || '';

                var canSelect = false;
                selectButton.attr('disabled', true);

                selectButton.on('click', function() {
                    switch (scope.$eval(attributes['selectSingleFile'])) {
                        case false :
                            if (canSelect && selectedExplorerItems.length) {
                                scope.$emit('explorer.selected.multiple', scope.explorerID, selectedExplorerItems);
                                angular.element('#' + explorerID).modal('hide');
                                //TODO ...
                                break;
                            }
                        case true :
                            if (canSelect && selectedExplorerItem) {
                                scope.$emit('explorer.selected.one', scope.explorerID, selectedExplorerItem);
                                angular.element('#' + explorerID).modal('hide');
                                //TODO ...
                            }
                    }
                });

                scope.$on('explorer.selected.one', function(scope, k, explorerID, explorerItem) {
                    selectButton.removeAttr('disabled');
                    canSelect = true;
                    selectedExplorerItem = explorerItem;
                    selectedExplorerItems = [];
                });

                scope.$on('explorer.selected.multiple', function(scope, k, explorerID, items) {
                    selectButton.removeAttr('disabled');
                    canSelect = true;
                    selectedExplorerItems = items || [];
                });

                scope.$on('explorer.deselected.all', function() {
                    canSelect = false;
                    selectedExplorerItem = null;
                    selectedExplorerItems = [];
                    selectButton.attr('disabled', true);
                });
            }
        };
    });

})(window, document);