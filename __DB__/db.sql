--
-- Table structure for table `sys_admins`
--

CREATE TABLE IF NOT EXISTS `sys_admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_added` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sys_admins`
--

INSERT INTO `sys_admins` (`id`, `group_id`, `username`, `password`, `first_name`, `last_name`, `is_active`, `date_added`) VALUES
(1, 1, 'admin', '098f6bcd4621d373cade4e832627b4f6', 'Admin', 'Adminov', 1, 1406550611);

-- --------------------------------------------------------

--
-- Table structure for table `sys_admin_groups`
--

CREATE TABLE IF NOT EXISTS `sys_admin_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `accessIds` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sys_admin_groups`
--

INSERT INTO `sys_admin_groups` (`id`, `name`, `accessIds`) VALUES
(1, 'Super Admin', '[0]');

-- --------------------------------------------------------

--
-- Table structure for table `sys_analytics`
--

CREATE TABLE IF NOT EXISTS `sys_analytics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(15) NOT NULL,
  `browser` varchar(100) NOT NULL,
  `version` varchar(50) NOT NULL,
  `platform` varchar(50) NOT NULL,
  `is_mobile` tinyint(1) NOT NULL,
  `user_agent` varchar(500) NOT NULL,
  `date_added` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_configs`
--

CREATE TABLE IF NOT EXISTS `sys_configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `last_update` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`),
  KEY `key_2` (`key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sys_configs`
--

INSERT INTO `sys_configs` (`id`, `key`, `value`, `last_update`) VALUES
(1, 'session_key', 'f045073afe1ea79265c77a9df778227d', 1406550808),
(2, 'libs_path.swift', 'libs/swift/classes/', 1408371342),
(3, 'mailer.smtp_server', 'inte12m2.superdnsserver.net', 1410448590),
(4, 'mailer.username', '{USERNAME}', 1410440075),
(5, 'mailer.password', '{PASSWORD}', 1410440073),
(6, 'mailer.port', '25', 1410506665),
(7, 'mailer.encription', '', 1410506640),
(8, 'mailer.from_email', '{FROM_EMAIL}', 1410440065),
(9, 'mailer.from_name', '{FROM_NAME}', 1410440067),
(10, 'mailer.subject', 'Subject', 1406550851),
(11, 'mailer.reply_to_email', '', 1410261927),
(12, 'mailer.reply_to_name', '', 1410261925),
(13, 'libs_path.phpexcel', 'libs/phpexcel/', 1406550866),
(14, 'phpexcel.creator', 'Kriv2 Creator', 1406550866),
(15, 'libs_path.upload', 'libs/fileTransfer/', 1406633096),
(16, 'fileTransferDirs.event_type', 'assets/icons/event_type/', 1406802291),
(17, 'baseUrl', '{BaseURL}', 1407422120),
(18, 'libs_path.mpdf', 'libs/mpdf/', 1408021320);
