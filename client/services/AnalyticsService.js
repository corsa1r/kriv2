/**
 * @file Implements AnalyticsService class
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @version 2014-7-23 15:56:52
 */
/**
 * @param {Object} $root    - window
 * @param {Object} $dom     - document
 * @param {undefined} $u    - undefined
 * @returns {undefined}
 */
(function ($root, $dom, $u) {

    "use strict";

    $root.$namespace.get('app').service('AnalyticsService', function (http, $log) {

        this.insert = function () {
            debug('INSERT', 'send', $root.navigator.userAgent);
            http.post('server/sys@AnalyticsController/insert', {
                user_agent: $root.navigator.userAgent
            });
        };

        function debug(method, message, params) {
            if ($root.$$environment.debug) {
                $log.debug('AnalyticsService ' + method.toUpperCase(), message, params);
            }
        }

    });

})(window, document);