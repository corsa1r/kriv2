/**
 * @file Implements LanguageInjectorService class
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @version 2014-10-10 14:35:49
 */
/**
 * @param {Object} $root    - window
 * @param {Object} $dom     - document
 * @param {undefined} $u    - undefined
 * @returns {undefined}
 */
(function ($root, $dom, $u) {

    "use strict";

    $root.$namespace.get('app').service('LanguageInjector', ['LanguagesService', '$routeParams', 'ToolsFactory', 'ReverseCallback', function (LanguagesService, $routeParams, ToolsFactory, ReverseCallback) {

            this.feed = function ($reference) {
                ToolsFactory.ArrayTool.each($reference.targetScope.$translatesWatchers, ReverseCallback(function (listener) {
                    listener();
                }));

                $reference.targetScope.$translateIndexes = 0;

                LanguagesService.loadLang($routeParams.langCode).then(function (translates) {
                    $reference.targetScope.translates = translates;
                    $reference.targetScope.langCode = $routeParams.langCode;
                }, function () {
                    console.error('LanguageInjector error', arguments);
                });
            };
        }]);

})(window, document);
