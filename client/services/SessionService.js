/**
 * @file Implements SessionService class
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @version 2014-7-23 16:33:00
 */
/**
 * @param {Object} $root    - window
 * @param {Object} $dom     - document
 * @param {undefined} $u    - undefined
 * @returns {undefined} 
 */
(function($root, $dom, $u) {
    
    "use strict";
    
    $root.$namespace.get('app').service('Session', function ($sessionStorage) {
        
        /**
         * This method adds new key and value to the session
         * @param {String} key - the key
         * @param {*} value    - the value
         * @returns {_L16}
         */
        this.set = function (key, value) {
            if(this.isValid(key) && this.isValid(value)) {
                $sessionStorage[key] = value;
            }
            
            return this;
        };
        
        /**
         * This method check if the given value is valid for set
         * @param {type} what
         * @returns {boolean} false if what is undefined or null true otherwise
         */
        this.isValid = function (what) {
            return !Boolean(angular.isUndefined(what) || what === null);
        };
        
        /**
         * This method gives you a value from session by given key
         * @param {type} key
         * @returns {@var;value|_L16.$sessionStorage}
         */
        this.get = function (key) {
            return this.exists(key) ? $sessionStorage[key] : null;
        };
        
        /**
         * This method removes value from session y given key
         * @param {type} key
         * @returns {_L16}
         */
        this.remove = function (key) {
            if(this.isValid(key) && this.exists(key)) {
                delete $sessionStorage[key];
            }
            
            return this;
        };
        
        /**
         * This method check if the given key is exists in current session
         * @param {type} key
         * @returns {Boolean} true if exists false otherwise
         */
        this.exists = function (key) {
            return Boolean(!angular.isUndefined($sessionStorage[key]));
        };
        
        /**
         * This method clear all session data
         * @returns {_L16}
         */
        this.empty = function () {
            $sessionStorage.$reset();
            return this;
        };
        
        /**
         * This method foreaches all session by key and value and pass it to the procedure callback
         * @param {Function} procedure - the callback function for each step of item in the session
         * @throws {Exception} error message if procedure $param is not a valid callable function
         * @returns {_L16}
         */
        this.each = function (procedure) {
            if(!angular.isFunction(procedure)) {
                throw 'Procedure parameter must be a valid callable function !';
            }
            
            $root.$namespace.tools.ArrayTool.each($sessionStorage, function (key, value) {
                if(!angular.isFunction(value)) {
                    //Do the procedure only if value is not a function
                    //This is because $sessionStorage have an functions in to the constructor.
                    procedure(key, value);
                }
            });
            return this;
        };
        
        return this;
    });
    
})(window, document);