;(function($root, $dom, $u, $angular) {
    
    "use strict";
    
    $root.$namespace.get('app').service('LanguagesService', ['http', '$q', '$location', function(http, $q, $location) {
        
        var translates = null;
        var loadedLang = null;
        
        this.loadLang = function(langCode) {
            var defer = $q.defer();
            
            //console.log('about to load');
            
            if(loadedLang === langCode && translates !== null) {
                defer.resolve(translates);
            } else {
                if(!langCode) {
                    defer.reject('This url has no lang code', $location.path());
                }
                
                http.get('./server/sys@LanguagesController/getTranslate/' + langCode).then(function(response) {
                    if(!response.state || !response.data) {
                        defer.reject('Cannot get translate for this language', langCode);
                    } else {
                        translates = response.data;
                        loadedLang = langCode;
                        defer.resolve(translates);
                        //console.log('asd');
                    }
                });
            }
            
            return defer.promise;
        };
        
        return this;
    }]);
    
})(window, document, undefined, window.angular);