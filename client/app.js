/**
 * @file implements Application init
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @version 14.07.2014
 *
 * 
 * @param {Object} $root - this is window alias
 * @returns {undefined}
 */
;(function($root) {
    $root.$namespace.add(new $root.$namespace.tools.Container(), 'dependencies');
    var $dependencies = $root.$namespace.get('dependencies');

    //Add application dependencies
    //-----------------------------------------------------------------------------------------------------------------
    $dependencies.add('ngRoute');
    $dependencies.add('ngCookies');
    $dependencies.add('ngStorage');
    $dependencies.add('ngSanitize');

//        $dependencies.add('google-maps');
//        $dependencies.add('facebook');
    //-----------------------------------------------------------------------------------------------------------------

    $root.$namespace.add(angular.module(document.getElementsByTagName('html')[0].getAttribute('ng-app'), $dependencies.toArray()), 'app');

    $root.$namespace.get('app').config(function($routeProvider) {
        $root.$namespace.get('routes').each(function(route) {
            route.templateUrl = "client/" + route.templateUrl;
            $routeProvider.when(route.when, route);
        });
        
        $routeProvider.otherwise({
            redirectTo: '/'
        });
    });

    if ($dependencies.indexOf('facebook') !== -1) {
        $root.$namespace.get('app').config(function(FacebookProvider) {
            FacebookProvider.init();
        });
    }

})(window);
