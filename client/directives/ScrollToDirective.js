/**
 * @file Implements ScrollNextDirective class
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @version 2014-10-30 11:28:27
 */
/**
 * @param {Object} $root    - window
 * @param {Object} $dom     - document
 * @param {undefined} $u    - undefined
 * @returns {undefined}
 */
(function ($root, $dom, $u) {

    "use strict";

    /**
     * @page index.html
     *      <a href="" scroll-to="name"> - when this link was clicked
     * @page other.html
     *      <ANY scroll-index="name"></ANY> - the page will be scrolled to this element
     *      
     * @description
     *  scroll-to seach for scroll-index in next page
     *  - after 20 attempts the element listener will be killed
     */
    $root.$namespace.get('app').directive('scrollTo', ['$interval', function ($interval) {
            return {
                restrict: 'A',
                link: function (scope, element, attributes) {
                    element.off('mousedown').on('mousedown', function () {

                        var attempts = 0;

                        var intervalId = $interval(function () {

                            var point = angular.element('[scroll-index="' + attributes.scrollTo + '"]');

                            if (point.length || attempts === 20) {
                                if (!angular.isUndefined(point.offset())) {
                                    angular.element('html, body').stop().animate({
                                        scrollTop: point.offset().top
                                    }, 333);
                                }

                                $interval.cancel(intervalId);
                            }

                            attempts++;
                        }, 300);
                    });
                }
            };
        }]);

})(window, document);
