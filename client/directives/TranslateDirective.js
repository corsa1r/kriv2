;
(function ($root, $dom, $u, $angular) {

    "use strict";

    $root.$namespace.get('app').directive('translate', ['$rootScope', '$compile', function ($rootScope, $compile) {

            $rootScope.$translatesWatchers = [];
            $rootScope.$translateIndexes = 0;

            return {
                restrict: 'A',
                link: function (scope, element, attributes) {

                    element.attr('tr-index', $rootScope.$translateIndexes++);

                    //TODO Test this Sahip!
                    if (typeof $rootScope.$translatesWatchers[element.attr('tr-index')] === 'function') {
                        $rootScope.$translatesWatchers[element.attr('tr-index')]();
                    }

                    $rootScope.$translatesWatchers[element.attr('tr-index')] = $rootScope.$watch('translates', function (hasTranslates) {
                        if (hasTranslates) {
                            var newElem = $compile('<translate>' + $rootScope.translates[attributes.translate] + '</translate>')(scope);

                            element.contents().remove();
                            element.append(newElem);
                        }
                    });

                }
            };
        }]);

})(window, document, undefined, window.angular);
