/**
 * Setup enviroment of this project
 * 
 */

;
(function ($root) {

    "use strict";

    /**
     * This is enviroment configuration of the CMS
     * @private
     */
    $root.$$environment = {};

    /**
     * If set this to 'true', the routeChangeSuccess event will call LanguagesInjectorService.feed() method.
     * @shortDescription
     *  If you are want to use multi-Language directives set this to 'true'
     */
    $root.$$environment.feedLanguageInjector = false;

    /**
     * Use this in development progress only
     */
    $root.$$environment.debug = true;

    /**
     * Set this to true if you want to disable loggers
     * @example console.log, console.info, console.warn, console.error
     * 
     * You can use this after release the project to get clean console.
     */
    $root.$$environment.disableLoggers = false;
    $root.$$environment.disabledLogger = (function () {
    });
})(window);