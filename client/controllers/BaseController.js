/**
 * @file Implements BaseController class
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @version 2014-7-23 16:23:26
 */
/**
 * @param {Object} $root    - window
 * @param {Object} $dom     - document
 * @param {undefined} $u    - undefined
 * @returns {undefined}
 */
(function ($root, $dom, $u) {

    "use strict";

    $root.$namespace.get('app').controller('BaseController',
            ['$rootScope', '$scope', 'AnalyticsService', '$location', '$routeParams', 'LanguageInjector', 'http', '$log', function ($rootScope, $scope, AnalyticsService, $location, $routeParams, LanguageInjector, http, $log) {

                    /**
                     * Dont touch this, ta dadada taa da
                     */
                    if ($root.$$environment.disableLoggers) {
                        angular.forEach(['log', 'info', 'warn', 'debug', 'error'], function (loggerName) {
                            $log[loggerName] = $root.$$environment.disabledLogger;
                            console[loggerName] = $log[loggerName];
                        });
                    }

                    $scope.$watch(function () {
                        return $location.path();
                    }, function (newPath) {
                        AnalyticsService.insert();
                        //Maybe Google Analitycs here ???!?!?!?!!!! Sahip.
                    });

                    if ($root.$$environment.feedLanguageInjector) {
                        $scope.$on('$routeChangeSuccess', LanguageInjector.feed);
                    }

                }]);

})(window, document);