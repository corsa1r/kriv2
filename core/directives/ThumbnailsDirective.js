/**
 * @file Implements ThumbnailsDirective class
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @version 2014-10-29 15:37:36
 */
/**
 * @param {Object} $root    - window
 * @returns {undefined}
 */
;
(function ($root) {

    "use strict";

    /**
     * @see
     *  <img thumb-path="./path/to/file.jpg" thumb-params="width/height/position/grayscale">
     *  @params thumb-path - the path to image
     *  @params thumb-params Number/Number/String:2/Boolean
     */
    $root.$namespace.get('app').directive('thumbPath', ['ToolsFactory',
        /**
         * 
         * @param {type} ToolsFactory
         * @returns {ThumbnailsDirective_L26.ThumbnailsDirectiveAnonym$1}
         */
        function (ToolsFactory) {
            return {
                restrict: 'A',
                link: function (scope, element, attributes) {
                    var positions = ['tl', 'tc', 'tr', 'ml', 'mc', 'mr', 'bl', 'bc', 'br'];
                    var server_path_prefix = $root.angular.element('html').attr('ng-app').split('-').pop().toLowerCase() === 'client' ? '.' : '..';

                    var path = attributes.thumbPath;

                    var params = attributes.thumbParams.split('/');

                    var width = scope.$eval(params[0]);
                    var height = scope.$eval(params[1]);

                    var position = 'mc';//optional
                    var grayscale = false;//optional

                    if (params[2] && positions.indexOf(params[2]) !== -1) {
                        position = params[2];
                    }

                    if (params[3]) {
                        if (params[3] !== 'true' && params[3] !== 'false') {
                            params[3] = grayscale.toString();
                        }

                        grayscale = scope.$eval(params[3]);
                    }

                    element.attr('src', server_path_prefix + '/server/sys@ImageController/getThumbPath/' + ToolsFactory.Str.Base64.encode([path, width, height, position, grayscale].join('#')));
                }
            };
        }]);
})(window, document);
