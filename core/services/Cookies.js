/**
* @file implements Cookie service
* @description This Service gives you simple interface based on setters and getters to set and get cookies from all browsers.
* @requires jquery.cookie.js
* @author CORSAIR <vladimir.corsair@gmail.com>
*
* @version 03.11.2014
*/
;(function($root, $angular, $u) {
  
    "use strict";
  
    $root.$namespace.get('app').service('Cookie', function() {
        
        /***
         * This methos sets a cookie
         * 
         * @param {String} key - cookie key
         * @param {String|Boolean|Number} value - cookie content
         * @param {Object} additionalOptions - the additional options
         *  @see 
         *    {
         *      expires : Number  //this is the value in days 7 = 1 week,
         *      path : String     // this is cookie path, by default is native browser intent
         *    }
         */
        this.set = function(key, value, additionalOptions) {
            return $angular.element.cookie(key, value, !$angular.isUndefined(additionalOptions) ? additionalOptions : {});
        };
        
        /**
         * This methos gets and/or convert value from cookie
         * 
         * @param {String} key - the key wich you search for.
         * @param {[Object]} classRef - this param is optional, but if you set it, must be a class reference
         *  @see Number, Object, String, Boolean
         * 
         */
        this.get = function(key, classRef) {
            return $angular.element.cookie(key, $angular.isFunction(classRef) ? classRef : $u);
        };
        
        /**
         * This method remove cookie from the store.
         * 
         * @param {String} key - the key wich you want to remove.
         */
        this.remove = function(key, paths) {
            return $angular.element.removeCookie(key, !$angular.isUndefined(paths) ? paths : $u);
        };
        
        /**
         * This method check if given key exists in cookies store.
         * 
         * @param {String} key - this is the key wich you want to remove.
         * @returns Boolean
         */
        this.exists = function(key) {
            return Boolean(this.get(key, $u));
        };
    
    });
})(window, window.angular);
