/**
 * @file Implements HttpService class
 * @author CORSAIR <vladimir.corsair@gmail.com>
 * @version 2014-8-8 14:53:04
 */
/**
 * @param {Object} $root    - window
 * @param {Object} $dom     - document
 * @param {undefined} $u    - undefined
 * @returns {undefined}
 */
(function ($root, $dom, $u, angular) {

    "use strict";

    $root.$namespace.get('app').service('http', ['$http', '$q', '$location', '$log', function ($http, $q, $location, $log) {
            var HTTP_VER = 1.1;
            this.get = function () {
                return doDeferredPromise('get', Array.prototype.slice.call(arguments));
            };

            this.post = function () {
                return doDeferredPromise('post', Array.prototype.slice.call(arguments));
            };

            this.getNative = function () {
                return doPromise('get', Array.prototype.slice.call(arguments));
            };

            this.postNative = function () {
                return doPromise('post', Array.prototype.slice.call(arguments));
            };

            function doPromise(/*method, arguments */) {
                var _args = Array.prototype.slice.call(arguments);
                var _method = _args.shift();
                var deferred = $q.defer();

                debug(_method, ': is about to start at ', _args);

                angular.element('.admin-http-loading').html('Loading: ' + _args[0]).show();

                $http[_method].apply($http[_method], _args)
                        .success(function (data) {
                            debug('is complete', data);
                            deferred.resolve(data);
                            angular.element('.admin-http-loading').hide();
                        })
                        .error(function (data) {
                            deferred.reject();
                            debug('handled error for reason:', data);
                        });

                return deferred.promise;
            }

            function doDeferredPromise(method, _args) {
                var deferred = $q.defer();
                _args.unshift(method);

                doPromise.apply(doPromise, _args).then(function (response) {
                    if (!isValidSession(response)) {
                        return false;
                    }

                    deferred.resolve(response);
                }, deferred.reject);

                return deferred.promise;
            }

            function isValidSession(response) {
                if (response.data && response.state) {
                    if (response.data.invalidSession) {
                        $location.path('/logout');
                        return false;
                    }
                }

                return true;
            }

            function debug(method, message, params) {
                if ($root.$$environment && $root.$$environment.debug) {
                    $log.debug('HTTP ' + HTTP_VER + '/' + method.toUpperCase(), message, params);
                }
            }

        }]);

})(window, document, undefined, angular);
