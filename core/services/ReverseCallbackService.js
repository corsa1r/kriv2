/**
* @file implements ReverseCallback Service
* @description - use this function to reverse order of parameters of function
* If you use ArrayTool.each and does't need 'index' variable you can use only 'value'
*
* @see
*   htt.get('something').then(ReverseCallback(function(value) {
*       //Do something with 'value' 
*   }));
* 
* @author CORSAIR <vladimir.corsair@gmail.com>
* @version 20.10.2014
*/
;(function($root, $dom, $u, $angular) {
    
    "use strict"
    
    $root.$namespace.get('app').factory('ReverseCallback', function() {
        return function (iteratorFn) {
            return (function() {
                var _args = Array.prototype.slice.call(arguments);
                iteratorFn.apply(this, [].concat(_args.reverse()));
            }).bind(iteratorFn);
        }
    });
    
})(window, document, undefined, angular);
