;(function ($root) {

    $root.$namespace.get('app').filter('shortText', function () {
        return function (text, maxLength) {
            if (text.toString().length > maxLength) {
                var trimmedString = text.toString().substr(0, maxLength);
                return trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" "))) + '...';
            }

            return text;
        };
    });

})(window);
