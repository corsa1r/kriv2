;(function ($root, $dom, $u) {
    
    "use strict";

    $root.$namespace.get('app').filter('stripHTML', function() {
        return function(text) {
            return String(text).replace(/<[^>]+>/gm, '');
        };
    });
    
})(window, document);