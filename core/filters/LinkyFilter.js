;
(function($root) {

    "use strict";

    $root.$namespace.get('app').filter('rlinks', ['AutolinkerService', '$sce', function(AutolinkerService, $sce) {

            return function(text, newWindow, stripPrefix) {
                if (!$root.angular.isUndefined(text) && text.length) {
                    return  $sce.trustAs($sce.HTML, AutolinkerService.findAndReplace(text, newWindow, stripPrefix));
                }
            };

        }]);

})(window);
