;
(function ($root) {

    $root.$namespace.get('app').filter('unsafeHtml', function ($sce) {
        return function (val) {
            return $sce.trustAsHtml(val);
        };
    });

})(window);