;
(function ($root) {

    $root.$namespace.get('app').filter('fixedLength', function () {
        return function (a, b) {
            return(1e8 + a + "").slice(-b)
        };
    });

})(window);